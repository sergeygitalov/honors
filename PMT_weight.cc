#include <iostream>

double GetWeight(double wavelength) {
    double ref_wav[11];
    for (int i = 0; i < 11; i++)
        ref_wav[i] = 295. + 42.5*i;

    double slope[12] = {0., 0.400, 0.235, 0.0235, -0.188, -0.188, -0.0941,
                        -0.134, -0.0565, -0.0191, -0.00188, 0.};
    double inter[12] = {0., -117.00, -61.41, 19.06, 108.53, 108.53, 60.76, 82.76,
                        36.76, 13.00, 1.37, 0.};

    for (int i = 0; i < 12; i++)
        if (wavelength < ref_wav[i])
            return 0.01 * (slope[i] * wavelength + inter[i]);
}

int main(int argc, char** argv) {
    for (int i = 0; i < 11; i++)
        std::cout << GetWeight(295 + i*42.5) << std::endl;

    return 0;
}
