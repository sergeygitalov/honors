//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// \file CosmicMuSteppingAction.cc
// \brief Implementation of CosmicMuSteppingAction class

#include "CosmicMuSteppingAction.hh"

#include "G4RunManager.hh"
#include "G4MuonMinus.hh"
#include "G4MuonPlus.hh"
#include "G4Electron.hh"
#include "G4Positron.hh"
#include "G4OpticalPhoton.hh"
#include "G4Neutron.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

#include "CosmicMuDetectorConstruction.hh"

CosmicMuSteppingAction::CosmicMuSteppingAction()
: G4UserSteppingAction(), fEvent((CosmicMuEventAction*) 
                                 G4RunManager::GetRunManager()
                                    ->GetUserEventAction())
{}

CosmicMuSteppingAction::~CosmicMuSteppingAction() {}

void CosmicMuSteppingAction::UserSteppingAction(const G4Step *Step)
{
    trackMuDecay(Step);
    trackParticle(Step, "mu", "InnerDetector_log");
    trackParticle(Step, "mu", "OuterDetector_log");
    trackParticle(Step, "e", "InnerDetector_log");
    trackParticle(Step, "e", "OuterDetector_log");
    trackPhoton(Step);
}

// Track whether the muon decayed, was captured, and what volume it decayed in
//
void CosmicMuSteppingAction::trackMuDecay(const G4Step* Step)
{
    G4Track* Track = Step->GetTrack();
    G4LogicalVolume* PreVolume
        = Step->GetPreStepPoint()->GetPhysicalVolume()->GetLogicalVolume();

    // fMuDecay values:
    // No decay = -2
    // Mu- capture = -1
    // Decay outside detector = 0
    // Inner Detector = 1
    // Outer Detector = 2

    if (*fEvent->GetfMuDecay() == -2
        && (Track->GetDefinition() == G4Electron::ElectronDefinition()
        || Track->GetDefinition() == G4Positron::PositronDefinition())
        && Track->GetCreatorProcess()->GetProcessName() == "Decay")
    {

        if (PreVolume->GetName() == "InnerDetector_log")
        *fEvent->GetfMuDecay() = 1;
        else if (PreVolume->GetName() == "OuterDetector_log")
        *fEvent->GetfMuDecay() = 2;
        else
        *fEvent->GetfMuDecay() = 0;

        if (*fEvent->GetfMuDecay() > 0)
        trackPosAndMom(Step, fEvent->GetfE4Pos(), fEvent->GetfE4Mom(), 0);
    }

    if (*fEvent->GetfMuDecay() != -1
        && Track->GetDefinition() == G4Neutron::NeutronDefinition()
        && Track->GetCreatorProcess()->GetProcessName() == "muMinusCaptureAtRest")
        *fEvent->GetfMuDecay() = -1;
}

// Tracks 4-position, 4-momentum, entry volume, and distance traveled for a
// given particle.
// Particle options: "mu", "e"
// Volume options: "InnerDetector_log", "OuterDetector_log"
//
void CosmicMuSteppingAction::trackParticle(const G4Step* Step,
                                           G4String particleName, 
                                           G4String volumeName)
{
    G4Track* Track = Step->GetTrack();
    G4LogicalVolume* PreVolume
        = Step->GetPreStepPoint()->GetPhysicalVolume()->GetLogicalVolume();
    G4LogicalVolume* PostVolume;

    G4VPhysicalVolume* fPostPhysVolume
        = Step->GetPostStepPoint()->GetPhysicalVolume();

    if (fPostPhysVolume != NULL)
        PostVolume = fPostPhysVolume->GetLogicalVolume();
    else
        PostVolume = NULL;

    G4ParticleDefinition* ParDef = NULL;
    G4ParticleDefinition* AntiParDef = NULL;
    G4double* _4Pos = NULL;
    G4double* _4Mom = NULL;
    G4int* entry = NULL;
    G4double* distance = NULL;

    if (particleName == "mu")
    {
        ParDef = G4MuonMinus::MuonMinusDefinition();
        AntiParDef = G4MuonPlus::MuonPlusDefinition();

        if (volumeName == "InnerDetector_log")
        {
            _4Pos = fEvent->GetfMuID4Pos();
            _4Mom = fEvent->GetfMuID4Mom();
            entry = fEvent->GetfMuIDEntry();
            distance = fEvent->GetfMuIDDistance();
        }
        else if (volumeName == "OuterDetector_log")
        {
            _4Pos = fEvent->GetfMuOD4Pos();
            _4Mom = fEvent->GetfMuOD4Mom();
            entry = fEvent->GetfMuODEntry();
            distance = fEvent->GetfMuODDistance();
        }
    }
    else if (particleName == "e")
    {
        ParDef = G4Electron::ElectronDefinition();
        AntiParDef = G4Positron::PositronDefinition();

        if (volumeName == "InnerDetector_log")
        {
            _4Pos = fEvent->GetfEID4Pos();
            _4Mom = fEvent->GetfEID4Mom();
            entry = fEvent->GetfEIDEntry();
            distance = fEvent->GetfEIDDistance();
        }
        else if (volumeName == "OuterDetector_log")
        {
            _4Pos = fEvent->GetfEOD4Pos();
            _4Mom = fEvent->GetfEOD4Mom();
            entry = fEvent->GetfEODEntry();
            distance = fEvent->GetfEODDistance();
        }
    }

    if (PostVolume != NULL
        && PostVolume->GetName() == volumeName
        && (Track->GetDefinition() == ParDef
        || Track->GetDefinition() == AntiParDef))
    {
        if (PreVolume->GetName() != volumeName)
        {
            if (*entry == 0)
                trackPosAndMom(Step, _4Pos, _4Mom, 1);

            *entry += 1;
        }

        if (PreVolume->GetName() == volumeName)
            trackDistance(Step, distance);
    }
}

// Track photons' 4-position, wavelength, and process when they reach the
// PMT photocathode
//
void CosmicMuSteppingAction::trackPhoton(const G4Step* Step)
{
    G4Track* Track = Step->GetTrack();
    G4LogicalVolume* PreVolume
        = Step->GetPreStepPoint()->GetPhysicalVolume()->GetLogicalVolume();
    G4LogicalVolume* PostVolume;

    G4VPhysicalVolume* fPostPhysVolume
        = Step->GetPostStepPoint()->GetPhysicalVolume();

    if (fPostPhysVolume != NULL)
        PostVolume = fPostPhysVolume->GetLogicalVolume();
    else
        PostVolume = NULL;

    if (PostVolume != NULL
        && ((PostVolume->GetName() == "IDPMTDetector_log"
        && PreVolume->GetName() != "IDPMTDetector_log")
        || (PostVolume->GetName() == "ODPMTDetector_log"
        && PreVolume->GetName() != "ODPMTDetector_log"))
        && Track->GetDefinition() == G4OpticalPhoton::OpticalPhotonDefinition())
    {
        G4StepPoint* postStepPoint = Step->GetPostStepPoint();

        G4double globalTime = postStepPoint->GetGlobalTime();
        G4double* electronTime = fEvent->GetfE4Pos();
        G4double wavelength
        = h_Planck*c_light/(postStepPoint->GetTotalEnergy() * nm);

        // Separate optical photons by their optical property
        // IDs:
        // Primary = 0
        // Cerenkov = 1
        // Scintillation = 2
        // Other = 3
        G4int process = 0;

        if (Track-> GetCreatorProcess() != 0)
        {
            G4String processName = Track->GetCreatorProcess()->GetProcessName();

            if (processName == "Cerenkov")
                process = 1;
            else if (processName == "Scintillation")
                process = 2;
            else
                process = 3;
        }

        // Separate photons by their mother particle
        // This is done by tracking whether the photon was detected before or
        // after muon decay
        //
        if (PostVolume->GetName() == "IDPMTDetector_log")
        {
            if (*fEvent->GetfMuDecay() > 0 && globalTime > *electronTime)
            {
                *fEvent->GetfPhIDE() += 1;
                fEvent->GetfPhIDETime().push_back(globalTime);
                fEvent->GetfPhIDEWavelength().push_back(wavelength);
                fEvent->GetfPhIDEProcess().push_back(process);
            }
            else
            {
                *fEvent->GetfPhIDMu() += 1;
                fEvent->GetfPhIDMuTime().push_back(globalTime);
                fEvent->GetfPhIDMuWavelength().push_back(wavelength);
                fEvent->GetfPhIDMuProcess().push_back(process);
            }
        }
        else if (PostVolume->GetName() == "ODPMTDetector_log")
        {
            if (*fEvent->GetfMuDecay() > 0 && globalTime > *electronTime)
            {
                *fEvent->GetfPhODE() += 1;
                fEvent->GetfPhODETime().push_back(globalTime);
                fEvent->GetfPhODEWavelength().push_back(wavelength);
                fEvent->GetfPhODEProcess().push_back(process);
            }
            else
            {
                *fEvent->GetfPhODMu() += 1;
                fEvent->GetfPhODMuTime().push_back(globalTime);
                fEvent->GetfPhODMuWavelength().push_back(wavelength);
                fEvent->GetfPhODMuProcess().push_back(process);
            }
        }
    }

    // Kill photons after they hit the detector
    //
    Track->SetTrackStatus(fStopAndKill);
}

// Helper method that modifies the input pointers _4Pos and _4Mom
// Options:
// 0: preStep
// 1: postStep
//
void CosmicMuSteppingAction::trackPosAndMom(const G4Step* Step,
                                            G4double* _4Pos, G4double* _4Mom, 
                                            int option)
{
    if (!_4Pos || !_4Mom)
        return;

    G4StepPoint* StepPoint;

    if (option == 0)
        StepPoint = Step->GetPreStepPoint();
    else if (option == 1)
        StepPoint = Step->GetPostStepPoint();
    else
    {
        G4cout << "Error: Invalid tracking option" << G4endl;
        return;
    }

    *_4Pos = StepPoint->GetGlobalTime();
    G4ThreeVector _3pos = StepPoint->GetPosition();
    *(_4Pos + 1) = _3pos.getX()/cm;
    *(_4Pos + 2) = _3pos.getY()/cm;
    *(_4Pos + 3) = _3pos.getZ()/cm;

    *_4Mom = StepPoint->GetTotalEnergy();
    G4ThreeVector _3mom = StepPoint->GetMomentum();
    *(_4Mom + 1) = _3mom.getX()/MeV;
    *(_4Mom + 2) = _3mom.getY()/MeV;
    *(_4Mom + 3) = _3mom.getZ()/MeV;
}

// Helper method to track distance. Modifies input pointer dist
//
void CosmicMuSteppingAction::trackDistance(const G4Step* Step, G4double* dist)
{
    if (!dist)
        return;

    G4StepPoint* preStepPoint = Step->GetPreStepPoint();
    G4StepPoint* postStepPoint = Step->GetPostStepPoint();

    G4ThreeVector prePos = preStepPoint->GetPosition();
    G4ThreeVector postPos = postStepPoint->GetPosition();

    G4double preX = prePos.getX()/cm;
    G4double preY = prePos.getY()/cm;
    G4double preZ = prePos.getZ()/cm;

    G4double postX = postPos.getX()/cm;
    G4double postY = postPos.getY()/cm;
    G4double postZ = postPos.getZ()/cm;

    *dist += std::pow(std::pow(postX - preX, 2) + std::pow(postY - preY, 2)
             + std::pow(postZ - preZ, 2), 0.5);
}

