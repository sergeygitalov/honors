//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file CosmicMuPhysicsList.cc
/// \brief Implementation of the CosmicMuPhysicsList class
//
#include "CosmicMuPhysicsList.hh"

#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4HadronElasticPhysics.hh"
#include "G4HadronPhysicsFTFP_BERT_HP.hh"
#include "G4HadronPhysicsQGSP_BIC_HP.hh"
#include "G4HadronPhysicsQGSP_BIC_AllHP.hh"
#include "G4HadronInelasticQBBC.hh"
#include "G4HadronPhysicsINCLXX.hh"
#include "G4IonElasticPhysics.hh"
#include "G4IonPhysicsXS.hh"
#include "G4IonINCLXXPhysics.hh"
#include "G4StoppingPhysics.hh"
#include "G4OpticalPhysics.hh"
#include "G4EmStandardPhysics.hh"
#include "G4DecayPhysics.hh"
#include "G4RadioactiveDecayPhysics.hh"

// This class uses pre-built geant physics lists. The most important ones  are
// decay and optical physics. Without them, muons won't decay and there is no
// Cherenkov or scintillation.
//
CosmicMuPhysicsList::CosmicMuPhysicsList()
:G4VModularPhysicsList()
{
    G4int verb = 0;
    SetVerboseLevel(verb);

    // Hadron Elastic scattering
    RegisterPhysics(new G4HadronElasticPhysics(verb));

    // Hadron Inelastic Physics
    RegisterPhysics(new G4HadronPhysicsFTFP_BERT_HP(verb));
    ////RegisterPhysics(new G4HadronPhysicsQGSP_BIC_HP(verb));
    ////RegisterPhysics(new G4HadronPhysicsQGSP_BIC_AllHP(verb));
    ////RegisterPhysics(new G4HadronInelasticQBBC(verb));
    ////RegisterPhysics(new G4HadronPhysicsINCLXX(verb));

    // Ion Elastic scattering
    RegisterPhysics(new G4IonElasticPhysics(verb));

    // Ion Inelastic physics
    RegisterPhysics(new G4IonPhysicsXS(verb));
    ////RegisterPhysics(new G4IonINCLXXPhysics(verb));

    // stopping Particles
    RegisterPhysics( new G4StoppingPhysics(verb));

    // EM physics
    RegisterPhysics(new G4EmStandardPhysics(verb));

    // Decay
    RegisterPhysics(new G4DecayPhysics(verb));

    // Radioactive decay
    RegisterPhysics(new G4RadioactiveDecayPhysics(verb));

    // Optical Physics
    RegisterPhysics(new G4OpticalPhysics(verb));
}

CosmicMuPhysicsList::~CosmicMuPhysicsList()
{}

void CosmicMuPhysicsList::SetCuts()
{}
