//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// \file CosmicMuEventAction.cc
// \brief Implementation of CosmicMuEventAction class

#include "CosmicMuEventAction.hh"

#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4VVisManager.hh"
#include "G4ios.hh"
#include "G4SystemOfUnits.hh"
#include "g4root.hh"

#include "CosmicMuRunAction.hh"

CosmicMuEventAction::CosmicMuEventAction() : G4UserEventAction() {}

CosmicMuEventAction::~CosmicMuEventAction() {}

void CosmicMuEventAction::BeginOfEventAction(const G4Event* event)
{
    // Reset all the tracked variables
    //
    ResetData();

    // Muon tracking upon generation
    //
    G4PrimaryVertex* primaryVertex = event->GetPrimaryVertex();

    fMu3Pos[0] = primaryVertex->GetX0()/cm;
    fMu3Pos[1] = primaryVertex->GetY0()/cm;
    fMu3Pos[2] = primaryVertex->GetZ0()/cm;

    G4PrimaryParticle* primary = primaryVertex->GetPrimary();

    fMu4Mom[0] = primary->GetTotalEnergy();
    fMu4Mom[1] = primary->GetPx();
    fMu4Mom[2] = primary->GetPy();
    fMu4Mom[3] = primary->GetPz();

    fMuCharge = primary->GetCharge();
}

void CosmicMuEventAction::EndOfEventAction(const G4Event*)
{
    // Analysis manager creates ntuples and saves them in a ROOT tree
    //
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

    // Muon tracking
    //
    analysisManager->FillNtupleDColumn(0, fMu3Pos[0]);
    analysisManager->FillNtupleDColumn(1, fMu3Pos[1]);
    analysisManager->FillNtupleDColumn(2, fMu3Pos[2]);
    analysisManager->FillNtupleDColumn(3, fMu4Mom[0]);
    analysisManager->FillNtupleDColumn(4, fMu4Mom[1]);
    analysisManager->FillNtupleDColumn(5, fMu4Mom[2]);
    analysisManager->FillNtupleDColumn(6, fMu4Mom[3]);
    analysisManager->FillNtupleIColumn(7, fMuCharge);
    analysisManager->FillNtupleIColumn(8, fMuDecay);

    analysisManager->FillNtupleDColumn(9, fMuID4Pos[0]);
    analysisManager->FillNtupleDColumn(10, fMuID4Pos[1]);
    analysisManager->FillNtupleDColumn(11, fMuID4Pos[2]);
    analysisManager->FillNtupleDColumn(12, fMuID4Pos[3]);
    analysisManager->FillNtupleDColumn(13, fMuID4Mom[0]);
    analysisManager->FillNtupleDColumn(14, fMuID4Mom[1]);
    analysisManager->FillNtupleDColumn(15, fMuID4Mom[2]);
    analysisManager->FillNtupleDColumn(16, fMuID4Mom[3]);
    analysisManager->FillNtupleIColumn(17, fMuIDEntry);
    analysisManager->FillNtupleDColumn(18, fMuIDDistance);

    analysisManager->FillNtupleDColumn(19, fMuOD4Pos[0]);
    analysisManager->FillNtupleDColumn(20, fMuOD4Pos[1]);
    analysisManager->FillNtupleDColumn(21, fMuOD4Pos[2]);
    analysisManager->FillNtupleDColumn(22, fMuOD4Pos[3]);
    analysisManager->FillNtupleDColumn(23, fMuOD4Mom[0]);
    analysisManager->FillNtupleDColumn(24, fMuOD4Mom[1]);
    analysisManager->FillNtupleDColumn(25, fMuOD4Mom[2]);
    analysisManager->FillNtupleDColumn(26, fMuOD4Mom[3]);
    analysisManager->FillNtupleIColumn(27, fMuODEntry);
    analysisManager->FillNtupleDColumn(28, fMuODDistance);

    // Electron Tracking
    //
    analysisManager->FillNtupleDColumn(29, fE4Pos[0]);
    analysisManager->FillNtupleDColumn(30, fE4Pos[1]);
    analysisManager->FillNtupleDColumn(31, fE4Pos[2]);
    analysisManager->FillNtupleDColumn(32, fE4Pos[3]);
    analysisManager->FillNtupleDColumn(33, fE4Mom[0]);
    analysisManager->FillNtupleDColumn(34, fE4Mom[1]);
    analysisManager->FillNtupleDColumn(35, fE4Mom[2]);
    analysisManager->FillNtupleDColumn(36, fE4Mom[3]);

    analysisManager->FillNtupleDColumn(37, fEID4Pos[0]);
    analysisManager->FillNtupleDColumn(38, fEID4Pos[1]);
    analysisManager->FillNtupleDColumn(39, fEID4Pos[2]);
    analysisManager->FillNtupleDColumn(40, fEID4Pos[3]);
    analysisManager->FillNtupleDColumn(41, fEID4Mom[0]);
    analysisManager->FillNtupleDColumn(42, fEID4Mom[1]);
    analysisManager->FillNtupleDColumn(43, fEID4Mom[2]);
    analysisManager->FillNtupleDColumn(44, fEID4Mom[3]);
    analysisManager->FillNtupleIColumn(45, fEIDEntry);
    analysisManager->FillNtupleDColumn(46, fEIDDistance);

    analysisManager->FillNtupleDColumn(47, fEOD4Pos[0]);
    analysisManager->FillNtupleDColumn(48, fEOD4Pos[1]);
    analysisManager->FillNtupleDColumn(49, fEOD4Pos[2]);
    analysisManager->FillNtupleDColumn(50, fEOD4Pos[3]);
    analysisManager->FillNtupleDColumn(51, fEOD4Mom[0]);
    analysisManager->FillNtupleDColumn(52, fEOD4Mom[1]);
    analysisManager->FillNtupleDColumn(53, fEOD4Mom[2]);
    analysisManager->FillNtupleDColumn(54, fEOD4Mom[3]);
    analysisManager->FillNtupleIColumn(55, fEODEntry);
    analysisManager->FillNtupleDColumn(56, fEODDistance);

    // Photon Tracking
    //
    analysisManager->FillNtupleIColumn(57, fPhIDMu);
    analysisManager->FillNtupleIColumn(61, fPhIDE);
    analysisManager->FillNtupleIColumn(65, fPhODMu);
    analysisManager->FillNtupleIColumn(69, fPhODE);

    analysisManager->AddNtupleRow();
}

void CosmicMuEventAction::ResetData()
{
    // Every variables that can be negative is set to a very large negative
    // number. Variables that can't be negative are set to 0.
    for (int i = 0; i < 4; i++)
    {
        fMuID4Pos[i] = - INT_MAX;
        fMuID4Mom[i] = - INT_MAX;
        fMuOD4Pos[i] = - INT_MAX;
        fMuOD4Mom[i] = - INT_MAX;
        fE4Pos[i] = - INT_MAX;
        fE4Mom[i] = - INT_MAX;
        fEID4Pos[i] = - INT_MAX;
        fEID4Mom[i] = - INT_MAX;
        fEOD4Pos[i] = - INT_MAX;
        fEOD4Mom[i] = - INT_MAX;
    }

    fMuDecay = -2;
    fMuIDEntry = 0;
    fMuIDDistance = 0.;
    fMuODEntry = 0;
    fMuIDDistance = 0.;
    fMuODEntry = 0;
    fMuODDistance = 0.;
    fEIDEntry = 0;
    fEIDDistance = 0.;
    fEODEntry = 0;
    fEODDistance = 0.;
    fPhIDMu = 0;
    fPhIDE = 0;
    fPhODMu = 0;
    fPhODE = 0;

    // Deletes all vector entries and resets size to 0
    //
    fPhIDMuTime.clear();
    fPhIDMuWavelength.clear();
    fPhIDMuProcess.clear();
    fPhIDETime.clear();
    fPhIDEWavelength.clear();
    fPhIDEProcess.clear();
    fPhODMuTime.clear();
    fPhODMuWavelength.clear();
    fPhODMuProcess.clear();
    fPhODETime.clear();
    fPhODEWavelength.clear();
    fPhODEProcess.clear();
}
