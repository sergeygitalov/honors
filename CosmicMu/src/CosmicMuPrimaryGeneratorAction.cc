//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// \file CosmicMuPrimaryGeneratorAction.hh
// \brief Definition of CosmicMuPrimaryGeneratorAction class

#include "CosmicMuPrimaryGeneratorAction.hh"

#include "Randomize.hh"

#include "G4Event.hh"
#include "G4GeneralParticleSource.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4ThreeVector.hh"
#include "TF1.h"
#include "G4PhysicalConstants.hh"
#include "globals.hh"
#include "G4SystemOfUnits.hh"

CosmicMuPrimaryGeneratorAction::CosmicMuPrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction()
{
    G4Gun = new G4ParticleGun();
    G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();

    G4String particleName;
    G4ParticleDefinition* particle;

    // Generate either a muon or an antimuon. The value in the if statement sets
    // the ratio
    //
    G4double mu_ratio = G4UniformRand();
    if (mu_ratio <= 0.333)
        particle = particleTable->FindParticle(particleName = "mu-");
    else
        particle = particleTable->FindParticle(particleName = "mu+");

    G4Gun->SetParticleDefinition(particle);
}

CosmicMuPrimaryGeneratorAction::~CosmicMuPrimaryGeneratorAction()
{
  delete G4Gun;
}

void CosmicMuPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
    // Position definitions
    // 60cm radius circle 40cm above
    // uniform distribution on a disk
    //
    G4double rho = std::pow(G4UniformRand(),0.5)*60.*cm;
    G4double phi_pos = twopi*G4UniformRand();
    G4double x_pos = rho*std::cos(phi_pos);
    G4double y_pos = rho*std::sin(phi_pos);
    G4double z_pos = 40.*cm;

    G4Gun->SetParticlePosition(G4ThreeVector(x_pos,y_pos,z_pos));
    
    // Kinetic energy uniformly distributed from 0 to 150 MeV
    //
    G4double Kin_energy = 150.*MeV*G4UniformRand();
    G4Gun->SetParticleEnergy(Kin_energy);

    //Momentum definitions
    //
    G4double phi_mom = twopi*G4UniformRand(); // momentum uniform in phi
    // cos zentih angle power dependance
    float power = 2; // default power dependance

    // Power depends on momentum. Used for testing.
    //
    //G4double momentum = pow((Kin_energy)*(Kin_energy) + 
    //                        2*Kin_energy*105.66,0.5);
    //if (momentum < 0.1)
    //  power = 16;
    //else
    //  power = 0.215*std::pow(std::log10(momentum/1000),2)-
    //                         2.1583*std::log10(momentum/1000)+3.5;

    // Distribute muons according to distribution
    //
    G4double cos_theta = std::pow(1-G4UniformRand(),1/(power+1));

    G4double x_mom = std::cos(phi_mom)*pow(1-pow(cos_theta,2),0.5); // sin_theta
    G4double y_mom = std::sin(phi_mom)*pow(1-pow(cos_theta,2),0.5); // sin_theta
    G4double z_mom = -cos_theta;

    G4Gun->SetParticleMomentumDirection(G4ThreeVector(x_mom,y_mom,z_mom));

    //create vertex
    G4Gun->GeneratePrimaryVertex(anEvent);
}

