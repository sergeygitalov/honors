//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// \file CosmicMuRunMessenger.cc
// \brief Implementation of CosmicMuRunMessenger class

#include "CosmicMuRunMessenger.hh"

#include "CosmicMuRunAction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"


CosmicMuRunMessenger::CosmicMuRunMessenger(CosmicMuRunAction* aRun)
: fRunAction(aRun)
{
  fFileNameDir = new G4UIdirectory("/CosmicMu/fileName/");
  fFileNameDir->SetGuidance("Run filename control");

  fFileNameCmd = new G4UIcmdWithAString("/CosmicMu/fileName/fileName",this);
  fFileNameCmd->SetGuidance("Set name of output ROOT file");
}

CosmicMuRunMessenger::~CosmicMuRunMessenger()
{
  delete fFileNameCmd;
  delete fFileNameDir;
}

void CosmicMuRunMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
  if( command == fFileNameCmd ) {
    fRunAction->SetFileName(newValue);
  }
}

