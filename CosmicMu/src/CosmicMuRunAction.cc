//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// \file CosmicMuRunAction.cc
// \brief Implementation of CosmicMuRunAction class

#include "CosmicMuRunAction.hh"

#include "G4Timer.hh"
#include "g4root.hh"
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"
#include "G4Run.hh"

#include "CosmicMuRunMessenger.hh"
#include "CosmicMuEventAction.hh"
#include "CosmicMuDetectorConstruction.hh"

CosmicMuRunAction::CosmicMuRunAction(CosmicMuEventAction* eventAction)
  : G4UserRunAction()
{
    timer = new G4Timer;

    // The messenger sets up the root file to be saved
    //
    runMessenger = new CosmicMuRunMessenger(this);

    G4cout << "Initial file name: " << fFileName << G4endl;

    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

    analysisManager->SetVerboseLevel(1);
    analysisManager->SetNtupleMerging(true);

    // Creating ntuple

    analysisManager->CreateNtuple("data","data");

    // Muon tracking

    analysisManager->CreateNtupleDColumn("fMuPosX");
    analysisManager->CreateNtupleDColumn("fMuPosY");
    analysisManager->CreateNtupleDColumn("fMuPosZ");
    analysisManager->CreateNtupleDColumn("fMuEnergy");
    analysisManager->CreateNtupleDColumn("fMuMomX");
    analysisManager->CreateNtupleDColumn("fMuMomY");
    analysisManager->CreateNtupleDColumn("fMuMomZ");
    analysisManager->CreateNtupleIColumn("fMuCharge");
    analysisManager->CreateNtupleIColumn("fMuDecay");

    analysisManager->CreateNtupleDColumn("fMuIDTime");
    analysisManager->CreateNtupleDColumn("fMuIDPosX");
    analysisManager->CreateNtupleDColumn("fMuIDPosY");
    analysisManager->CreateNtupleDColumn("fMuIDPosZ");
    analysisManager->CreateNtupleDColumn("fMuIDEnergy");
    analysisManager->CreateNtupleDColumn("fMuIDMomX");
    analysisManager->CreateNtupleDColumn("fMuIDMomY");
    analysisManager->CreateNtupleDColumn("fMuIDMomZ");
    analysisManager->CreateNtupleIColumn("fMuIDEntry");
    analysisManager->CreateNtupleDColumn("fMuIDDistance");

    analysisManager->CreateNtupleDColumn("fMuODTime");
    analysisManager->CreateNtupleDColumn("fMuODPosX");
    analysisManager->CreateNtupleDColumn("fMuODPosY");
    analysisManager->CreateNtupleDColumn("fMuODPosZ");
    analysisManager->CreateNtupleDColumn("fMuODEnergy");
    analysisManager->CreateNtupleDColumn("fMuODMomX");
    analysisManager->CreateNtupleDColumn("fMuODMomY");
    analysisManager->CreateNtupleDColumn("fMuODMomZ");
    analysisManager->CreateNtupleIColumn("fMuODEntry");
    analysisManager->CreateNtupleDColumn("fMuODDistance");

    // Electron Tracking

    analysisManager->CreateNtupleDColumn("fETime");
    analysisManager->CreateNtupleDColumn("fEPosX");
    analysisManager->CreateNtupleDColumn("fEPosY");
    analysisManager->CreateNtupleDColumn("fEPosZ");
    analysisManager->CreateNtupleDColumn("fEEnergy");
    analysisManager->CreateNtupleDColumn("fEMomX");
    analysisManager->CreateNtupleDColumn("fEMomY");
    analysisManager->CreateNtupleDColumn("fEMomZ");

    analysisManager->CreateNtupleDColumn("fEIDTime");
    analysisManager->CreateNtupleDColumn("fEIDPosX");
    analysisManager->CreateNtupleDColumn("fEIDPosY");
    analysisManager->CreateNtupleDColumn("fEIDPosZ");
    analysisManager->CreateNtupleDColumn("fEIDEnergy");
    analysisManager->CreateNtupleDColumn("fEIDMomX");
    analysisManager->CreateNtupleDColumn("fEIDMomY");
    analysisManager->CreateNtupleDColumn("fEIDMomZ");
    analysisManager->CreateNtupleIColumn("fEIDEntry");
    analysisManager->CreateNtupleDColumn("fEIDDistance");

    analysisManager->CreateNtupleDColumn("fEODTime");
    analysisManager->CreateNtupleDColumn("fEODPosX");
    analysisManager->CreateNtupleDColumn("fEODPosY");
    analysisManager->CreateNtupleDColumn("fEODPosZ");
    analysisManager->CreateNtupleDColumn("fEODEnergy");
    analysisManager->CreateNtupleDColumn("fEODMomX");
    analysisManager->CreateNtupleDColumn("fEODMomY");
    analysisManager->CreateNtupleDColumn("fEODMomZ");
    analysisManager->CreateNtupleIColumn("fEODEntry");
    analysisManager->CreateNtupleDColumn("fEODDistance");

    // Photon Tracking

    analysisManager->CreateNtupleIColumn("fPhIDMu");
    analysisManager->CreateNtupleDColumn("fPhIDMuTime",
        eventAction->GetfPhIDMuTime());
    analysisManager->CreateNtupleDColumn("fPhIDMuWavelength",
        eventAction->GetfPhIDMuWavelength());
    analysisManager->CreateNtupleIColumn("fPhIDMuProcess",
        eventAction->GetfPhIDMuProcess());
    analysisManager->CreateNtupleIColumn("fPhIDE");
    analysisManager->CreateNtupleDColumn("fPhIDETime",
        eventAction->GetfPhIDETime());
    analysisManager->CreateNtupleDColumn("fPhIDEWavelength",
        eventAction->GetfPhIDEWavelength());
    analysisManager->CreateNtupleIColumn("fPhIDEProcess",
        eventAction->GetfPhIDEProcess());

    analysisManager->CreateNtupleIColumn("fPhODMu");
    analysisManager->CreateNtupleDColumn("fPhODMuTime",
        eventAction->GetfPhODMuTime());
    analysisManager->CreateNtupleDColumn("fPhODMuWavelength",
        eventAction->GetfPhODMuWavelength());
    analysisManager->CreateNtupleIColumn("fPhODMuProcess",
        eventAction->GetfPhODMuProcess());
    analysisManager->CreateNtupleIColumn("fPhODE");
    analysisManager->CreateNtupleDColumn("fPhODETime",
        eventAction->GetfPhODETime());
    analysisManager->CreateNtupleDColumn("fPhODEWavelength",
        eventAction->GetfPhODEWavelength());
    analysisManager->CreateNtupleIColumn("fPhODEProcess",
        eventAction->GetfPhODEProcess());

    analysisManager->FinishNtuple();
}

CosmicMuRunAction::~CosmicMuRunAction()
{
    delete runMessenger;
    delete timer;
    delete G4AnalysisManager::Instance();
}

void CosmicMuRunAction::BeginOfRunAction(const G4Run* aRun)
{
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

    analysisManager->OpenFile(fFileName);

    G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;
    timer->Start();
}

void CosmicMuRunAction::EndOfRunAction(const G4Run* aRun)
{
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

    G4cout << "Ending run" << G4endl;
    timer->Stop();
    G4cout << "Number of events = " << aRun->GetNumberOfEvent() << G4endl;
    G4cout << "Elapsed time: " << *timer << G4endl;

    G4cout << "Writing file" << G4endl;
    analysisManager->Write();
    G4cout << "Closing File" << G4endl;
    analysisManager->CloseFile();
}

void CosmicMuRunAction::SetFileName(G4String name)
{
    fFileName = name; 
    G4cout << "Setting file name to " << fFileName << G4endl;
}

