// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes, nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// \file CosmicMuDetectorConstruction.cc
// \brief Implementation of CosmicMuDetectorConstruction class
//
#include "CosmicMuDetectorConstruction.hh"

#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4ElementTable.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4LogicalSkinSurface.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4Cons.hh"
#include "G4Tubs.hh"
#include "G4Torus.hh"
#include "G4LogicalVolume.hh"
#include "G4RotationMatrix.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4PVPlacement.hh"
#include "G4OpBoundaryProcess.hh"
#include "G4VisAttributes.hh"
#include "G4OpticalSurface.hh"
#include "G4UnionSolid.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

// Nist manager, colors, and constants are immediately initialized in the
// constructor
//
CosmicMuDetectorConstruction::CosmicMuDetectorConstruction()
  : G4VUserDetectorConstruction(), fNist(G4NistManager::Instance()), 
    fBlue(0, 0, 1), fCyan(0, 1, 1), fGray(0.5, 0.5, 0.5), fGreen(0, 1, 0), 
    fRed(1, 0, 0), fWhite(1, 1, 1), fYellow(1, 1, 0)
{
    SetConstants();
}

CosmicMuDetectorConstruction::~CosmicMuDetectorConstruction()
{
    delete[] fPhotonEnergy;
    delete[] fAbsorption;
}

G4VPhysicalVolume* CosmicMuDetectorConstruction::Construct()
{
    ConstructElements();
    ConstructMaterials();
    ConstructMaterialPropertiesTables();
    ConstructVolumes();
    ConstructSurfaces();

    //---------- Visualization Attributes ----------//

    G4VisAttributes *DetVisAtt = new G4VisAttributes(fRed);
    DetVisAtt->SetVisibility(true);
    DetVisAtt->SetForceWireframe(true);
    fODPMTDetector_log->SetVisAttributes(DetVisAtt);
    fIDPMTDetector_log->SetVisAttributes(DetVisAtt);

    // Always return the physical World
    //
    return fWorld_phys;
}

void CosmicMuDetectorConstruction::SetConstants()
{
    fPhotonEnergy = new G4double[32] {2.034*eV, 2.068*eV, 2.103*eV, 2.139*eV,
                                      2.177*eV, 2.216*eV, 2.256*eV, 2.298*eV,
                                      2.341*eV, 2.386*eV, 2.433*eV, 2.481*eV,
                                      2.532*eV, 2.585*eV, 2.640*eV, 2.697*eV,
                                      2.757*eV, 2.820*eV, 2.885*eV, 2.954*eV,
                                      3.026*eV, 3.102*eV, 3.181*eV, 3.265*eV,
                                      3.353*eV, 3.446*eV, 3.545*eV, 3.649*eV,
                                      3.760*eV, 3.877*eV, 4.002*eV, 4.136*eV};

    fAbsConstant = 1;

    fAbsorption = new G4double[32] {3.448*m, 4.082*m, 6.329*m, 9.174*m, 
                                    12.346*m, 13.889*m, 15.152*m, 17.241*m,
                                    18.868*m, 20.000*m, 26.316*m, 35.714*m,
                                    45.455*m, 47.619*m, 52.632*m, 52.632*m,
                                    55.556*m, 52.632*m, 52.632*m, 47.619*m,
                                    45.455*m, 41.667*m, 37.037*m, 33.333*m,
                                    30.000*m, 28.500*m, 27.000*m, 24.500*m,
                                    22.000*m, 19.500*m, 17.500*m, 14.500*m};

    fScintilFast = new G4double[32];

    fScintilSlow = new G4double[32] {0.01, 1.00, 2.00, 3.00, 4.00, 5.00, 6.00,
                                     7.00, 8.00, 9.00, 8.00, 7.00, 6.00, 4.00,
                                     3.00, 2.00, 1.00, 0.01, 1.00, 2.00, 3.00,
                                     4.00, 5.00, 6.00, 7.00, 8.00, 9.00, 8.00,
                                     7.00, 6.00, 5.00, 4.00};

    for (int i = 0; i < 32; i++)
    {
        fAbsorption[i] = fAbsConstant;
        fScintilFast[i] = 1.;
    }
}

void CosmicMuDetectorConstruction::ConstructElements()
{
    fH = fNist->FindOrBuildElement("H");
    fB = fNist->FindOrBuildElement("B");
    fC = fNist->FindOrBuildElement("C");
    fN = fNist->FindOrBuildElement("N");
    fO = fNist->FindOrBuildElement("O");
    fSi = fNist->FindOrBuildElement("Si");
    fP = fNist->FindOrBuildElement("P");
    fS = fNist->FindOrBuildElement("S");
    fCr = fNist->FindOrBuildElement("Cr");
    fMn = fNist->FindOrBuildElement("Mn");
    fFe = fNist->FindOrBuildElement("Fe");
    fNi = fNist->FindOrBuildElement("Ni");
}

void CosmicMuDetectorConstruction::ConstructMaterials()
{
    // All materials are initialized first in alphabetical order

    fAir = fNist->FindOrBuildMaterial("G4_AIR");
    fAluminium = fNist->FindOrBuildMaterial("G4_Al");
    fBoratedPolyethylene = new G4Material("BoratedPolyethylene", 1.19*g/cm3, 2);
    fCarbon = fNist->FindOrBuildMaterial("G4_GRAPHITE");
    fFreon = new G4Material("Freon", 176.4*mg/cm3, 2);
    fGlass = new G4Material("Glass", 14, 28.08*g/mole, 2.7*g/cm3);
    fIsopropyl = fNist->FindOrBuildMaterial("G4_N-PROPYL_ALCOHOL");
    fMineralOil = new G4Material("MineralOil", 0.8356*g/cm3, 2);
    fNE_216 = new G4Material("NE-216", 0.885*g/cm3, 3);
    fPlexiglass = fNist->FindOrBuildMaterial("G4_PLEXIGLASS");
    fPolyethylene = fNist->FindOrBuildMaterial("G4_POLYETHYLENE");
    fPolystyrene = fNist->FindOrBuildMaterial("G4_POLYSTYRENE");
    fPOPOP = new G4Material("POPOP", 364.4*g/mole, 4);
    fPPO = new G4Material("PPO", 1.06*g/cm3, 4);
    fSilicon = new G4Material("Silicon", 14, 28.08*g/mole, 2.7*g/cm3);
    fSteel = new G4Material("Stainleess steel", 8.03*g/cm3, 9);
    fToluene = fNist->FindOrBuildMaterial("G4_TOLUENE");
    fVacuum = new G4Material("Vacuum", 1, 1*g/mole, 1.e-5*g/cm3);
    fWater = fNist->FindOrBuildMaterial("G4_WATER");

    // Elements and materials are added afterwards to prevent passing an
    // uninitialized pointer

    fBoratedPolyethylene->AddMaterial(fPolyethylene, 0.7);
    fBoratedPolyethylene->AddElement(fB, 0.3);
    fFreon->AddElement(fH, 4);
    fFreon->AddElement(fC, 8);
    fMineralOil->AddElement(fC, 1);
    fMineralOil->AddElement(fH, 2);
    fNE_216->AddMaterial(fToluene, 89.12*perCent);
    fNE_216->AddMaterial(fPPO, 10.66*perCent);
    fNE_216->AddMaterial(fPOPOP, 0.22*perCent);
    fPOPOP->AddElement(fC, 24);
    fPOPOP->AddElement(fH, 16);
    fPOPOP->AddElement(fN, 2);
    fPOPOP->AddElement(fO, 2);
    fPPO->AddElement(fC, 15);
    fPPO->AddElement(fH, 11);
    fPPO->AddElement(fN, 1);
    fPPO->AddElement(fO, 1);
    fSteel->AddElement(fC, .08*perCent);
    fSteel->AddElement(fMn, 2*perCent);
    fSteel->AddElement(fP, .045*perCent);
    fSteel->AddElement(fS, .03*perCent);
    fSteel->AddElement(fSi, .75*perCent);
    fSteel->AddElement(fCr, 20*perCent);
    fSteel->AddElement(fNi, 12*perCent);
    fSteel->AddElement(fN, .1*perCent);
    fSteel->AddElement(fFe, 64.995*perCent);
}

//----------------------------------------------------------------------------//
// Material Properties Tables
//----------------------------------------------------------------------------//

void CosmicMuDetectorConstruction::ConstructMaterialPropertiesTables()
{
    ConstructAluminiumMPT();
    ConstructGlassMPT();
    ConstructIsopropylMPT();
    ConstructMineralOilMPT();
    ConstructNE_216MPT();
    ConstructPlexiglassMPT();
    ConstructPolystyreneMPT();
    ConstructWaterMPT();
}

void CosmicMuDetectorConstruction::ConstructAluminiumMPT()
{
    fAluminiumMPT = new G4MaterialPropertiesTable();

    G4double backscatter[32];
    G4double reflectivity[32] = {0.9100, 0.9110, 0.9120, 0.9125, 0.9130, 0.9140,
                                 0.9150, 0.9160, 0.9165, 0.9170, 0.9175, 0.9180,
                                 0.9190, 0.9195, 0.9200, 0.9210, 0.9220, 0.9224,
                                 0.9228, 0.9232, 0.9236, 0.9240, 0.9245, 0.9250,
                                 0.9250, 0.9250, 0.9250, 0.9250, 0.9245, 0.9240,
                                 0.9235, 0.9230};
    G4double efficiency[32], rindex[32], specularLobe[32],
                     specularSpike[32];

    for (int i = 0; i < 32; i++)
    {
        backscatter[i] = 0.1;
        efficiency[i] = 0.5 + i*(0.4/32);
        rindex[i] = 1.3 + i*(0.1/32);
        specularLobe[i] = 0.3;
        specularSpike[i] = 0.2;
    }

    fAluminiumMPT->AddProperty("BACKSCATTERCONSTANT", fPhotonEnergy, 
                               backscatter, 32);
    fAluminiumMPT->AddProperty("REFLECTIVITY", fPhotonEnergy, reflectivity, 32);
    fAluminiumMPT->AddProperty("EFFICIENCY", fPhotonEnergy, efficiency, 32);
    fAluminiumMPT->AddProperty("RINDEX", fPhotonEnergy, rindex, 32);
    fAluminiumMPT->AddProperty("SPECULARLOBECONSTANT", fPhotonEnergy,
                               specularLobe, 32);
    fAluminiumMPT->AddProperty("SPECULARSPIKECONSTANT", fPhotonEnergy, 
                               specularSpike, 32);

    fAluminium->SetMaterialPropertiesTable(fAluminiumMPT);
}

void CosmicMuDetectorConstruction::ConstructGlassMPT()
{
    fGlassMPT = new G4MaterialPropertiesTable();

    G4double backscatter[32], efficiency[32], rindex[32],
                     absLength[32];

    for (int i = 0; i < 32; i++)
    {
        backscatter[i] = 0.1;
        efficiency[i] = 0.5;
        rindex[i] = 1.49;
        absLength[i] = 420.*cm;
    }

    fGlassMPT->AddProperty("BACKSCATTERCONSTANT", fPhotonEnergy, backscatter,
                           32);
    fGlassMPT->AddProperty("EFFICIENCY", fPhotonEnergy, efficiency, 32);
    fGlassMPT->AddProperty("RINDEX", fPhotonEnergy, rindex, 32);
    fGlassMPT->AddProperty("ABSLENGTH", fPhotonEnergy, absLength, 32);

    fGlass->SetMaterialPropertiesTable(fGlassMPT);
}

void CosmicMuDetectorConstruction::ConstructIsopropylMPT()
{
    fIsopropylMPT = new G4MaterialPropertiesTable();

    G4double rindex[32];

    for (int i = 0; i < 32; i++)
    {
        G4double wavelength = h_Planck*c_light/(fPhotonEnergy[i] * um);
        G4double n = 1.374 + 0.00306/std::pow(wavelength, 2) + 
                     0.00002/std::pow(wavelength, 4);
        rindex[i] = n;
    }

    fIsopropylMPT->AddProperty("RINDEX", fPhotonEnergy, rindex, 32);
    fIsopropylMPT->AddProperty("ABSLENGTH", fPhotonEnergy, fAbsorption, 32);
    fIsopropylMPT->AddProperty("FASTCOMPONENT", fPhotonEnergy, fScintilFast,
                               32);
    fIsopropylMPT->AddProperty("SLOWCOMPONENT", fPhotonEnergy, fScintilSlow, 
                               32);

    fIsopropylMPT->AddConstProperty("SCINTILLATIONYIELD", 50./MeV);
    fIsopropylMPT->AddConstProperty("RESOLUTIONSCALE", 1.0);
    fIsopropylMPT->AddConstProperty("FASTTIMECONSTANT", 1.*ns);
    fIsopropylMPT->AddConstProperty("SLOWTIMECONSTANT", 10.*ns);
    fIsopropylMPT->AddConstProperty("YIELDRATIO", 0.8);

    fIsopropyl->SetMaterialPropertiesTable(fIsopropylMPT);
}

void CosmicMuDetectorConstruction::ConstructMineralOilMPT()
{
    fMineralOilMPT = new G4MaterialPropertiesTable();

    G4double rindex[32];
    G4double absorption[32];
    G4double fast[32];
    for (int i = 0; i < 32; i++)
    {
        rindex[i] = 1.4620;
        absorption[i] = 26.45*m;
        fast[i] = 19.0;
    }

    fMineralOilMPT->AddProperty("RINDEX", fPhotonEnergy, rindex, 32);
    fMineralOilMPT->AddProperty("ABSLENGTH", fPhotonEnergy, absorption, 32);
    fMineralOilMPT->AddProperty("FASTCOMPONENT", fPhotonEnergy, fast, 32);

    fMineralOilMPT->AddConstProperty("SCINTILLATIONYIELD", 4.7/MeV);
    fMineralOilMPT->AddConstProperty("RESOLUTIONSCALE", 1.0);
    fMineralOilMPT->AddConstProperty("FASTTIMECONSTANT", 1.*ns);

    fMineralOil->SetMaterialPropertiesTable(fMineralOilMPT);
}

void CosmicMuDetectorConstruction::ConstructNE_216MPT()
{
    fNE_216MPT = new G4MaterialPropertiesTable();

    G4double rindex[32];
    G4double absorption[32];
    G4double fast[32];
    //G4double slow[NumEntries];

    for (int i = 0; i < 32; i++)
    {
        rindex[i] = 1.523;
        absorption[i] = 1.5*m;
        fast[i] = 3.5;
        //slow[i] = 17.61;
    }

    fNE_216MPT->AddProperty("RINDEX", fPhotonEnergy, rindex, 32);
    fNE_216MPT->AddProperty("ABSLENGTH", fPhotonEnergy, absorption, 32);
    fNE_216MPT->AddProperty("FASTCOMPONENT", fPhotonEnergy, fast, 32);
    //fNE_216MPT->AddProperty("SLOWCOMPONENT", scintEnergies, slow, 32);

    fNE_216MPT->AddConstProperty("SCINTILLATIONYIELD", 13572/MeV);
    fNE_216MPT->AddConstProperty("RESOLUTIONSCALE", 1.0);
    fNE_216MPT->AddConstProperty("FASTTIMECONSTANT", 1.*ns);
    //fNE_216MPT->AddConstProperty("SLOWTIMECONSTANT", 1.*ns);
    //fNE_216MPT->AddConstProperty("YIELDRATIO", 0.895);

    fNE_216->SetMaterialPropertiesTable(fNE_216MPT);
}

void CosmicMuDetectorConstruction::ConstructPlexiglassMPT()
{
    fPlexiglassMPT = new G4MaterialPropertiesTable();

    G4double rindex[32];
    G4double absorption[32];

    for (int i = 0; i < 32; i++)
    {
        rindex[i]= 1.49;

        if (i < 13)
            absorption[i] = 4.0*m;
        else
            absorption[i] = 1.*mm;
    }

    fPlexiglassMPT->AddProperty("RINDEX", fPhotonEnergy, rindex, 32);
    fPlexiglassMPT->AddProperty("ABSLENGTH", fPhotonEnergy, absorption, 32);
    fPlexiglass->SetMaterialPropertiesTable(fPlexiglassMPT);
}

void CosmicMuDetectorConstruction::ConstructPolystyreneMPT()
{
    fPolystyreneMPT = new G4MaterialPropertiesTable();

    G4double rindex[32];
    G4double absorption[32];
    for (int i = 0; i < 32; i++)
    {
        rindex[i] = 1.59;

        if (i < 13)
            absorption[i]=4.0*m;
        else
            absorption[i]= 1*mm;
    }

    G4double emission_intensity[32] = {0.10, 0.10, 0.15, 0.19, 0.25, 0.33, 0.47,
                                       0.54, 0.66, 0.87, 1.00, 0.78, 0.29, 0.03,
                                       0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00,
                                       0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00,
                                       0.00, 0.00, 0.00, 0.00};

    fPolystyreneMPT->AddProperty("RINDEX", fPhotonEnergy, rindex, 32);
    fPolystyreneMPT->AddProperty("WLSABSLENGTH", fPhotonEnergy, absorption, 32);
    fPolystyreneMPT->AddProperty("WLSCOMPONENT", fPhotonEnergy,
                                 emission_intensity, 32);
    fPolystyreneMPT->AddConstProperty("WLSTIMECONSTANT", 0.5*ns);
    fPolystyrene->SetMaterialPropertiesTable(fPolystyreneMPT);
}

void CosmicMuDetectorConstruction::ConstructWaterMPT()
{
    fWaterMPT = new G4MaterialPropertiesTable();

    G4double rindex[32] = {1.3435, 1.3440, 1.3445, 1.3450, 1.3455, 1.3460,
                           1.3465, 1.3470, 1.3475, 1.3480, 1.3485, 1.3492,
                           1.3500, 1.3505, 1.3510, 1.3518, 1.3522, 1.3530,
                           1.3535, 1.3540, 1.3545, 1.3550, 1.3555, 1.3560,
                           1.3568, 1.3572, 1.3580, 1.3585, 1.3590, 1.3595,
                           1.3600, 1.3608};

    fWaterMPT->AddProperty("RINDEX", fPhotonEnergy, rindex, 32);
    fWaterMPT->AddProperty("ABSLENGTH", fPhotonEnergy, fAbsorption, 32);
    //fWaterMPT->AddProperty("FASTCOMPONENT", fPhotonEnergy, fScintilFast, 32);
    //fWaterMPT->AddProperty("SLOWCOMPONENT", fPhotonEnergy, fScintilSlow, 32);

    //fWaterMPT->AddConstProperty("SCINTILLATIONYIELD", 50./MeV);
    //fWaterMPT->AddConstProperty("RESOLUTIONSCALE", 1.0);
    //fWaterMPT->AddConstProperty("FASTTIMECONSTANT", 1.*ns);
    //fWaterMPT->AddConstProperty("SLOWTIMECONSTANT", 10.*ns);
    //fWaterMPT->AddConstProperty("YIELDRATIO", 0.8);

    fWater->SetMaterialPropertiesTable(fWaterMPT);
}

//----------------------------------------------------------------------------//
// Volumes
//----------------------------------------------------------------------------//

void CosmicMuDetectorConstruction::ConstructVolumes()
{
    ConstructWorld();
    //ConstructGasTank();
    //ConstructCarbonShielding();
    ConstructOuterShell();
    ConstructOuterDetector();
    ConstructInnerShell();
    ConstructInnerDetector();
    ConstructPMTs();
}

void CosmicMuDetectorConstruction::ConstructWorld()
{
    G4double x, y, z;
    x = y = z = 5.*m;

    G4VSolid* World_solid = new G4Box("World_solid", x, y, z);

    fWorld_log = new G4LogicalVolume(World_solid, fAir, "World_log");

    G4VisAttributes* World_VA = new G4VisAttributes(fWhite);

    fWorld_log->SetVisAttributes(World_VA);

    G4RotationMatrix* rot = NULL;
    const G4ThreeVector pos(0., 0., 0.);
    G4LogicalVolume* mother = NULL;

    fWorld_phys
        = new G4PVPlacement(rot, pos, fWorld_log, "World_phys", mother, false,
                            0);
}

void CosmicMuDetectorConstruction::ConstructGasTank()
{
    G4double x, y, z;
    x = y = z = 3.*m;

    G4VSolid* GasTank_box = new G4Box("GasTank_solid", x, y, z);

    fGasTank_log = new G4LogicalVolume(GasTank_box, fAir, "GasTank_log");

    G4VisAttributes* GasTank_VA = new G4VisAttributes(fWhite);

    fGasTank_log->SetVisAttributes(GasTank_VA);

    G4RotationMatrix* rot = NULL;
    const G4ThreeVector pos(0., 0., 0.);
    G4LogicalVolume* mother = fWorld_log;

    G4VPhysicalVolume* GasTank_phys
        = new G4PVPlacement(rot, pos, fGasTank_log, "GasTank_phys", mother,
                            false, 0);
}

void CosmicMuDetectorConstruction::ConstructCarbonShielding()
{
    G4double radius = 28.44*cm;
    G4double height = 113.76*cm;

    G4VSolid* CarbonShielding_solid
        = new G4Tubs("CarbonShielding_solid", 0, radius, 0.5*height, 0*deg,
                     360*deg);

    G4LogicalVolume* CarbonShielding_log
        = new G4LogicalVolume(CarbonShielding_solid, fCarbon,
                              "CarbonShielding_log");

    G4VisAttributes* CarbonShielding_VA = new G4VisAttributes(fGray);

    CarbonShielding_log->SetVisAttributes(CarbonShielding_VA);

    G4RotationMatrix* rot = NULL;
    const G4ThreeVector pos(0., 0., 0.);
    G4LogicalVolume* mother = fGasTank_log;

    G4VPhysicalVolume* CarbonShielding_phys
    = new G4PVPlacement(rot, pos, CarbonShielding_log, "CarbonShielding_phys",
                        mother, false, 0);
}

void CosmicMuDetectorConstruction::ConstructOuterShell()
{
    G4double radius = 15.716*cm;
    G4double height = 15.096*cm;

    G4VSolid* OuterShellTop
        = new G4Tubs("OuterShellTop", 0, radius, 0.5*height, 0*deg, 360*deg);

    G4VSolid* OuterShellBottom
        = new G4Sphere("OuterShellBottom", 0, radius, 0*deg, 360*deg, 90*deg,
                       180*deg);

    G4ThreeVector OuterShellAttach(0, 0, 0.5*height);

    G4UnionSolid* OuterShell_solid
        = new G4UnionSolid("OuterShell_solid", OuterShellBottom, OuterShellTop,
                           0, OuterShellAttach);

    fOuterShell_log
        = new G4LogicalVolume(OuterShell_solid, fAluminium, "OuterShell_log");

    G4VisAttributes* OuterShell_VA = new G4VisAttributes(fBlue);

    fOuterShell_log->SetVisAttributes(OuterShell_VA);

    G4RotationMatrix* rot = NULL;
    const G4ThreeVector pos(0., 0., 0.);
    G4LogicalVolume* mother = fWorld_log;

    fOuterShell_phys
        = new G4PVPlacement(rot, pos, fOuterShell_log, "OuterShell_phys",
                            mother, false, 0);
}

void CosmicMuDetectorConstruction::ConstructOuterDetector()
{
    G4double radius = 15.24*cm;
    G4double height = 14.62*cm;

    G4VSolid* OuterDetectorTop
        = new G4Tubs("OuterDetectorTop", 0, radius, 0.5*height, 0*deg, 360*deg);

    G4VSolid* OuterDetectorBottom
        = new G4Sphere("OuterDetectorBottom", 0, radius, 0*deg, 360*deg, 90*deg,
                       180*deg);

    G4ThreeVector OuterDetectorAttach(0, 0, 0.5*height);

    G4UnionSolid* OuterDetector_solid
        = new G4UnionSolid("OuterDetector_solid", OuterDetectorBottom,
                           OuterDetectorTop, 0, OuterDetectorAttach);

    fOuterDetector_log
        = new G4LogicalVolume(OuterDetector_solid, fNE_216,
                              "OuterDetector_log");

    G4VisAttributes* OuterDetector_VA = new G4VisAttributes(fGreen);

    fOuterDetector_log->SetVisAttributes(OuterDetector_VA);

    G4RotationMatrix* rot = NULL;
    const G4ThreeVector pos(0., 0., 0.);
    G4LogicalVolume* mother = fOuterShell_log;

    fOuterDetector_phys
        = new G4PVPlacement(rot, pos, fOuterDetector_log, "OuterDetector_phys",
                            mother, false, 0);
}

void CosmicMuDetectorConstruction::ConstructInnerShell()
{
    G4double radius = 12.75*cm;
    G4double height = 12.13*cm;

    G4VSolid* InnerShellTop
        = new G4Tubs("InnerShellTop", 0, radius, 0.5*height, 0*deg, 360*deg);

    G4VSolid* InnerShellBottom
        = new G4Sphere("InnerShellBottom", 0, radius, 0*deg, 360*deg, 90*deg,
                       180*deg);

    G4ThreeVector InnerShellAttach(0, 0, 0.5*height);

    G4UnionSolid* InnerShell_solid
        = new G4UnionSolid("InnerShell_solid", InnerShellBottom, InnerShellTop,
                           0, InnerShellAttach);

    fInnerShell_log
        = new G4LogicalVolume(InnerShell_solid, fSteel, "InnerShell_log");

    G4VisAttributes* InnerShell_VA = new G4VisAttributes(fBlue);

    fInnerShell_log->SetVisAttributes(InnerShell_VA);

    G4RotationMatrix* rot = NULL;
    const G4ThreeVector pos(0., 0., 0.);
    G4LogicalVolume* mother = fOuterDetector_log;

    fInnerShell_phys
        = new G4PVPlacement(rot, pos, fInnerShell_log, "InnerShell_phys",
                            mother, false, 0);
}

void CosmicMuDetectorConstruction::ConstructInnerDetector()
{
    G4double radius = 12.7*cm;
    G4double height = 12.08*cm;

    G4VSolid* InnerDetectorTop
        = new G4Tubs("InnerDetectorTop", 0, radius, 0.5*height, 0*deg, 360*deg);

    G4VSolid* InnerDetectorBottom
        = new G4Sphere("InnerDetectorBottom", 0, radius, 0*deg, 360*deg, 90*deg,
                       180*deg);

    G4ThreeVector InnerDetectorAttach(0, 0, 0.5*height);

    G4UnionSolid* InnerDetector_solid
        = new G4UnionSolid("InnerDetector_solid", InnerDetectorBottom, 
                           InnerDetectorTop, 0, InnerDetectorAttach);

    fInnerDetector_log
        = new G4LogicalVolume(InnerDetector_solid, fMineralOil,
                              "InnerDetector_log");

    G4VisAttributes* InnerDetector_VA = new G4VisAttributes(fWhite);

    fInnerDetector_log->SetVisAttributes(InnerDetector_VA);

    G4RotationMatrix* rot = NULL;
    const G4ThreeVector pos(0., 0., 0.);
    G4LogicalVolume* mother = fInnerShell_log;

    fInnerDetector_phys
        = new G4PVPlacement(rot, pos, fInnerDetector_log, "InnerDetector_phys",
                            mother, false, 0);
}

// Since PMTs are made of multiple materials, they are all constructed
// separately
//
void CosmicMuDetectorConstruction::ConstructPMTs()
{
    ConstructPMTShell();
    ConstructPMTVacuum();
    ConstructPMTGlassCover();
    ConstructPMTDetector();
}

void CosmicMuDetectorConstruction::ConstructPMTShell()
{
    G4double radius = 2.54*cm;
    G4double height = 15.21*cm;

    G4VSolid* PMTShell_solid
        = new G4Tubs("PMTShell_solid", 0, radius, 0.5*height, 0*deg, 360*deg);

    G4LogicalVolume* PMTShell_log
        = new G4LogicalVolume(PMTShell_solid, fGlass, "PMTShell_log");

    G4VisAttributes* PMTShell_VA = new G4VisAttributes(fYellow);

    PMTShell_log->SetVisAttributes(PMTShell_VA);

    G4VPhysicalVolume* PMTShell_phys;

    G4double outer_height = 22.225*cm;
    G4double inner_height = 19.685*cm;

    PlacePMTComponent(PMTShell_phys, PMTShell_log, "PMTShell_phys",
                      outer_height, inner_height, "both");
}

void CosmicMuDetectorConstruction::ConstructPMTVacuum()
{
    G4double radius = 2.2225*cm;
    G4double height = 14.58*cm;

    G4VSolid* PMTVacuum_solid
        = new G4Tubs("PMTVacuum_solid", 0, radius, 0.5*height, 0*deg, 360*deg);

    G4LogicalVolume* PMTVacuum_log
    = new G4LogicalVolume(PMTVacuum_solid, fVacuum, "PMTVacuum_log");

    G4VisAttributes* PMTVacuum_VA = new G4VisAttributes(fCyan);

    PMTVacuum_log->SetVisAttributes(PMTVacuum_VA);

    G4VPhysicalVolume* PMTVacuum_phys;

    G4double outer_height = 21.91*cm;
    G4double inner_height = 19.37*cm;

    PlacePMTComponent(PMTVacuum_phys, PMTVacuum_log, "PMTVacuum_phys",
                      outer_height, inner_height, "both");
}

void CosmicMuDetectorConstruction::ConstructPMTGlassCover()
{
    G4double radius = 2.54*cm;
    G4double height = 0.3*cm;

    G4VSolid* PMTGlassCover_solid
        = new G4Tubs("PMTGlassCover_solid", 0, radius, 0.5*height, 0*deg,
                     360*deg);

    G4LogicalVolume* PMTGlassCover_log
        = new G4LogicalVolume(PMTGlassCover_solid, fGlass, "PMTGlassCover_log");

    G4VisAttributes* PMTGlassCover_VA = new G4VisAttributes(fYellow);

    PMTGlassCover_log->SetVisAttributes(PMTGlassCover_VA);

    G4double outer_height = 14.44*cm;
    G4double inner_height = 11.9*cm;

    PlacePMTComponent(fPMTGlassCover_phys, PMTGlassCover_log,
                      "PMTGlassCover_phys", outer_height, inner_height, "both");
}

// This is a volume used for tracking. It corresponds to the photocathode on the
// tubes
//
void CosmicMuDetectorConstruction::ConstructPMTDetector()
{
    G4double radius = 2.54*cm;
    G4double height = 0.03*cm;

    G4VSolid* PMTDetector_solid
        = new G4Tubs("PMTDetector_solid", 0, radius, 0.5*height, 0*deg,
                     360*deg);

    // Detectors are separated into OD and ID logical volumes for tracking

    fODPMTDetector_log
        = new G4LogicalVolume(PMTDetector_solid, fAluminium,
                              "ODPMTDetector_log");
    fIDPMTDetector_log
        = new G4LogicalVolume(PMTDetector_solid, fAluminium,
                              "IDPMTDetector_log");

    G4VisAttributes* PMTDetector_VA = new G4VisAttributes(fRed);

    fODPMTDetector_log->SetVisAttributes(PMTDetector_VA);
    fIDPMTDetector_log->SetVisAttributes(PMTDetector_VA);

    G4double outer_height = 14.57*cm;
    G4double inner_height = 12.065*cm;

    PlacePMTComponent(fPMTDetector_phys, fODPMTDetector_log, "PMTDetector_phys",
                      outer_height, inner_height, "outer");
    PlacePMTComponent(fPMTDetector_phys, fIDPMTDetector_log, "PMTDetector_phys",
                      outer_height, inner_height, "inner");
}

// This method is used to place all the components of the PMTs. It places them
// either in the outer detector or in the inner detector. G4String volume
// is used to specify whether it is outer or inner or both.
//
void CosmicMuDetectorConstruction::PlacePMTComponent (G4VPhysicalVolume* phys, 
                                                      G4LogicalVolume* log, 
                                                      G4String name, 
                                                      G4double outer_height, 
                                                      G4double inner_height, 
                                                      G4String volume)
{
    for (G4int i = 0; i < 4; i++)
    {
        if (volume == "outer")
        {
            G4RotationMatrix* rot = NULL;
            G4double outer_angle = i * 90*deg;
            G4double outer_radius = 12.7*cm;
            const G4ThreeVector outer_pos(cos(outer_angle)*outer_radius, 
                                          sin(outer_angle)*outer_radius, 
                                          outer_height);
            G4LogicalVolume* outer_mother = fOuterDetector_log;
            G4int outer_id = i;

            phys = new G4PVPlacement(rot, outer_pos, log, name, outer_mother,
                                     false, outer_id);
        }
        else if (volume == "inner")
        {
            G4RotationMatrix* rot = NULL;
            G4double inner_angle = (i * 90*deg) + 45*deg;
            G4double inner_radius = 6.*cm;
            const G4ThreeVector inner_pos(cos(inner_angle)*inner_radius,
                                          sin(inner_angle)*inner_radius, 
                                          inner_height);
            G4LogicalVolume* inner_mother = fInnerDetector_log;
            G4int inner_id = i + 4;

            phys = new G4PVPlacement(rot, inner_pos, log, name, inner_mother,
                                     false, inner_id);
        }
        else if (volume == "both")
        {
            G4RotationMatrix* rot = NULL;
            G4double outer_angle = i * 90*deg;
            G4double outer_radius = 12.7*cm;
            const G4ThreeVector outer_pos(cos(outer_angle)*outer_radius, 
                                          sin(outer_angle)*outer_radius, 
                                          outer_height);
            G4LogicalVolume* outer_mother = fOuterDetector_log;
            G4int outer_id = i;

            phys = new G4PVPlacement(rot, outer_pos, log, name, outer_mother,
                                     false, outer_id);

            G4double inner_angle = (i * 90*deg) + 45*deg;
            G4double inner_radius = 6.*cm;
            const G4ThreeVector inner_pos(cos(inner_angle)*inner_radius,
                                          sin(inner_angle)*inner_radius, 
                                          inner_height);
            G4LogicalVolume* inner_mother = fInnerDetector_log;
            G4int inner_id = i + 4;

            phys = new G4PVPlacement(rot, inner_pos, log, name, inner_mother, 
                                     false, inner_id);
        }
    }
}

//----------------------------------------------------------------------------//
// Surfaces
//----------------------------------------------------------------------------//

void CosmicMuDetectorConstruction::ConstructSurfaces()
{
    ConstructInnerDetector_InnerShellSurface();
    ConstructInnerDetector_PMTGlassCoverSurface();
    ConstructOuterDetectorSurface();
    ConstructPMTDetectorSkinSurface();
}

void CosmicMuDetectorConstruction::ConstructInnerDetector_InnerShellSurface()
{
    G4OpticalSurface* InnerDetector_InnerShellSurface 
        = new G4OpticalSurface("InnerDetector_InnerShellSurface");
    InnerDetector_InnerShellSurface->SetType(dielectric_metal);
    InnerDetector_InnerShellSurface->SetFinish(polished);
    InnerDetector_InnerShellSurface->SetModel(glisur);
    //InnerDetector_InnerShellSurface->SetModel(unified);
    InnerDetector_InnerShellSurface->SetMaterialPropertiesTable(fAluminiumMPT);

    G4LogicalBorderSurface* InnerDetector_InnerShellSurface_log
        = new G4LogicalBorderSurface("InnerDetector_InnerShellSurface_log",
                                     fInnerDetector_phys, fInnerShell_phys, 
                                     InnerDetector_InnerShellSurface);
}

void CosmicMuDetectorConstruction::ConstructInnerDetector_PMTGlassCoverSurface()
{
    G4OpticalSurface* InnerDetector_PMTGlassCoverSurface
        = new G4OpticalSurface("InnerDetector_PMTGlassCoverSurface");
    InnerDetector_PMTGlassCoverSurface->SetType(dielectric_dielectric);
    InnerDetector_PMTGlassCoverSurface->SetFinish(ground);
    InnerDetector_PMTGlassCoverSurface->SetModel(glisur);
    InnerDetector_PMTGlassCoverSurface->SetMaterialPropertiesTable(fGlassMPT);


    G4LogicalBorderSurface* InnerDetector_PMTGlassCoverSurface_log
        = new G4LogicalBorderSurface("InnerDetector_PMTGlassCoverSurface_log",
                                     fInnerDetector_phys, fPMTGlassCover_phys, 
                                     InnerDetector_PMTGlassCoverSurface);
}

void CosmicMuDetectorConstruction::ConstructOuterDetectorSurface()
{
    G4OpticalSurface* OuterDetectorSurface
        = new G4OpticalSurface("OuterDetectorSurface");
    OuterDetectorSurface->SetType(dielectric_metal);
    OuterDetectorSurface->SetFinish(polished);
    OuterDetectorSurface->SetModel(glisur);
    OuterDetectorSurface->SetMaterialPropertiesTable(fAluminiumMPT);

    G4LogicalBorderSurface* OuterDetector_InnerShellSurface_log
        = new G4LogicalBorderSurface("OuterDetector_InnerShellSurface_log",
                                     fOuterDetector_phys, fInnerShell_phys,
                                     OuterDetectorSurface);

    G4LogicalBorderSurface* OuterDetector_OuterShellSurface_log
        = new G4LogicalBorderSurface("OuterDetector_OuterShellSurface_log",
                                     fOuterDetector_phys, fOuterShell_phys,
                                     OuterDetectorSurface);
}

void CosmicMuDetectorConstruction::ConstructPMTDetectorSkinSurface()
{
    G4OpticalSurface* PMTDetectorSurface
        = new G4OpticalSurface("PMTDetectorSurface");
    PMTDetectorSurface->SetType(dielectric_dielectric);
    PMTDetectorSurface->SetFinish(polished);
    PMTDetectorSurface->SetModel(glisur);
    PMTDetectorSurface->SetMaterialPropertiesTable(fAluminiumMPT);

    G4LogicalSkinSurface* ODPMTDetectorSurface_log
        = new G4LogicalSkinSurface("PMTDetectorSurface_log", fODPMTDetector_log,
                                   PMTDetectorSurface);
    G4LogicalSkinSurface* IDPMTDetectorSurface_log
        = new G4LogicalSkinSurface("PMTDetectorSurface_log", fIDPMTDetector_log,
                                   PMTDetectorSurface);
}

