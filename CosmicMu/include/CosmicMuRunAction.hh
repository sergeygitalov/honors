//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// \file CosmicMuRunAction.hh
// \brief Definition of CosmicMuRunAction class

#ifndef CosmicMuRunAction_h
#define CosmicMuRunAction_h 1

#include "globals.hh"
#include "G4UserRunAction.hh"
#include "CosmicMuEventAction.hh"

class G4Timer;
class G4Run;
class CosmicMuRunMessenger;

class CosmicMuRunAction : public G4UserRunAction
{
public:
    CosmicMuRunAction(CosmicMuEventAction* eventAction);
    ~CosmicMuRunAction();
    
    void BeginOfRunAction(const G4Run* aRun);
    void EndOfRunAction(const G4Run* aRun); 

    void SetFileName(G4String name); // sets name of ROOT output file 
    
private:
    G4Timer* timer;
    // There is a default file name in case none was specified. This prevents data
    // from not saving, wasting the run.
    //
    G4String fFileName = "CosmicMu.root";
    CosmicMuRunMessenger* runMessenger;
};

#endif 
