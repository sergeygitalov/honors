//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// \file CosmicMuDetectorConstruction.hh
// \brief Definition of CosmicMuDetectorConstruction class

#ifndef CosmicMuDetectorConstruction_h
#define CosmicMuDetectorConstruction_h 1

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4NistManager.hh"
#include "G4Element.hh"
#include "G4Colour.hh"

class CosmicMuDetectorConstruction : public G4VUserDetectorConstruction
{
public:
    CosmicMuDetectorConstruction();
    ~CosmicMuDetectorConstruction();

    G4VPhysicalVolume* Construct();

private:
    void SetConstants(); // initialize contants used in multiple methods
    void ConstructElements(); // initializes period table elements
    void ConstructMaterials(); // initializes detector materials 

    void ConstructMaterialPropertiesTables(); // set optic properties
    void ConstructAluminiumMPT();
    void ConstructGlassMPT();
    void ConstructIsopropylMPT();
    void ConstructMineralOilMPT();
    void ConstructNE_216MPT();
    void ConstructPlexiglassMPT();
    void ConstructPolystyreneMPT();
    void ConstructWaterMPT();

    void ConstructVolumes(); // constructs physical and logical volumes
    void ConstructWorld();
    void ConstructGasTank();
    void ConstructCarbonShielding();
    void ConstructOuterShell();
    void ConstructOuterDetector();
    void ConstructInnerShell();
    void ConstructInnerDetector();
    void ConstructPMTs();
    void ConstructPMTShell();
    void ConstructPMTVacuum();
    void ConstructPMTGlassCover();
    void ConstructPMTDetector();
    // Since there are multiple PMTs, this method makes it easier to construct
    // its components multiple times
    //
    void PlacePMTComponent(G4VPhysicalVolume*, G4LogicalVolume*, G4String name,
                           G4double outer_height, G4double inner_height,
                           G4String volume);

    void ConstructSurfaces(); // initializes optical properties of surfaces
    void ConstructInnerDetector_InnerShellSurface();
    void ConstructInnerDetector_PMTGlassCoverSurface();
    void ConstructOuterDetectorSurface();
    void ConstructPMTDetectorSkinSurface();

    G4NistManager* fNist; // nist manager helps construct elements and materials

    // All the variables below are used throughout multiple methods, so they are
    // stored globally. There are still some local variables present in methods.
    //
    G4double *fPhotonEnergy, *fAbsorption, *fScintilFast, *fScintilSlow;
    G4double fAbsConstant;

    G4Element *fH, *fB, *fC, *fN, *fO, *fSi, *fP, *fS, *fCr, *fMn, *fFe, *fNi;
    G4Material *fAir, *fAluminium, *fBoratedPolyethylene, *fCarbon, *fFreon,
               *fGlass, *fIsopropyl, *fMineralOil, *fNE_216, *fPlexiglass,
               *fPolyethylene, *fPolystyrene, *fPOPOP, *fPPO, *fSilicon, 
               *fSteel, *fToluene, *fVacuum, *fWater;
    G4MaterialPropertiesTable *fAluminiumMPT, *fGlassMPT, *fIsopropylMPT,
                              *fMineralOilMPT, *fNE_216MPT, *fPlexiglassMPT,
                              *fPolystyreneMPT, *fWaterMPT;

    // Colors are used for visualization
    //
    G4Colour fBlue, fCyan, fGray, fGreen, fRed, fWhite, fYellow;

    G4LogicalVolume *fWorld_log, *fGasTank_log, *fOuterShell_log,
                    *fOuterDetector_log, *fInnerShell_log, *fInnerDetector_log,
                    *fODPMTDetector_log, *fIDPMTDetector_log;

    G4VPhysicalVolume *fWorld_phys, *fOuterShell_phys, *fOuterDetector_phys,
                      *fInnerShell_phys, *fInnerDetector_phys,
                      *fPMTGlassCover_phys, *fPMTDetector_phys;
};

#endif
