//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// \file CosmicMuEventAction.hh
// \brief Definition of CosmicMuEventAction class

#ifndef CosmicMuEventAction_h
#define CosmicMuEventAction_h 1

#include <vector>

#include "G4UserEventAction.hh"
#include "G4ThreeVector.hh"
#include "globals.hh"

class G4Event;
class CosmicMuRunAction;

class CosmicMuEventAction : public G4UserEventAction
{
public:
    CosmicMuEventAction();
    ~CosmicMuEventAction();

    void BeginOfEventAction(const G4Event*);
    void EndOfEventAction(const G4Event*);

    // Get methods for tracked variables
    //
    G4double* GetfMu3Pos() {return fMu3Pos;}
    G4double* GetfMu4Mom() {return fMu4Mom;}
    G4int* GetfMuCharge() {return &fMuCharge;}
    G4int* GetfMuDecay() {return &fMuDecay;}
    G4double* GetfMuID4Pos() {return fMuID4Pos;}
    G4double* GetfMuID4Mom() {return fMuID4Mom;}
    G4int* GetfMuIDEntry() {return &fMuIDEntry;}
    G4double* GetfMuIDDistance() {return &fMuIDDistance;}
    G4double* GetfMuOD4Pos() {return fMuOD4Pos;}
    G4double* GetfMuOD4Mom() {return fMuOD4Mom;}
    G4int* GetfMuODEntry() {return &fMuODEntry;}
    G4double* GetfMuODDistance() {return &fMuODDistance;}
    G4double* GetfE4Pos() {return fE4Pos;}
    G4double* GetfE4Mom() {return fE4Mom;}
    G4double* GetfEID4Pos() {return fEID4Pos;}
    G4double* GetfEID4Mom() {return fEID4Mom;}
    G4int* GetfEIDEntry() {return &fEIDEntry;}
    G4double* GetfEIDDistance() {return &fEIDDistance;}
    G4double* GetfEOD4Pos() {return fEOD4Pos;}
    G4double* GetfEOD4Mom() {return fEOD4Mom;}
    G4int* GetfEODEntry() {return &fEODEntry;}
    G4double* GetfEODDistance() {return &fEODDistance;}
    G4int* GetfPhIDMu() {return &fPhIDMu;}
    std::vector<G4double>& GetfPhIDMuTime() {return fPhIDMuTime;}
    std::vector<G4double>& GetfPhIDMuWavelength() {return fPhIDMuWavelength;}
    std::vector<G4int>& GetfPhIDMuProcess() {return fPhIDMuProcess;}
    G4int* GetfPhIDE() {return &fPhIDE;}
    std::vector<G4double>& GetfPhIDETime() {return fPhIDETime;}
    std::vector<G4double>& GetfPhIDEWavelength() {return fPhIDEWavelength;}
    std::vector<G4int>& GetfPhIDEProcess() {return fPhIDEProcess;}
    G4int* GetfPhODMu() {return &fPhODMu;}
    std::vector<G4double>& GetfPhODMuTime() {return fPhODMuTime;}
    std::vector<G4double>& GetfPhODMuWavelength() {return fPhODMuWavelength;}
    std::vector<G4int>& GetfPhODMuProcess() {return fPhODMuProcess;}
    G4int* GetfPhODE() {return &fPhODE;}
    std::vector<G4double>& GetfPhODETime() {return fPhODETime;}
    std::vector<G4double>& GetfPhODEWavelength() {return fPhODEWavelength;}
    std::vector<G4int>& GetfPhODEProcess() {return fPhODEProcess;}

private:
    // Method to reset variables after each event
    //
    void ResetData();

    // Tracked variables. All of these are saved in a root file at the end of
    // the run.
    //
    // Muons
    //
    G4double fMu3Pos[3]; // initial 3-position
    G4double fMu4Mom[4]; // initial 4-momentum
    G4int fMuCharge;
    G4int fMuDecay; // decay state and logical volume

    G4double fMuID4Pos[4]; // 4-position on entry in ID
    G4double fMuID4Mom[4]; // 4-momentum on entry in ID
    G4int fMuIDEntry; // # of times mu entered ID
    G4double fMuIDDistance; // total distance traveled in ID

    G4double fMuOD4Pos[4]; // 4-position on entry in OD
    G4double fMuOD4Mom[4]; // 4-momentum on entry in OD
    G4int fMuODEntry; // # of times mu entered OD
    G4double fMuODDistance; // total distance traveled in OD

    // Electrons
    //
    G4double fE4Pos[4]; // initial 4-position
    G4double fE4Mom[4]; // initial 4-momentum

    G4double fEID4Pos[4]; // 4-position on entry in ID
    G4double fEID4Mom[4]; // 4-momentum on entry in ID
    G4int fEIDEntry; // # of times e entered ID
    G4double fEIDDistance; // total distance traveled in OD

    G4double fEOD4Pos[4]; // 4-position on entry in OD
    G4double fEOD4Mom[4]; // 4-position on entry in OD
    G4int fEODEntry; // # of times e entered OD
    G4double fEODDistance; // total distance traveled in OD

    // Photons
    //
    G4int fPhIDMu; // # of photons detected in ID PMT produced by muons
    std::vector<G4double> fPhIDMuTime; // time at PMT
    std::vector<G4double> fPhIDMuWavelength;
    std::vector<G4int> fPhIDMuProcess; // Cherenkov, scintillation, etc.

    G4int fPhIDE; // # of photons detected in ID PMT produced by electrons
    std::vector<G4double> fPhIDETime; // time at PMT
    std::vector<G4double> fPhIDEWavelength;
    std::vector<G4int> fPhIDEProcess; // Cherenkov, scintillation, etc.

    G4int fPhODMu; // # of photons detected in OD PMT produced by muons
    std::vector<G4double> fPhODMuTime; // time at PMT
    std::vector<G4double> fPhODMuWavelength;
    std::vector<G4int> fPhODMuProcess; // Cherenkov, scintillation, etc.

    G4int fPhODE; // # of photons detected in OD PMT produced by electrons
    std::vector<G4double> fPhODETime; // time at PMT
    std::vector<G4double> fPhODEWavelength;
    std::vector<G4int> fPhODEProcess; // Cherenkov, scintillation, etc.
};

#endif
