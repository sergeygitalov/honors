#!/bin/bash
rm CosMu.mac
touch CosMu.mac
echo "/run/initialize" >> CosMu.mac
let length=$1-1
for i in $( eval echo {0..$length} )
do
	echo "/WaterCherenkov/fileName/fileName CosMu$i.root
/run/beamOn 1000" >> CosMu.mac
done

THREADS=$(nproc)
./WaterCherenkov -m CosMu.mac -t $THREADS
