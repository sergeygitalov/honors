GEANT4 code for the simulation of the DUNE experiment detector prototype of the
stopped muon monitor. The simulation uses version 10.05 of GEANT4. Detector
geometry, cosmic ray muon generation, detailed particle tracking, and ROOT file
output are all implemented.

Contents:
    CMakeLists.txt: makefile to compile the code. Uses standard geant
        compilation procedure.
    CosmicMu.cc: main()
    include and src:
        CosmicMuActionInitialization: contains ActionInitialization for 
            various other geant classes. Unless you want to add brand new 
            classes, there is no need to modify this file.
        CosmicMuDetectorConstruction: contains detector geometry with
            material optical properties.
        CosmicMuEventAction: fills ntuples with data from event
        CosmicMuPhysicsList: contains pre-made geant physics classes
        CosmicMuPrimaryGeneratorAction: generates cosmic ray muons. Has a
            default cosine-sqaured distribution and a test momentum-dependent
            distribution.
        CosmicMuRunAction: contains ntuple and root file initializations
        CosmicMuRunMessenger: contains geant command implementations for setting
            root file name.
        CosmicMuSteppingAction: tracking for most of the data is done here. 
    testrun.mac: quick test macro.
    vis.mac: visualization macro. If no macro is specified in the command line,
        this macro is used by default.

