//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// \file CosmicMu.cc
// \brief file containing main() for cosmic muon simulation

#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#else
#include "G4RunManager.hh"
#endif

#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"
#include "G4ios.hh"
#include "time.h"
#include "Randomize.hh"

#include "CosmicMuDetectorConstruction.hh"
#include "CosmicMuPhysicsList.hh"
#include "CosmicMuActionInitialization.hh"

#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif
#include "G4UIExecutive.hh"

namespace
{
// Print error message if invalid argument
//
void PrintUsage()
{
    G4cerr << " Usage: " << G4endl;
    G4cerr << " CosmicMu"
    << "[-m macro ] [-u UIsession] [-t nThreads] [-s seed]" << G4endl;
    G4cerr << "   note: -t option is available only for multi-threaded mode."
        << G4endl;
}
}

int main(int argc,char** argv)
{
    G4String macro;
    G4String session;
    G4int seed = time(NULL);
#ifdef G4MULTITHREADED
    G4int nThreads = 0;
#endif
    for (G4int i = 1; i < argc; i = i+2)
    {
        if (G4String(argv[i]) == "-m" )
            macro = argv[i+1];
        else if (G4String(argv[i]) == "-u")
            session = argv[i+1];
        else if (G4String(argv[i]) == "-s")
            seed = atoi(argv[i+1]);
#ifdef G4MULTITHREADED
        else if (G4String(argv[i]) == "-t")
            nThreads = G4UIcommand::ConvertToInt(argv[i+1]);
#endif
        else
        {
            PrintUsage();
            return 1;
        }
    }

    // Detect interactive mode (if no macro provided) and define UI session
    //
    G4UIExecutive* ui = nullptr;
    if (!macro.size())
        ui = new G4UIExecutive(argc, argv, session);

    // Choose the Random engine
    //
    G4Random::setTheEngine(new CLHEP::RanecuEngine);

    // Set random seed
    //
    G4Random::setTheSeed(seed);

    // Construct the default run manager
    //
#ifdef G4MULTITHREADED
    G4MTRunManager* runManager = new G4MTRunManager;
    if (nThreads > 0)
        runManager->SetNumberOfThreads(nThreads);
#else
    G4RunManager* runManager = new G4RunManager;
#endif

    // UserInitialization classes - mandatory
    //
    runManager-> SetUserInitialization(new CosmicMuDetectorConstruction());
    //
    runManager-> SetUserInitialization(new CosmicMuPhysicsList());
    //
    runManager-> SetUserInitialization(new CosmicMuActionInitialization());

    // Initialize visualization
    //
#ifdef G4VIS_USE
    G4VisExecutive* visManager = new G4VisExecutive;
    visManager->Initialize();
#endif

    // Get the pointer to the User Interface manager
    G4UImanager* UImanager = G4UImanager::GetUIpointer();

    // Process macro or start UI session
    //
    if (macro.size())
    {
        // batch mode
        G4String command = "/control/execute ";
        UImanager->ApplyCommand(command+macro);
    }
    else
    {
        // interactive mode : define UI session
#ifdef G4VIS_USE
        UImanager->ApplyCommand("/control/execute vis.mac");
#endif
        ui->SessionStart();
        delete ui;
    }

    // Job termination
    // Free the store: user actions, physics_list and detector_description are
    // owned and deleted by the run manager, so they should not be deleted
    // in the main() program !

#ifdef G4VIS_USE
    delete visManager;
#endif
    delete runManager;

    return 0;
}
