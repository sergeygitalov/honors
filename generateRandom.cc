#include <time.h>
#include <fstream>
#include <cstdlib>

//#define RAND_MAX = 9999999999;

using namespace std;

int main(int argc, char* argv[])
{
	srand(time(NULL));

	ofstream ofs("random.list");

	for (int i = 0; i < atoi(argv[1]); i++)
		ofs << rand() % 8999999999 + 1000000000 << "\n";

	return 0;
}
