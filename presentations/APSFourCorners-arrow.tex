%----------------------------------------------------------------------------------------
%	PACKAGES AND THEMES
%----------------------------------------------------------------------------------------

\documentclass{beamer}

\mode<presentation> {

% The Beamer class comes with a number of default slide themes
% which change the colors and layouts of slides. Below this is a list
% of all the themes, uncomment each in turn to see what they look like.

\usetheme{default}
%\usetheme{AnnArbor}
%\usetheme{Antibes}
%\usetheme{Bergen}
%\usetheme{Berkeley}
%\usetheme{Berlin}
%\usetheme{Boadilla}
%\usetheme{CambridgeUS}
%\usetheme{Copenhagen}
%\usetheme{Darmstadt}
%\usetheme{Dresden}
%\usetheme{Frankfurt}
%\usetheme{Goettingen}
%\usetheme{Hannover}
%\usetheme{Ilmenau}
%\usetheme{JuanLesPins}
%\usetheme{Luebeck}
%\usetheme{Madrid}
%\usetheme{Malmoe}
%\usetheme{Marburg}
%\usetheme{Montpellier}
%\usetheme{PaloAlto}
%\usetheme{Pittsburgh}
%\usetheme{Rochester}
%\usetheme{Singapore}
%\usetheme{Szeged}
%\usetheme{Warsaw}

\setbeamertemplate{footline}{%
	\raisebox{5pt}{\makebox[\paperwidth]{\hfill\makebox[15pt]{\scriptsize\insertframenumber}}}}

% As well as themes, the Beamer class has a number of color themes
% for any slide theme. Uncomment each of these in turn to see how it
% changes the colors of your current slide theme.

%\usecolortheme{albatross}
%\usecolortheme{beaver}
%\usecolortheme{beetle}
%\usecolortheme{crane}
%\usecolortheme{dolphin}
%\usecolortheme{dove}
%\usecolortheme{fly}
%\usecolortheme{lily}
%\usecolortheme{orchid}
%\usecolortheme{rose}
%\usecolortheme{seagull}
%\usecolortheme{seahorse}
%\usecolortheme{whale}
%\usecolortheme{wolverine}

%\setbeamertemplate{footline} % To remove the footer line in all slides uncomment this line
%\setbeamertemplate{footline}[page number] % To replace the footer line in all slides with a simple slide count uncomment this line

\setbeamertemplate{navigation symbols}{} % To remove the navigation symbols from the bottom of all slides uncomment this line
}

\usepackage{graphicx} % Allows including images
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables

\usepackage[labelformat=empty]{caption}
\usepackage[export]{adjustbox}

\usepackage{tikz}
\usetikzlibrary{positioning}
\usetikzlibrary{decorations.pathmorphing}
\usepackage{pgfplots}
\usepackage{amsmath}
\usepackage{graphicx}

\title[Stopped Muon Monitor Monte Carlo]{Monte Carlo Simulation of the Stopped Muon Monitor at DUNE} % The short title appears at the bottom of every slide, the full title is only on the title page

\author{Sergey Gitalov} % Your name
\institute[CU Boulder] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
CU Boulder \\ % Your institution for the title page
\medskip
\textit{sergey.gitalov@colorado.edu} % Your email address
}
\date{October 11, 2019} % Date, can be changed to a custom date

\begin{document}

\begin{frame}
\titlepage % Print the title page as the first slide
\end{frame}

\begin{frame}
\frametitle{Overview} % Table of contents slide, comment this block out to remove it
\tableofcontents % Throughout your presentation, if you choose to use \section{} and \subsection{} commands, these will automatically be printed on this slide as an overview of your presentation
\end{frame}

%----------------------------------------------------------------------------------------
%	PRESENTATION SLIDES
%----------------------------------------------------------------------------------------

%------------------------------------------------

\section{DUNE}

\begin{frame}
\frametitle{Deep Underground Neutrino Experiment (DUNE)}
\begin{itemize}
\item DUNE is a 1300 $km$ long baseline neutrino experiment with the neutrino beam going from Fermilab in Illinois to Sanford in South Dakota.
\item This will be the longest baseline for a neutrino experiment, allowing to measure oscillations with better precision.
\item Planned to be completed in the late 2020s.
\end{itemize}
\begin{figure}
\includegraphics[width = 1\textwidth]{dunediagram}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Muon Alcove at DUNE}
\begin{itemize}
\item Neutrinos have a very small cross-section, so it is important to track other particles involved in the production of the neutrino beam.
\item The main decay mode is charged pion decay that produces muons as a byproduct.
\begin{equation*}
\pi^{\pm} \rightarrow \mu^{\pm} + \overset{(-)}{\nu_{\mu}}
\end{equation*}
\item Muons in DUNE will be tracked in the muon alcove.
\item The muon alcove consists of multiple layers of steel shielding designed to slow down muons with muon monitors installed between shielding.
\end{itemize}
\end{frame}

\section{Monitor Purpose and Functionality}

\begin{frame}
\begin{itemize}
\frametitle{Monitor Purpose and Functionality}
\item Stopped Muon Monitor (SMM) is to be installed in the muon alcove in between steel shielding to detect muons at various energies.
\item Muons that are $< 150 \ MeV$ in kinetic energy may stop inside the SMM and decay.
\item This allows to anaylze the beam by portions, in contrast to other muon monitors that detect the entire beam at once.
\item An array of SMMs can be deployed in the muon alcove to monitor portions of the muon beam at different positions and ranges of energy (via layers of shielding).
\item Decay electrons emit Cherenkov light that is detected by photomultiplier tubes (PMTs).
\item In order to trigger the Cherenkov light collection, scintillator is used to detect incoming muons.
\item Signal strength should correlate with the number of stopped muons.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Monte Carlo Goals}
\begin{itemize}
\item There is a prototype at CU Boulder that needs to be tested first.
\item The Monte Carlo will be compared to data from the prototype to make any corrections to the simulation.
\item The corrected Monte Carlo will be used to simulate the SMM in the NuMI beam.
\item SMMs will then be deployed in the NuMI beam to test performance.
\item How the SMM performs at the NuMI beam will determine its future in the DUNE experiment.
\end{itemize}
\end{frame}

\section{Monitor Construction} % Sections can be created in order to organize your presentation into discrete blocks, all sections and subsections are automatically printed in the table of contents as an overview of the talk
%------------------------------------------------

\begin{frame}
\frametitle{Monitor Construction}
\begin{itemize}
\item Inner Detector (ID) filled with mineral oil.
\item ID designed to detect Cherenkov light from $\mu$ and $e$.
\item Outer Detector (OD) filled with scintillating material.
\item 4 PMTs per detector.
\item $10 \ cm$ inner detector radius
\item $13 \ cm$ overall radius
\item $25 \ cm$ height without PMTs
\item It is important to know when an event is contained, i.e. the muon decayed in the ID, and the decay electron stayed in the ID; in order to ensure that the muon indeed stopped and to collect all Cherenkov light from the electron in the ID.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Example Event}
%\begin{figure}
%\includegraphics[width = 0.5\textwidth]{ExEvent}
%\caption*{DUNE WIP}
%\end{figure}
\centering
	\begin{tikzpicture}
		\node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=0.8\textwidth]{ExEvent}};
		\draw [<-, line width=0.4mm,red] (4,4) -- (3,3) node[pos=1.3,align=center,font=\footnotesize] {inner \\ detector};
		\draw [<-, line width=0.4mm,red] (5,3.4) -- (5,2.5) node[pos=1.3,align=center,font=\footnotesize] {outer \\ detector};
		\draw [<-, line width=0.4mm,red] (5.2,6.5) -- (6,7) node[pos=1.3,align=center,font=\footnotesize] {PMT};
		\node<1-> [text width=2.5cm] at (2.8,5.2) {\tiny \color{white} DUNE Work\\ \vspace{-1em} in Progress};
	\end{tikzpicture}
\end{frame}

\section{Cosmic Ray Model}

\begin{frame}
\frametitle{Cosmic Rays}
\begin{itemize}
\item Uniform energy distribution in the 0-150 MeV range
\item Flux depends on zenith (polar) angle. $I \propto \cos^n{\theta}$, where n depends on momentum
\item $2 : 1$ ratio of $\mu^+ : \mu^-$
\end{itemize}
\begin{figure}
\caption*{Momentum Dependence of Zenith Angle Distribution}
\includegraphics[width = 0.4\textwidth]{AngleDep}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Simulation}
\begin{itemize}
\item Muons uniformly generated on a disk 40 cm above the detector center and 60 cm in radius.
\item Radius is determined empirically as $3\sigma$ away for muons that hit the detector.
\item Kinetic energy uniformly distributed between $0$ and $150 \ MeV$.
\item Momentum uniform in $\phi$, polar angle distributed such that
\newline differential flux $\propto\cos^2{\theta}$.
\end{itemize}
\end{frame}

\begin{frame}
\begin{figure}
	\caption*{Initial xy-Position of Muons That Hit The Detector}
%\includegraphics[width = 0.6\textwidth]{MuPosXY_cEntry}
	\begin{tikzpicture}[every node/.style={inner sep=0,outer sep=0}]
		\node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=0.8\textwidth]{MuPosXY_cEntry}};
		%\draw [->, line width=0.4mm,red] (1.5,2.5) -- (1.5,1.5) node[pos=1.3,align=center,font=\footnotesize] {larger \\ signal};
		\node<1-> [text width=2.5cm] at (2.6,6.9) {\scriptsize \color{red} DUNE Work\\ \vspace{-0.5em} in Progress};
	\end{tikzpicture}
\end{figure}
\end{frame}

\begin{frame}{Initial xy-Position of Muons That Hit The Detector}
\centering
	\begin{tikzpicture}
		\node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=0.9\textwidth,trim=0 0 0 50,clip]{MuPosXY_cEntry}};
		%\draw [->, line width=0.4mm,green] (2,2) -- (4,7.5) node[pos=1.1,align=center,font=\footnotesize] {Text here maybe?};
		\node<1-> [text width=2.5cm] at (2.6,7.3) {\scriptsize \color{red} DUNE Work\\ \vspace{-0.5em} in Progress};
	\end{tikzpicture}
\end{frame}

\begin{frame}
\frametitle{Separating $\mu$ and $e$}
\begin{itemize}
\item Signal in both the ID and the OD can be separated by time of decay.
\item Geant4 can track the time of decay for each individual event as well as the detector where a decay occured.
\item Muon signal is registered in the first $25 \ ns$ follwed by signal from $e$.
\end{itemize}
\end{frame}

\begin{frame}
\begin{figure}
\caption*{Time of Detection of Muon Photons in OD}
%\includegraphics[width = 1.0\textwidth]{PhMuODTime}
%\caption*{DUNE WIP}
\begin{tikzpicture}[every node/.style={inner sep=0,outer sep=0}]
		\node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=1.0\textwidth]{PhMuODTime}};
		%\draw [->, line width=0.4mm,red] (1.5,2.5) -- (1.5,1.5) node[pos=1.3,align=center,font=\footnotesize] {larger \\ signal};
		\node<1-> [text width=2.5cm] at (4.0,2.0) {\scriptsize \color{red} DUNE Work\\ \vspace{-0.5em} in Progress};
	\end{tikzpicture}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}
\caption*{Time of Detection of Electron Photons in OD}
%\includegraphics[width = 1.0\textwidth]{PhEODTime}
\begin{tikzpicture}[every node/.style={inner sep=0,outer sep=0}]
		\node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=1.0\textwidth]{PhEODTime}};
		%\draw [->, line width=0.4mm,red] (1.5,2.5) -- (1.5,1.5) node[pos=1.3,align=center,font=\footnotesize] {larger \\ signal};
		\node<1-> [text width=2.5cm] at (4.0,3.0) {\scriptsize \color{red} DUNE Work\\ \vspace{-0.5em} in Progress};
	\end{tikzpicture}
\end{figure}
\end{frame}

\section{Results}

\begin{frame}
\begin{figure}
\caption*{Photons in OD vs Muon Energy on Entry}
%\includegraphics[width = 0.75\textwidth]{MuEnergyvsPhOD}
\begin{tikzpicture}[every node/.style={inner sep=0,outer sep=0}]
		\node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=1.0\textwidth]{MuEnergyvsPhOD}};
		%\draw [->, line width=0.4mm,red] (1.5,2.5) -- (1.5,1.5) node[pos=1.3,align=center,font=\footnotesize] {larger \\ signal};
		\node<1-> [text width=2.5cm] at (5.0,3.7) {\scriptsize \color{red} DUNE Work\\ \vspace{-0.5em} in Progress};
	\end{tikzpicture}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}
\caption*{Photons in OD vs Muon Energy on Entry for contained events}
%\includegraphics[width = 0.75\textwidth]{MuEnergyvsPhOD_cOId}
\begin{tikzpicture}[every node/.style={inner sep=0,outer sep=0}]
		\node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=1.0\textwidth]{MuEnergyvsPhOD_cOId}};
		%\draw [->, line width=0.4mm,red] (1.5,2.5) -- (1.5,1.5) node[pos=1.3,align=center,font=\footnotesize] {larger \\ signal};
		\node<1-> [text width=2.5cm] at (7.0,4.0) {\scriptsize \color{red} DUNE Work\\ \vspace{-0.5em} in Progress};
	\end{tikzpicture}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}
\caption*{Photons in OD vs Muon Energy on Entry for uncontained events}
%\includegraphics[width = 0.75\textwidth]{MuEnergyvsPh_cUncontained}
\begin{tikzpicture}[every node/.style={inner sep=0,outer sep=0}]
		\node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=1.0\textwidth]{MuEnergyvsPh_cUncontained}};
		%\draw [->, line width=0.4mm,red] (1.5,2.5) -- (1.5,1.5) node[pos=1.3,align=center,font=\footnotesize] {larger \\ signal};
		\node<1-> [text width=2.5cm] at (7.0,4.0) {\scriptsize \color{red} DUNE Work\\ \vspace{-0.5em} in Progress};
	\end{tikzpicture}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Michel Spectrum}
\begin{figure}
\caption{Decay Electron Initial Energy}
%\includegraphics[width = 0.75\textwidth]{EEnergy}
\begin{tikzpicture}[every node/.style={inner sep=0,outer sep=0}]
		\node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=1.0\textwidth]{EEnergy}};
		%\draw [->, line width=0.4mm,red] (1.5,2.5) -- (1.5,1.5) node[pos=1.3,align=center,font=\footnotesize] {larger \\ signal};
		\node<1-> [text width=2.5cm] at (7.0,3.0) {\scriptsize \color{red} DUNE Work\\ \vspace{-0.5em} in Progress};
	\end{tikzpicture}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}
\caption*{Photons in ID vs Electron Initial Energy}
%\includegraphics[width = 0.75\textwidth]{EEnergyvsPhID}
\begin{tikzpicture}[every node/.style={inner sep=0,outer sep=0}]
		\node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=1.0\textwidth]{EEnergyvsPhID}};
		%\draw [->, line width=0.4mm,red] (1.5,2.5) -- (1.5,1.5) node[pos=1.3,align=center,font=\footnotesize] {larger \\ signal};
		\node<1-> [text width=2.5cm] at (7.0,3.0) {\scriptsize \color{red} DUNE Work\\ \vspace{-0.5em} in Progress};
	\end{tikzpicture}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}
\caption*{Photons in ID vs Electron Initial Energy for contained events}
%\includegraphics[width = 0.75\textwidth]{EEnergyvsPhID_cdI}
\begin{tikzpicture}[every node/.style={inner sep=0,outer sep=0}]
		\node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=1.0\textwidth]{EEnergyvsPhID_cdI}};
		%\draw [->, line width=0.4mm,red] (1.5,2.5) -- (1.5,1.5) node[pos=1.3,align=center,font=\footnotesize] {larger \\ signal};
		\node<1-> [text width=2.5cm] at (7.0,3.0) {\scriptsize \color{red} DUNE Work\\ \vspace{-0.5em} in Progress};
	\end{tikzpicture}
\end{figure}
\end{frame}

\begin{frame}
\begin{enumerate}
\frametitle{Conclusion}
\item There is a distinct energy range for muons that are contained.
\item Both the contained muon and its decay electron produce signal distributions that are distinct from uncontained events.
\item There is a signal threshold in the OD for contained events.
\item Contained events allow us to relate signal in the ID with the Michel spectrum, while uncontained events don't.
\end{enumerate}
\end{frame}

\section{Current Work}

\begin{frame}
\frametitle{Current Work}
\begin{itemize}
\item Working on prototype at CU
\item Near future: comparing Monte Carlo results with empirical data from the prototype.
\end{itemize}
\end{frame}

\begin{frame}
\begin{center}
Thank you!
\end{center}
\begin{figure}
\includegraphics[width = 0.5\textwidth]{doe}
\end{figure}
\end{frame}


\begin{frame}
\begin{figure}
\caption*{Number of Events vs Cosine of Zenith Angle}
%\includegraphics[width = 0.75\textwidth]{CosZenith}
\begin{tikzpicture}[every node/.style={inner sep=0,outer sep=0}]
		\node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=1.0\textwidth]{CosZenith}};
		%\draw [->, line width=0.4mm,red] (1.5,2.5) -- (1.5,1.5) node[pos=1.3,align=center,font=\footnotesize] {larger \\ signal};
		\node<1-> [text width=2.5cm] at (7.0,3.0) {\scriptsize \color{red} DUNE Work\\ \vspace{-0.5em} in Progress};
	\end{tikzpicture}
\end{figure}
\end{frame}

%----------------------------------------------------------------------------------------

\end{document} 
