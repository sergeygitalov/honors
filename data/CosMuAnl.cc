#include "CosMuAnl.hh"

CosMuAnl::CosMuAnl(int numFiles)
{
	ChainFiles(numFiles);
	SetAddresses();
	CreateHistos();
}

CosMuAnl::~CosMuAnl()
{
	delete data;
	delete hPrimePosX;
	delete hPrimePosY;
	delete hPrimePosZ;
	delete hPrimePosXY;
	delete hPrimeEnergy;
	delete hPrimeMomX;
	delete hPrimeMomY;
	delete hPrimeMomZ;
	delete hPrimeMomXY;
	delete hPrimeMomXZ;
	delete hPrimeMomYZ;
	delete hPrimeMomentum;
	delete hPrimeKinEnergy;
	delete hPrimeBeta;
	delete hCosZenithAngle;
	delete hMuonTime;
	delete hMuonPosX;
	delete hMuonPosY;
	delete hMuonPosZ;
	delete hMuonPosXY;
	delete hMuonPosXZ;
	delete hMuonPosYZ;
	delete hMuonEnergy;
	delete hMuonMomX;
	delete hMuonMomY;
	delete hMuonMomZ;
	delete hMuonMomXY;
	delete hMuonMomXZ;
	delete hMuonMomYZ;
	delete hMuonMomentum;
	delete hMuonKinEnergy;
	delete hMuonBeta;
	delete hElectronTime;
	delete hElectronPosX;
	delete hElectronPosY;
	delete hElectronPosZ;
	delete hElectronPosXY;
	delete hElectronPosXZ;
	delete hElectronPosYZ;
	delete hElectronEnergy;
	delete hElectronMomX;
	delete hElectronMomY;
	delete hElectronMomZ;
	delete hElectronMomXY;
	delete hElectronMomXZ;
	delete hElectronMomYZ;
	delete hElectronMomentum;
	delete hElectronKinEnergy;
	delete hElectronBeta;
	delete hInnerDetectorPhotonHits;
	delete hInnerDetectorPhotonTime;
	delete hInnerDetectorPhotonWavelength;
	delete hOuterDetectorPhotonHits;
	delete hOuterDetectorPhotonTime;
	delete hOuterDetectorPhotonWavelength;
}

void CosMuAnl::ChainFiles(int numFiles)
{
	data = new TChain("data");
	for (int i = 0; i < numFiles; i++)
	{
		stringstream ss;
		ss << i;
		string str = ss.str();
		string filename = "CosMu" + str + ".root";
		data->Add(filename.c_str());
	}
}

void CosMuAnl::AddCut(string cut)
{
	if (cuts.find(cut) == cuts.end())
		cuts[cut] = false;
	else
		cout << "Error: Cut already present" << endl;
}

void CosMuAnl::SetCut(string cut)
{
	if (cuts.find(cut) == cuts.end())
		cout << "Error: Cut not found" << endl;
	else
		cuts[cut] = true;
}

void CosMuAnl::SetCuts()
{
	if (cuts.bucket_count == 0)
	{
		cout << "Error: No cuts set" << endl;
		return;
	}
	
	for (auto x : cuts)
		x.second = true;
}

void CosMuAnl::ResetCut(string cut)
{
	if (cuts.find(cut) == cuts.end())
		cout << "Error: Cut not found" << endl;
	else
		cuts[cut] = false;
}

void CosMuAnl::ResetCuts()
{
	if (cuts.bucket_count == 0)
	{
		cout << "Error: No cuts set" << endl;
		return;
	}

	for (auto x : cuts)
		x.second = false;
}

void CosMuAnl::Fill()
{
	Int_t nentries = (Int_t)data->GetEntries();

	for (int i = 0; i < nentries; i++)
{
	data->GetEntry(i);

	// Filling 1D Histos

	hPrimePosX->Fill(fPrimePosX);
	hPrimePosY->Fill(fPrimePosY);
	hPrimePosZ->Fill(fPrimePosZ);
	hPrimePosXY->Fill(fPrimePosX,fPrimePosY);
	hPrimeEnergy->Fill(fPrimeEnergy);
	hPrimeMomX->Fill(fPrimeMomX);
	hPrimeMomY->Fill(fPrimeMomY);
	hPrimeMomZ->Fill(fPrimeMomZ);
	hPrimeMomXY->Fill(fPrimeMomX,fPrimeMomY);
	hPrimeMomXZ->Fill(fPrimeMomX,fPrimeMomZ);
	hPrimeMomYZ->Fill(fPrimeMomY,fPrimeMomZ);
	hPrimeMomentum->Fill(fPrimeMomentum);
	hPrimeKinEnergy->Fill(fPrimeKinEnergy);
	hPrimeBeta->Fill(fPrimeBeta);
	hCosZenithAngle->Fill(fCosZenithAngle);

	if (fMuonEntryVolume > 0)
	{
		hMuonTime->Fill(fMuonTime);
		hMuonPosX->Fill(fMuonPosX);
		hMuonPosY->Fill(fMuonPosY);
		hMuonPosZ->Fill(fMuonPosZ);
		hMuonPosXY->Fill(fMuonPosX,fMuonPosY);
		hMuonPosXZ->Fill(fMuonPosX,fMuonPosZ);
		hMuonPosYZ->Fill(fMuonPosY,fMuonPosZ);
		hMuonEnergy->Fill(fMuonEnergy);
		hMuonMomX->Fill(fMuonMomX);
		hMuonMomY->Fill(fMuonMomY);
		hMuonMomZ->Fill(fMuonMomZ);
		hMuonMomXY->Fill(fMuonMomX,fMuonMomY);
		hMuonMomXZ->Fill(fMuonMomX,fMuonMomZ);
		hMuonMomYZ->Fill(fMuonMomY,fMuonMomZ);
		hMuonMomentum->Fill(fMuonMomentum);
		hMuonKinEnergy->Fill(fMuonKinEnergy);
		hMuonBeta->Fill(fMuonBeta);
	}

	if (fDecayVolume > -1)
	{
		hElectronTime->Fill(fElectronTime);
		hElectronPosX->Fill(fElectronPosX);
		hElectronPosY->Fill(fElectronPosY);
		hElectronPosZ->Fill(fElectronPosZ);
		hElectronPosXY->Fill(fElectronPosX,fElectronPosY);
		hElectronPosXZ->Fill(fElectronPosX,fElectronPosZ);
		hElectronPosYZ->Fill(fElectronPosY,fElectronPosZ);
		hElectronEnergy->Fill(fElectronEnergy);
		hElectronMomX->Fill(fElectronMomX);
		hElectronMomY->Fill(fElectronMomY);
		hElectronMomZ->Fill(fElectronMomZ);
		hElectronMomXY->Fill(fElectronMomX,fElectronMomY);
		hElectronMomXZ->Fill(fElectronMomX,fElectronMomZ);
		hElectronMomYZ->Fill(fElectronMomY,fElectronMomZ);
		hElectronMomentum->Fill(fElectronMomentum);
		hElectronKinEnergy->Fill(fElectronKinEnergy);
		hElectronBeta->Fill(fElectronBeta);
	}
	if (fInnerDetectorPhotonHits > 0)
		hInnerDetectorPhotonHits->Fill(fInnerDetectorPhotonHits);

	hInnerDetectorPhotonTime->Fill(fInnerDetectorPhotonTime);
	hInnerDetectorPhotonWavelength->Fill(fInnerDetectorPhotonWavelength);
	if (fOuterDetectorPhotonHits > 0)
		hOuterDetectorPhotonHits->Fill(fOuterDetectorPhotonHits);

	hOuterDetectorPhotonTime->Fill(fOuterDetectorPhotonTime);
	hOuterDetectorPhotonWavelength->Fill(fOuterDetectorPhotonWavelength);
}
}

void CosMuAnl::SetAddresses()
{
	data->SetBranchAddress("fPrimePosX",&fPrimePosX);
	data->SetBranchAddress("fPrimePosY",&fPrimePosY);
	data->SetBranchAddress("fPrimePosZ",&fPrimePosZ);
	data->SetBranchAddress("fPrimeEnergy",&fPrimeEnergy);
	data->SetBranchAddress("fPrimeMomX",&fPrimeMomX);
	data->SetBranchAddress("fPrimeMomY",&fPrimeMomY);
	data->SetBranchAddress("fPrimeMomZ",&fPrimeMomZ);
	data->SetBranchAddress("fPrimeMomentum",&fPrimeMomentum);
	data->SetBranchAddress("fPrimeKinEnergy",&fPrimeKinEnergy);
	data->SetBranchAddress("fPrimeBeta",&fPrimeBeta);
	data->SetBranchAddress("fCosZenithAngle",&fCosZenithAngle);
	data->SetBranchAddress("fPrimeCharge",&fPrimeCharge);

	data->SetBranchAddress("fMuonTime",&fMuonTime);
	data->SetBranchAddress("fMuonPosX",&fMuonPosX);
	data->SetBranchAddress("fMuonPosY",&fMuonPosY);
	data->SetBranchAddress("fMuonPosZ",&fMuonPosZ);
	data->SetBranchAddress("fMuonEnergy",&fMuonEnergy);
	data->SetBranchAddress("fMuonMomX",&fMuonMomX);
	data->SetBranchAddress("fMuonMomY",&fMuonMomY);
	data->SetBranchAddress("fMuonMomZ",&fMuonMomZ);
	data->SetBranchAddress("fMuonMomentum",&fMuonMomentum);
	data->SetBranchAddress("fMuonKinEnergy",&fMuonKinEnergy);
	data->SetBranchAddress("fMuonBeta",&fMuonBeta);
	data->SetBranchAddress("fMuonEntryVolume",&fMuonEntryVolume);

	data->SetBranchAddress("fElectronTime",&fElectronTime);
	data->SetBranchAddress("fElectronPosX",&fElectronPosX);
	data->SetBranchAddress("fElectronPosY",&fElectronPosY);
	data->SetBranchAddress("fElectronPosZ",&fElectronPosZ);
	data->SetBranchAddress("fElectronEnergy",&fElectronEnergy);
	data->SetBranchAddress("fElectronMomX",&fElectronMomX);
	data->SetBranchAddress("fElectronMomY",&fElectronMomY);
	data->SetBranchAddress("fElectronMomZ",&fElectronMomZ);
	data->SetBranchAddress("fElectronMomentum",&fElectronMomentum);
	data->SetBranchAddress("fElectronKinEnergy",&fElectronKinEnergy);
	data->SetBranchAddress("fElectronBeta",&fElectronBeta);
	data->SetBranchAddress("fDecayVolume",&fDecayVolume);

	data->SetBranchAddress("fInnerDetectorPhotonHits",&fInnerDetectorPhotonHits);
	data->SetBranchAddress("fInnerDetectorPhotonTime",&fInnerDetectorPhotonTime);
	data->SetBranchAddress("fInnerDetectorPhotonWavelength",&fInnerDetectorPhotonWavelength);
	data->SetBranchAddress("fOuterDetectorPhotonHits",&fOuterDetectorPhotonHits);
	data->SetBranchAddress("fOuterDetectorPhotonTime",&fOuterDetectorPhotonTime);
	data->SetBranchAddress("fOuterDetectorPhotonWavelength",&fOuterDetectorPhotonWavelength);
}

void CosMuAnl::CreateHistos()
{
	// 1D Histos

	TH1D* hPrimePosX = new TH1D("hPrimePosX","Muon Initial x-Position;position (cm);count",100,-80.,80.);
	TH1D* hPrimePosY = new TH1D("hPrimePosY","Muon Initial y-Position;position (cm);count",100,-80.,80.);
	TH1D* hPrimePosZ = new TH1D("hPrimePosZ","Muon Initial z-Position;position (cm);count",100,0.,50.);
	TH1D* hPrimeEnergy = new TH1D("hPrimeEnergy","Muon Initial Total Energy; energy (MeV); count",100,100.,260.);
	TH1D* hPrimeMomX  = new TH1D("hPrimeMomX","Muon Initial x-Momentum;momentum (MeV/c);count",100,-240.,240.);
	TH1D* hPrimeMomY = new TH1D("hPrimeMomY","Muon Initial y-Momentum;momentum (MeV/c);count",100,-240.,240.);
	TH1D* hPrimeMomZ = new TH1D("hPrimeMomZ","Muon Initial z-Momentum;momentum (MeV/c);count",100,-240.,0.);
	TH1D* hPrimeMomentum = new TH1D("hPrimeMomentum","Muon Initial Total Momentum;momentum (MeV/c);count",100,0.,240.);
	TH1D* hPrimeKinEnergy = new TH1D("hPrimeKinEnergy","Muon Initial Kinetic Energy;energy (MeV);count",100,0.,150.);
	TH1D* hPrimeBeta = new TH1D("hPrimeBeta","Muon Inital Speed;speed (fraction of c);count",100,0.,1.);
	TH1D* hCosZenithAngle = new TH1D("hCosZenithAngle","Cosine of FLux Zenith Angle",100,0.,1.);

	TH1D* hMuonTime = new TH1D("hMuonTime","Muon Time on Entry;time (ns);count",100,0.,5.);
	TH1D* hMuonPosX = new TH1D("hMuonPosX","Muon x-Position on Entry;position (cm);count",100,-16.,16.);
	TH1D* hMuonPosY = new TH1D("hMuonPosY","Muon y-Position on Entry;position (cm);count",100,-16.,16.);
	TH1D* hMuonPosZ = new TH1D("hMuonPosZ","Muon z-Position on Entry;position (cm);count",100,-13.,15.);
	TH1D* hMuonEnergy = new TH1D("hMuonEnergy","Muon Total Energy on Entry;energy (MeV);count",100,100.,260.);
	TH1D* hMuonMomX = new TH1D("hMuonMomX","Muon x-Momentum on Entry;momentum (MeV/c);count",100,-260.,260.);
	TH1D* hMuonMomY = new TH1D("hMuonMomY","Muon y-Momentum on Entry;momentum (MeV/c);count",100,-260.,260.);
	TH1D* hMuonMomZ = new TH1D("hMuonMomZ","Muon z-Momentum on Entry;momentum (MeV/c);count",100,-260.,0.);
	TH1D* hMuonMomentum = new TH1D("hMuonMomentum","Muon Total Momentum on Entry;momentum (MeV);count",100,0.,260.);
	TH1D* hMuonKinEnergy = new TH1D("hMuonKinEnergy","Muon Kinetic Energy on Entry;energy (MeV);count",100,0.,150.);
	TH1D* hMuonBeta = new TH1D("hMuonBeta","Muon Speed on Entry;speed (fraction of c);count",100,0.,1.);

	TH1D* hElectronTime = new TH1D("hElectronTime","Muon Decay Time;time (ns);count",100,0.,20000.);
	TH1D* hElectronPosX = new TH1D("hElectronPosX","Muon Decay x-Position;position (cm);count",100,-500.,500.);
	TH1D* hElectronPosY = new TH1D("hElectronPosY","Muon Decay y-Position;position (cm);count",100,-500.,500.);
	TH1D* hElectronPosZ = new TH1D("hElectronPosZ","Muon Decay z-Position;position (cm);count",100,-600.,40.);
	TH1D* hElectronEnergy = new TH1D("hElectronEnergy","Decay Electron Initial Total Energy;energy (MeV);count",100,0.,240.);
	TH1D* hElectronMomX = new TH1D("hElectronMomX","Decay Electron Initial x-Momentum; momentum (MeV/c);count",100,-240.,240.);
	TH1D* hElectronMomY = new TH1D("hElectronMomY","Decay Electron Initial y-Momentum; momentum (MeV/c);count",100,-240.,240.);
	TH1D* hElectronMomZ = new TH1D("hElectronMomZ","Decay Electron Initial z-Momentum; momentum (MeV/c);count",100,-240.,240.);
	TH1D* hElectronMomentum = new TH1D("hElectronMomentum","Decay Electron Initial Total Momentum;momentum (MeV/c);count",100,0.,240.);
	TH1D* hElectronKinEnergy = new TH1D("hElectronKinEnergy","Decay Electron Initial Kinetic Energy;energy (MeV);count",100,0.,240.);
	TH1D* hElectronBeta = new TH1D("hElectronBeta","Decay Electron Initial Speed;speed (fraction of c);count",100,0.999,1.);

	TH1I* hInnerDetectorPhotonHits = new TH1I("hInnerDetectorPhotonHits","Photons in the Inner Detector per Event;# of photons;# of events",100,0,5000);
	TH1D* hInnerDetectorPhotonTime = new TH1D("hInnerDetectorPhotonTime","Time of Photon Detection;time (ns);# of photons",100,0.,1000.);
	TH1D* hInnerDetectorPhotonWavelength = new TH1D("hInnerDetectorPhotonWavelength","Photon Wavelength in the Inner Detector;wavelength (nm);# of photons",100,200.,700.);

	TH1I* hOuterDetectorPhotonHits = new TH1I("hOuterDetectorPhotonHits","Photons in the Outer Detector per Event;# of photons;# of events",100,0,10000);
	TH1D* hOuterDetectorPhotonTime = new TH1D("hOuterDetectorPhotonTime","Time Of Photon Detection;time (ns);# of events",100,0.,100.);
	TH1D* hOuterDetectorPhotonWavelength = new TH1D("hOuterDetectorPhotonWavelength","Photons in the Outer Detector per Event;# of photons;# of events",100,200.,700.);

	// 2D Histos

	TH2D* hPrimePosXY = new TH2D("hPrimePosXY","Muon Initial xy-Position",100,-80.,80.,100,-80.,80.);
	TH2D* hPrimeMomXY = new TH2D("hPrimeMomXY","Muon Initial xy-momentum",100,-240.,240.,100,-240.,240.);
	TH2D* hPrimeMomXZ = new TH2D("hPrimeMomXZ","Muon Initial xz-momentum",100,-240.,240.,100,-240.,0.);
	TH2D* hPrimeMomYZ = new TH2D("hPrimeMomYZ","Muon Initial yz-momentum",100,-240.,240.,100,-240.,0.);

	TH2D* hMuonPosXY = new TH2D("hMuonPosXY","Muon xy-Position on entry",100,-16.,16.,100,-16.,16.);
	TH2D* hMuonPosXZ = new TH2D("hMuonPosXZ","Muon xz-Position on entry",100,-16.,16.,100,-16.,16.);
	TH2D* hMuonPosYZ = new TH2D("hMuonPosYZ","Muon yz-Position on entry",100,-16.,16.,100,-16.,16.);
	TH2D* hMuonMomXY = new TH2D("hMuonMomXY","Muon xy-Momentum on entry",100,-260.,260.,100,-260.,260.);
	TH2D* hMuonMomXZ = new TH2D("hMuonMomXZ","Muon xz-Momentum on entry",100,-260.,260.,100,-260.,0.);
	TH2D* hMuonMomYZ = new TH2D("hMuonMomYZ","Muon yz-Momentum on entry",100,-260.,260.,100,-260.,0.);

	TH2D* hElectronPosXY = new TH2D("hElectronPosXY","Electron initial xy-Position",100,-500.,500.,100,-500.,500.);
	TH2D* hElectronPosXZ = new TH2D("hElectronPosXZ","Electron initial xz-Position",100,-500.,500.,100,-600.,40.);
	TH2D* hElectronPosYZ = new TH2D("hElectronPosYZ","Electron initial yz-Position",100,-500.,500.,100,-600.,40.);
	TH2D* hElectronMomXY = new TH2D("hElectronMomXY","Electron initial xy-Momentum",100,-260.,260.,100,-260.,260.);
	TH2D* hElectronMomXZ = new TH2D("hElectronMomXZ","Electron initial xz-Momentum",100,-260.,260.,100,-260.,0.);
	TH2D* hElectronMomYZ = new TH2D("hElectronMomYZ","Electron initial yz-Momentum",100,-260.,260.,100,-260.,0.);
}