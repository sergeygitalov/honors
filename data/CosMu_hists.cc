#include <iostream>

#include "TCanvas.h"
#include "TStyle.h"

#include "HistogramManager.h"

// 1 Oe
// 2 OIOe
// 3 IOe
// 4 OdO
// 5 OdOI
// 6 OdOIO
// 7 OIdI
// 8 OIdIO
// 9 OIOdO
// 10 OIOdOI
// 11 OIOdOIO
// 12 IdI
// 13 IdIO
// 14 IOdO
// 15 IOdOI
// 16 IOdOIO

static const std::string cut[11] = {"raw", "Oe", "OIOe", "OdO", "OdOI", "OdOIO",
                                    "OIOdO", "OIOdOI", "OIOdOIO", "OIdI",
                                    "OIdIO"};

const void PrintCutRatios(HistogramManager& Man);
void DrawEnergy(HistogramManager& Man);
void Draw1D(HistogramManager& Man);
void Extract(HistogramManager& Man);
void ExtractEscapeAndDecay(HistogramManager& Man);

int main()
{
  HistogramManager Man;

  ExtractEscapeAndDecay(Man);
  DrawEnergy(Man);

  //Extract(Man);

  //PrintCutRatios(Man);

  //Draw1D(Man);

  return 0;
}

const void PrintCutRatios(HistogramManager& Man)
{
  std::vector<TH1*> v = Man.GetHists(1, "pdv","Mu;N/A;PosX");

  int start = 1;

  auto sum = 0;
  for (int i = start; i < v.size(); i++)
  {
    sum += v[i]->GetEntries();
  }
  std::cout << "Sum: " << sum << std::endl;
  for (int i = start; i < v.size(); i++)
  {
    auto ratio = v[i]->GetEntries()/sum;
    std::cout << std::endl << cut[i] << ":" << std::endl;
    std::cout << "entries = " << v[i]->GetEntries() << std::endl;
    std::cout << "ratio = " << ratio << std::endl;
  }
}

void DrawEnergy(HistogramManager& Man)
{
  TCanvas c;

  std::vector<TH1*> v = Man.GetHists(1, "pdmv", "Mu;N/A;N/A;Energy");

  std::cout << v.size() << std::endl;

  v[1]->SetLineColor(kOrange);
  v[2]->SetLineColor(kRed);

  for (auto h : v)
  {
    std::cout << h->GetName() << std::endl;
    h->Draw("same");
  }

  c.Print("test.pdf");
}

void Draw1D(HistogramManager& Man)
{
  std::vector<TH1*> vMuPosX = Man.GetHists(1, "pdv", "Mu;N/A;PosX");
  std::vector<TH1*> vMuPosY = Man.GetHists(1, "pdv", "Mu;N/A;PosY");
  std::vector<TH1*> vMuPosZ = Man.GetHists(1, "pdv", "Mu;N/A;PosZ");

  TCanvas c("c","c",1);
  gStyle->SetOptStat(10); // only display # of entries

  gStyle->SetStatX(0.9);
  gStyle->SetStatY(0.9);

  for (auto h : vMuPosX)
  {
    h->Draw("same");
  }
  c.Print("test.pdf");
}

void Extract(HistogramManager& Man)
{
  Man.ExtractFromFile("CosMu/mainTest/CosMu_histlist_raw.root","raw");
  Man.ExtractFromFile("CosMu/mainTest/CosMu_histlist_cOe.root","cOe");
  Man.ExtractFromFile("CosMu/mainTest/CosMu_histlist_cOIOe.root","cOIOe");
  Man.ExtractFromFile("CosMu/mainTest/CosMu_histlist_cOdO.root","cOdO");
  Man.ExtractFromFile("CosMu/mainTest/CosMu_histlist_cOdOI.root","cOdOI");
  Man.ExtractFromFile("CosMu/mainTest/CosMu_histlist_cOdOIO.root","cOdOIO");
  Man.ExtractFromFile("CosMu/mainTest/CosMu_histlist_cOIOdO.root","cOIOdO");
  Man.ExtractFromFile("CosMu/mainTest/CosMu_histlist_cOIOdOI.root","cOIOdOI");
  Man.ExtractFromFile("CosMu/mainTest/CosMu_histlist_cOIOdOIO.root","cOIOdOIO");
  Man.ExtractFromFile("CosMu/mainTest/CosMu_histlist_cOIdI.root","cOIdI");
  Man.ExtractFromFile("CosMu/mainTest/CosMu_histlist_cOIdIO.root","cOIdIO");
}

void ExtractEscapeAndDecay(HistogramManager& Man)
{
  Man.ExtractFromFile("CosMu/mainTest/CosMu_histlist_cEntry.root", "entry");
  Man.ExtractFromFile("CosMu/mainTest/CosMu_histlist_cEscape.root", "escape");
  Man.ExtractFromFile("CosMu/mainTest/CosMu_histlist_cDecay.root", "decay");
}
