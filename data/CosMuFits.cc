#include <iostream>
#include <vector>

#include "TStyle.h"
#include "TLegend.h"
#include "TFile.h"
#include "TList.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TH1.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF1.h"
#include "TF1Convolution.h"

using namespace std;

void printList(TList*);

void CosMuFits() {
//============================================================================//
  //
  // Extracting histograms from files
  //
//============================================================================//


  TFile* fRaw_hists = new TFile("CosMu_histlist_raw.root");
  TFile* fEntry_hists = new TFile("CosMu_histlist_cEntry.root");
  TFile* fDecay_hists = new TFile("CosMu_histlist_cDecay.root");
  TFile* fPass_hists = new TFile("CosMu_histlist_cPass.root");

  TList* lRaw_hists = (TList*)fRaw_hists->Get("histlist");
  TList* lEntry_hists = (TList*)fEntry_hists->Get("histlist");
  TList* lDecay_hists = (TList*)fDecay_hists->Get("histlist");
  TList* lPass_hists = (TList*)fPass_hists->Get("histlist");

  TH1D* fPrimePosX = (TH1D*)lRaw_hists->FindObject("fPrimePosX");
  TH1D* fPrimePosY = (TH1D*)lRaw_hists->FindObject("fPrimePosY");
  TH1D* fPrimePosZ = (TH1D*)lRaw_hists->FindObject("fPrimePosZ");
  TH2D* fPrimePosXY = (TH2D*)lRaw_hists->FindObject("fPrimePosX:fPrimePosY");
  TH1D* fPrimeEnergy = (TH1D*)lRaw_hists->FindObject("fPrimeEnergy");
  TH1D* fPrimeMomX = (TH1D*)lRaw_hists->FindObject("fPrimeMomX");
  TH1D* fPrimeMomY = (TH1D*)lRaw_hists->FindObject("fPrimeMomY");
  TH1D* fPrimeMomZ = (TH1D*)lRaw_hists->FindObject("fPrimeMomZ");
  TH2D* fPrimeMomXY = (TH2D*)lRaw_hists->FindObject("fPrimeMomX:fPrimeMomY");
  TH2D* fPrimeMomXZ = (TH2D*)lRaw_hists->FindObject("fPrimeMomX:fPrimeMomZ");
  TH2D* fPrimeMomYZ = (TH2D*)lRaw_hists->FindObject("fPrimeMomY:fPrimeMomZ");
  TH1D* fCosZenithAngle = (TH1D*)lRaw_hists->FindObject("fCosZenithAngle");
  TH1D* fMuTime = (TH1D*)lRaw_hists->FindObject("fMuTime");
  TH1D* fMuPosX = (TH1D*)lRaw_hists->FindObject("fMuPosX");
  TH1D* fMuPosY = (TH1D*)lRaw_hists->FindObject("fMuPosY");
  TH1D* fMuPosZ = (TH1D*)lRaw_hists->FindObject("fMuPosZ");
  TH2D* fMuPosXY = (TH2D*)lRaw_hists->FindObject("fMuPosX:fMuPosY");
  TH2D* fMuPosXZ = (TH2D*)lRaw_hists->FindObject("fMuPosX:fMuPosZ");
  TH2D* fMuPosYZ = (TH2D*)lRaw_hists->FindObject("fMuPosY:fMuPosZ");
  TH1D* fMuEnergy = (TH1D*)lRaw_hists->FindObject("fMuEnergy");
  TH1D* fMuMomX = (TH1D*)lRaw_hists->FindObject("fMuMomX");
  TH1D* fMuMomY = (TH1D*)lRaw_hists->FindObject("fMuMomY");
  TH1D* fMuMomZ = (TH1D*)lRaw_hists->FindObject("fMuMomZ");
  TH2D* fMuMomXY = (TH2D*)lRaw_hists->FindObject("fMuMomX:fMuMomY");
  TH2D* fMuMomXZ = (TH2D*)lRaw_hists->FindObject("fMuMomX:fMuMomZ");
  TH2D* fMuMomYZ = (TH2D*)lRaw_hists->FindObject("fMuMomY:fMuMomZ");
  TH1D* fETime = (TH1D*)lRaw_hists->FindObject("fETime");
  TH1D* fEPosX = (TH1D*)lRaw_hists->FindObject("fEPosX");
  TH1D* fEPosY = (TH1D*)lRaw_hists->FindObject("fEPosY");
  TH1D* fEPosZ = (TH1D*)lRaw_hists->FindObject("fEPosZ");
  TH2D* fEPosXY = (TH2D*)lRaw_hists->FindObject("fEPosX:fEPosY");
  TH2D* fEPosXZ = (TH2D*)lRaw_hists->FindObject("fEPosX:fEPosZ");
  TH2D* fEPosYZ = (TH2D*)lRaw_hists->FindObject("fEPosY:fEPosZ");
  TH1D* fEEnergy = (TH1D*)lRaw_hists->FindObject("fEEnergy");
  TH1D* fEMomX = (TH1D*)lRaw_hists->FindObject("fEMomX");
  TH1D* fEMomY = (TH1D*)lRaw_hists->FindObject("fEMomY");
  TH1D* fEMomZ = (TH1D*)lRaw_hists->FindObject("fEMomZ");
  TH2D* fEMomXY = (TH2D*)lRaw_hists->FindObject("fEMomX:fEMomY");
  TH2D* fEMomXZ = (TH2D*)lRaw_hists->FindObject("fEMomX:fEMomZ");
  TH2D* fEMomYZ = (TH2D*)lRaw_hists->FindObject("fEMomY:fEMomZ");
  TH1D* fIDMuPh = (TH1D*)lRaw_hists->FindObject("fIDMuPh");
  TH1D* fIDMuPhTime = (TH1D*)lRaw_hists->FindObject("fIDMuPhTime");
  TH1D* fIDMuPhWavelength = (TH1D*)lRaw_hists->FindObject("fIDMuPhWavelength");
  TH1D* fIDEPh = (TH1D*)lRaw_hists->FindObject("fIDEPh");
  TH1D* fIDEPhTime = (TH1D*)lRaw_hists->FindObject("fIDEPhTime");
  TH1D* fIDEPhWavelength = (TH1D*)lRaw_hists->FindObject("fIDEPhWavelength");
  TH1D* fODMuPh = (TH1D*)lRaw_hists->FindObject("fODMuPh");
  TH1D* fODMuPhTime = (TH1D*)lRaw_hists->FindObject("fODMuPhTime");
  TH1D* fODMuPhWavelength = (TH1D*)lRaw_hists->FindObject("fODMuPhWavelength");
  TH1D* fODEPh = (TH1D*)lRaw_hists->FindObject("fODEPh");
  TH1D* fODEPhTime = (TH1D*)lRaw_hists->FindObject("fODEPhTime");
  TH1D* fODEPhWavelength = (TH1D*)lRaw_hists->FindObject("fODEPhWavelength");
  TH1D* fIDPh = (TH1D*)lRaw_hists->FindObject("fIDPh");
  TH1D* fIDPhTime = (TH1D*)lRaw_hists->FindObject("fIDPhTime");
  TH1D* fIDPhWavelength = (TH1D*)lRaw_hists->FindObject("fIDPhWavelength");
  TH1D* fODPh = (TH1D*)lRaw_hists->FindObject("fODPh");
  TH1D* fODPhTime = (TH1D*)lRaw_hists->FindObject("fODPhTime");
  TH1D* fODPhWavelength = (TH1D*)lRaw_hists->FindObject("fODPhWavelength");

  TH1D* fPrimePosX_cEntry = (TH1D*)lEntry_hists->FindObject("fPrimePosX");
  TH1D* fPrimePosY_cEntry = (TH1D*)lEntry_hists->FindObject("fPrimePosY");
  TH1D* fPrimePosZ_cEntry = (TH1D*)lEntry_hists->FindObject("fPrimePosZ");
  TH2D* fPrimePosXY_cEntry = (TH2D*)lEntry_hists->FindObject("fPrimePosX:fPrimePosY");
  TH1D* fPrimeEnergy_cEntry = (TH1D*)lEntry_hists->FindObject("fPrimeEnergy");
  TH1D* fPrimeMomX_cEntry = (TH1D*)lEntry_hists->FindObject("fPrimeMomX");
  TH1D* fPrimeMomY_cEntry = (TH1D*)lEntry_hists->FindObject("fPrimeMomY");
  TH1D* fPrimeMomZ_cEntry = (TH1D*)lEntry_hists->FindObject("fPrimeMomZ");
  TH2D* fPrimeMomXY_cEntry = (TH2D*)lEntry_hists->FindObject("fPrimeMomX:fPrimeMomY");
  TH2D* fPrimeMomXZ_cEntry = (TH2D*)lEntry_hists->FindObject("fPrimeMomX:fPrimeMomZ");
  TH2D* fPrimeMomYZ_cEntry = (TH2D*)lEntry_hists->FindObject("fPrimeMomY:fPrimeMomZ");

  TH1D* fPrimePosX_cDecay = (TH1D*)lDecay_hists->FindObject("fPrimePosX");
  TH1D* fPrimePosY_cDecay = (TH1D*)lDecay_hists->FindObject("fPrimePosY");
  TH1D* fPrimePosZ_cDecay = (TH1D*)lDecay_hists->FindObject("fPrimePosZ");
  TH2D* fPrimePosXY_cDecay = (TH2D*)lDecay_hists->FindObject("fPrimePosX:fPrimePosY");
  TH1D* fPrimeEnergy_cDecay = (TH1D*)lDecay_hists->FindObject("fPrimeEnergy");
  TH1D* fPrimeMomX_cDecay = (TH1D*)lDecay_hists->FindObject("fPrimeMomX");
  TH1D* fPrimeMomY_cDecay = (TH1D*)lDecay_hists->FindObject("fPrimeMomY");
  TH1D* fPrimeMomZ_cDecay = (TH1D*)lDecay_hists->FindObject("fPrimeMomZ");
  TH2D* fPrimeMomXY_cDecay = (TH2D*)lDecay_hists->FindObject("fPrimeMomX:fPrimeMomY");
  TH2D* fPrimeMomXZ_cDecay = (TH2D*)lDecay_hists->FindObject("fPrimeMomX:fPrimeMomZ");
  TH2D* fPrimeMomYZ_cDecay = (TH2D*)lDecay_hists->FindObject("fPrimeMomY:fPrimeMomZ");
  TH1D* fMuTime_cDecay = (TH1D*)lDecay_hists->FindObject("fMuTime");
  TH1D* fMuPosX_cDecay = (TH1D*)lDecay_hists->FindObject("fMuPosX");
  TH1D* fMuPosY_cDecay = (TH1D*)lDecay_hists->FindObject("fMuPosY");
  TH1D* fMuPosZ_cDecay = (TH1D*)lDecay_hists->FindObject("fMuPosZ");
  TH2D* fMuPosXY_cDecay = (TH2D*)lDecay_hists->FindObject("fMuPosX:fMuPosY");
  TH2D* fMuPosXZ_cDecay = (TH2D*)lDecay_hists->FindObject("fMuPosX:fMuPosZ");
  TH2D* fMuPosYZ_cDecay = (TH2D*)lDecay_hists->FindObject("fMuPosY:fMuPosZ");
  TH1D* fMuEnergy_cDecay = (TH1D*)lDecay_hists->FindObject("fMuEnergy");
  TH1D* fMuMomX_cDecay = (TH1D*)lDecay_hists->FindObject("fMuMomX");
  TH1D* fMuMomY_cDecay = (TH1D*)lDecay_hists->FindObject("fMuMomY");
  TH1D* fMuMomZ_cDecay = (TH1D*)lDecay_hists->FindObject("fMuMomZ");
  TH2D* fMuMomXY_cDecay = (TH2D*)lDecay_hists->FindObject("fMuMomX:fMuMomY");
  TH2D* fMuMomXZ_cDecay = (TH2D*)lDecay_hists->FindObject("fMuMomX:fMuMomZ");
  TH2D* fMuMomYZ_cDecay = (TH2D*)lDecay_hists->FindObject("fMuMomY:fMuMomZ");
  TH1D* fIDMuPh_cDecay = (TH1D*)lDecay_hists->FindObject("fIDMuPh");
  TH1D* fIDMuPhTime_cDecay = (TH1D*)lDecay_hists->FindObject("fIDMuPhTime");
  TH1D* fIDMuPhWavelength_cDecay = (TH1D*)lDecay_hists->FindObject("fIDMuPhWavelength");
  TH1D* fIDEPh_cDecay = (TH1D*)lDecay_hists->FindObject("fIDEPh");
  TH1D* fIDEPhTime_cDecay = (TH1D*)lDecay_hists->FindObject("fIDEPhTime");
  TH1D* fIDEPhWavelength_cDecay = (TH1D*)lDecay_hists->FindObject("fIDEPhWavelength");
  TH1D* fODMuPh_cDecay = (TH1D*)lDecay_hists->FindObject("fODMuPh");
  TH1D* fODMuPhTime_cDecay = (TH1D*)lDecay_hists->FindObject("fODMuPhTime");
  TH1D* fODMuPhWavelength_cDecay = (TH1D*)lDecay_hists->FindObject("fODMuPhWavelength");
  TH1D* fODEPh_cDecay = (TH1D*)lDecay_hists->FindObject("fODEPh");
  TH1D* fODEPhTime_cDecay = (TH1D*)lDecay_hists->FindObject("fODEPhTime");
  TH1D* fODEPhWavelength_cDecay = (TH1D*)lDecay_hists->FindObject("fODEPhWavelength");
  TH1D* fIDPh_cDecay = (TH1D*)lDecay_hists->FindObject("fIDPh");
  TH1D* fODPh_cDecay = (TH1D*)lDecay_hists->FindObject("fODPh");

  TH1D* fPrimePosX_cPass = (TH1D*)lPass_hists->FindObject("fPrimePosX");
  TH1D* fPrimePosY_cPass = (TH1D*)lPass_hists->FindObject("fPrimePosY");
  TH1D* fPrimePosZ_cPass = (TH1D*)lPass_hists->FindObject("fPrimePosZ");
  TH2D* fPrimePosXY_cPass = (TH2D*)lPass_hists->FindObject("fPrimePosX:fPrimePosY");
  TH1D* fPrimeEnergy_cPass = (TH1D*)lPass_hists->FindObject("fPrimeEnergy");
  TH1D* fPrimeMomX_cPass = (TH1D*)lPass_hists->FindObject("fPrimeMomX");
  TH1D* fPrimeMomY_cPass = (TH1D*)lPass_hists->FindObject("fPrimeMomY");
  TH1D* fPrimeMomZ_cPass = (TH1D*)lPass_hists->FindObject("fPrimeMomZ");
  TH2D* fPrimeMomXY_cPass = (TH2D*)lPass_hists->FindObject("fPrimeMomX:fPrimeMomY");
  TH2D* fPrimeMomXZ_cPass = (TH2D*)lPass_hists->FindObject("fPrimeMomX:fPrimeMomZ");
  TH2D* fPrimeMomYZ_cPass = (TH2D*)lPass_hists->FindObject("fPrimeMomY:fPrimeMomZ");
  TH1D* fCosZenithAngle_cPass = (TH1D*)lPass_hists->FindObject("fCosZenithAngle");
  TH1D* fMuTime_cPass = (TH1D*)lPass_hists->FindObject("fMuTime");
  TH1D* fMuPosX_cPass = (TH1D*)lPass_hists->FindObject("fMuPosX");
  TH1D* fMuPosY_cPass = (TH1D*)lPass_hists->FindObject("fMuPosY");
  TH1D* fMuPosZ_cPass = (TH1D*)lPass_hists->FindObject("fMuPosZ");
  TH2D* fMuPosXY_cPass = (TH2D*)lPass_hists->FindObject("fMuPosX:fMuPosY");
  TH2D* fMuPosXZ_cPass = (TH2D*)lPass_hists->FindObject("fMuPosX:fMuPosZ");
  TH2D* fMuPosYZ_cPass = (TH2D*)lPass_hists->FindObject("fMuPosY:fMuPosZ");
  TH1D* fMuEnergy_cPass = (TH1D*)lPass_hists->FindObject("fMuEnergy");
  TH1D* fMuMomX_cPass = (TH1D*)lPass_hists->FindObject("fMuMomX");
  TH1D* fMuMomY_cPass = (TH1D*)lPass_hists->FindObject("fMuMomY");
  TH1D* fMuMomZ_cPass = (TH1D*)lPass_hists->FindObject("fMuMomZ");
  TH2D* fMuMomXY_cPass = (TH2D*)lPass_hists->FindObject("fMuMomX:fMuMomY");
  TH2D* fMuMomXZ_cPass = (TH2D*)lPass_hists->FindObject("fMuMomX:fMuMomZ");
  TH2D* fMuMomYZ_cPass = (TH2D*)lPass_hists->FindObject("fMuMomY:fMuMomZ");
  TH1D* fIDMuPh_cPass = (TH1D*)lPass_hists->FindObject("fIDMuPh");
  TH1D* fIDMuPhTime_cPass = (TH1D*)lPass_hists->FindObject("fIDMuPhTime");
  TH1D* fIDMuPhWavelength_cPass = (TH1D*)lPass_hists->FindObject("fIDMuPhWavelength");
  TH1D* fODMuPh_cPass = (TH1D*)lPass_hists->FindObject("fODMuPh");
  TH1D* fODMuPhTime_cPass = (TH1D*)lPass_hists->FindObject("fODMuPhTime");
  TH1D* fODMuPhWavelength_cPass = (TH1D*)lPass_hists->FindObject("fODMuPhWavelength");


//============================================================================//
  //
  // Plots of raw histograms (no fits)
  //
//============================================================================//

//----------------------------------------------------------------------------//
  //
  // Prime 1Ds: entry, decay, and pass on same graph
  //
//----------------------------------------------------------------------------//


  // Canvas style

  TCanvas c1("c1","c1",1);
  gStyle->SetOptStat(10); // only display # of entries

  gStyle->SetStatX(0.9);
  gStyle->SetStatY(0.9);

//----------------------------------------------------------------------------//

  fPrimePosX_cEntry->Draw();
  fPrimePosX_cPass->Draw("same");
  fPrimePosX_cPass->SetLineColor(kOrange);
  fPrimePosX_cDecay->Draw("same");
  fPrimePosX_cDecay->SetLineColor(kRed);

  TLegend legend1(0.1,0.7,0.3,0.9);
  legend1.AddEntry(fPrimePosX_cEntry,"entry");
  legend1.AddEntry(fPrimePosX_cPass,"escaped");
  legend1.AddEntry(fPrimePosX_cDecay,"decay");
  legend1.Draw();

  c1.Print("histsPrinted/fPrimePosX.pdf");

//----------------------------------------------------------------------------//

  fPrimePosY_cEntry->Draw();
  fPrimePosY_cPass->Draw("same");
  fPrimePosY_cPass->SetLineColor(kOrange);
  fPrimePosY_cDecay->Draw("same");
  fPrimePosY_cDecay->SetLineColor(kRed);

  TLegend legend2(0.1,0.7,0.3,0.9);
  legend2.AddEntry(fPrimePosY_cEntry,"entry");
  legend2.AddEntry(fPrimePosY_cPass,"escaped");
  legend2.AddEntry(fPrimePosY_cDecay,"decay");
  legend2.Draw();

  c1.Print("histsPrinted/fPrimePosY.pdf");

//----------------------------------------------------------------------------//

  fPrimeEnergy_cEntry->Draw();
  fPrimeEnergy_cPass->Draw("same");
  fPrimeEnergy_cPass->SetLineColor(kOrange);
  fPrimeEnergy_cDecay->Draw("same");
  fPrimeEnergy_cDecay->SetLineColor(kRed);

  TLegend legend3(0.1,0.7,0.2,0.9);
  legend3.AddEntry(fPrimeEnergy_cEntry,"entry");
  legend3.AddEntry(fPrimeEnergy_cPass,"escaped");
  legend3.AddEntry(fPrimeEnergy_cDecay,"decay");
  legend3.Draw();

  c1.Print("histsPrinted/fPrimeEnergy.pdf");

//----------------------------------------------------------------------------//

  fPrimeMomX_cEntry->Draw();
  fPrimeMomX_cPass->Draw("same");
  fPrimeMomX_cPass->SetLineColor(kOrange);
  fPrimeMomX_cDecay->Draw("same");
  fPrimeMomX_cDecay->SetLineColor(kRed);

  TLegend legend4(0.1,0.7,0.3,0.9);
  legend4.AddEntry(fPrimeMomX_cEntry,"entry");
  legend4.AddEntry(fPrimeMomX_cPass,"escaped");
  legend4.AddEntry(fPrimeMomX_cDecay,"decay");
  legend4.Draw();

  c1.Print("histsPrinted/fPrimeMomX.pdf");

//----------------------------------------------------------------------------//

  fPrimeMomY_cEntry->Draw();
  fPrimeMomY_cPass->Draw("same");
  fPrimeMomY_cPass->SetLineColor(kOrange);
  fPrimeMomY_cDecay->Draw("same");
  fPrimeMomY_cDecay->SetLineColor(kRed);

  TLegend legend5(0.1,0.7,0.3,0.9);
  legend5.AddEntry(fPrimeMomY_cEntry,"entry");
  legend5.AddEntry(fPrimeMomY_cPass,"escaped");
  legend5.AddEntry(fPrimeMomY_cDecay,"decay");
  legend5.Draw();

  c1.Print("histsPrinted/fPrimeMomY.pdf");

//----------------------------------------------------------------------------//

  fPrimeMomZ_cEntry->Draw();
  fPrimeMomZ_cPass->Draw("same");
  fPrimeMomZ_cPass->SetLineColor(kOrange);
  fPrimeMomZ_cDecay->Draw("same");
  fPrimeMomZ_cDecay->SetLineColor(kRed);

  TLegend legend6(0.1,0.7,0.3,0.9);
  legend6.AddEntry(fPrimeMomZ_cEntry,"entry");
  legend6.AddEntry(fPrimeMomZ_cPass,"escaped");
  legend6.AddEntry(fPrimeMomZ_cDecay,"decay");
  legend6.Draw();

  c1.Print("histsPrinted/fPrimeMomZ.pdf");

//----------------------------------------------------------------------------//

  // Entry position

  gStyle->SetStatX(0.85);
  gStyle->SetStatY(0.9);

  fMuPosX->Draw();
  fMuPosX_cPass->Draw("same");
  fMuPosX_cPass->SetLineColor(kOrange);
  fMuPosX_cDecay->Draw("same");
  fMuPosX_cDecay->SetLineColor(kRed);

  TLegend legend7(0.2,0.7,0.4,0.9);
  legend7.AddEntry(fMuPosX,"entry");
  legend7.AddEntry(fMuPosX_cPass,"escaped");
  legend7.AddEntry(fMuPosX_cDecay,"decay");
  legend7.Draw();

  c1.Print("histsPrinted/fMuPosX.pdf");

//----------------------------------------------------------------------------//

  fMuPosY->Draw();
  fMuPosY_cPass->Draw("same");
  fMuPosY_cPass->SetLineColor(kOrange);
  fMuPosY_cDecay->Draw("same");
  fMuPosY_cDecay->SetLineColor(kRed);

  TLegend legend8(0.2,0.7,0.4,0.9);
  legend8.AddEntry(fMuPosY,"entry");
  legend8.AddEntry(fMuPosY_cPass,"escaped");
  legend8.AddEntry(fMuPosY_cDecay,"decay");
  legend8.Draw();

  c1.Print("histsPrinted/fMuPosY.pdf");

//----------------------------------------------------------------------------//

  fMuPosZ->Draw();
  fMuPosZ_cPass->Draw("same");
  fMuPosZ_cPass->SetLineColor(kOrange);
  fMuPosZ_cDecay->Draw("same");
  fMuPosZ_cDecay->SetLineColor(kRed);

  TLegend legend9(0.1,0.7,0.3,0.9);
  legend9.AddEntry(fMuPosZ,"entry");
  legend9.AddEntry(fMuPosZ_cPass,"escaped");
  legend9.AddEntry(fMuPosZ_cDecay,"decay");
  legend9.Draw();

  c1.Print("histsPrinted/fMuPosZ.pdf");


//----------------------------------------------------------------------------//
  //
  // Electron time
  //
//----------------------------------------------------------------------------//

  gStyle->SetStatX(0.9);
  gStyle->SetStatY(0.9);
  gStyle->SetOptStat(1110); // no stat title
  gStyle->SetOptFit(111); // display chi2

  fETime->Fit("expo");
  c1.Print("histsPrinted/fETime.pdf");


//----------------------------------------------------------------------------//
  //
  // 2D histograms
  //
//----------------------------------------------------------------------------//

  // Canvas style

  TCanvas c2("c2","c2",2); // square 500x500
  gStyle->SetOptStat(1110); // removes title on statbox

  // Move the stat box to lower left corner
  gStyle->SetStatX(0.32);
  gStyle->SetStatY(0.33);

//----------------------------------------------------------------------------//

  // Prime

  fPrimePosXY->GetXaxis()->SetTitle("Position (cm)");
  fPrimePosXY->GetYaxis()->SetTitle("Position (cm)");
  fPrimePosXY->Draw("COLZ");
  c2.Print("histsPrinted/fPrimePosXY.pdf");

  fPrimeMomXY->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomXY->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomXY->Draw("COLZ");
  c2.Print("histsPrinted/fPrimeMomXY.pdf");

  fPrimeMomXZ->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomXZ->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomXZ->Draw("COLZ");
  c2.Print("histsPrinted/fPrimeMomXZ.pdf");

  fPrimeMomYZ->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomYZ->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomYZ->Draw("COLZ");
  c2.Print("histsPrinted/fPrimeMomYZ.pdf");

  fPrimePosXY_cEntry->SetTitle("Muon Initial xy-position (Reached detector)");
  fPrimePosXY_cEntry->GetXaxis()->SetTitle("Position (cm)");
  fPrimePosXY_cEntry->GetYaxis()->SetTitle("Position (cm)");
  fPrimePosXY_cEntry->Draw("COLZ");
  c2.Print("histsPrinted/fPrimePosXY_cEntry.pdf");

  fPrimeMomXY_cEntry->SetTitle("Muon Initial xy-momentum (Reached detector)");
  fPrimeMomXY_cEntry->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomXY_cEntry->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomXY_cEntry->Draw("COLZ");
  c2.Print("histsPrinted/fPrimeMomXY_cEntry.pdf");

  fPrimeMomXZ_cEntry->SetTitle("Muon Initial xz-momentum (Reached detector)");
  fPrimeMomXZ_cEntry->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomXZ_cEntry->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomXZ_cEntry->Draw("COLZ");
  c2.Print("histsPrinted/fPrimeMomXZ_cEntry.pdf");

  fPrimeMomYZ_cEntry->SetTitle("Muon Initial yz-momentum (Reached detector)");
  fPrimeMomYZ_cEntry->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomYZ_cEntry->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomYZ_cEntry->Draw("COLZ");
  c2.Print("histsPrinted/fPrimeMomYZ_cEntry.pdf");

  fPrimePosXY_cDecay->SetTitle("Muon Initial xy-position (Decayed inside)");
  fPrimePosXY_cDecay->GetXaxis()->SetTitle("Position (cm)");
  fPrimePosXY_cDecay->GetYaxis()->SetTitle("Position (cm)");
  fPrimePosXY_cDecay->Draw("COLZ");
  c2.Print("histsPrinted/fPrimePosXY_cDecay.pdf");

  fPrimeMomXY_cDecay->SetTitle("Muon Initial xy-momentum (Decayed inside)");
  fPrimeMomXY_cDecay->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomXY_cDecay->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomXY_cDecay->Draw("COLZ");
  c2.Print("histsPrinted/fPrimeMomXY_cDecay.pdf");

  fPrimeMomXZ_cDecay->SetTitle("Muon Initial xz-momentum (Decayed inside)");
  fPrimeMomXZ_cDecay->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomXZ_cDecay->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomXZ_cDecay->Draw("COLZ");
  c2.Print("histsPrinted/fPrimeMomXZ_cDecay.pdf");

  fPrimeMomYZ_cDecay->SetTitle("Muon Initial yz-momentum (Decayed inside)");
  fPrimeMomYZ_cDecay->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomYZ_cDecay->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomYZ_cDecay->Draw("COLZ");
  c2.Print("histsPrinted/fPrimeMomYZ_cDecay.pdf");

  fPrimePosXY_cPass->SetTitle("Muon Initial xy-position (Escaped)");
  fPrimePosXY_cPass->GetXaxis()->SetTitle("Position (cm)");
  fPrimePosXY_cPass->GetYaxis()->SetTitle("Position (cm)");
  fPrimePosXY_cPass->Draw("COLZ");
  c2.Print("histsPrinted/fPrimePosXY_cPass.pdf");

  fPrimeMomXY_cPass->SetTitle("Muon Initial xy-momentum (Escaped)");
  fPrimeMomXY_cPass->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomXY_cPass->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomXY_cPass->Draw("COLZ");
  c2.Print("histsPrinted/fPrimeMomXY_cPass.pdf");

  fPrimeMomXZ_cPass->SetTitle("Muon Initial xz-momentum (Escaped)");
  fPrimeMomXZ_cPass->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomXZ_cPass->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomXZ_cPass->Draw("COLZ");
  c2.Print("histsPrinted/fPrimeMomXZ_cPass.pdf");

  fPrimeMomYZ_cPass->SetTitle("Muon Initial yz-momentum (Escaped)");
  fPrimeMomYZ_cPass->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomYZ_cPass->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fPrimeMomYZ_cPass->Draw("COLZ");
  c2.Print("histsPrinted/fPrimeMomYZ_cPass.pdf");

//----------------------------------------------------------------------------//

  // Entry

  fMuPosXY->GetXaxis()->SetTitle("Position (cm)");
  fMuPosXY->GetYaxis()->SetTitle("Position (cm)");
  fMuPosXY->Draw("COLZ");
  c2.Print("histsPrinted/fMuPosXY.pdf");

  fMuPosXZ->GetXaxis()->SetTitle("Position (cm)");
  fMuPosXZ->GetYaxis()->SetTitle("Position (cm)");
  fMuPosXZ->Draw("COLZ");
  c2.Print("histsPrinted/fMuPosXZ.pdf");

  fMuPosYZ->GetXaxis()->SetTitle("Position (cm)");
  fMuPosYZ->GetYaxis()->SetTitle("Position (cm)");
  fMuPosYZ->Draw("COLZ");
  c2.Print("histsPrinted/fMuPosYZ.pdf");

  fMuMomXY->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fMuMomXY->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fMuMomXY->Draw("COLZ");
  c2.Print("histsPrinted/fMuMomXY.pdf");

  fMuMomXZ->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fMuMomXZ->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fMuMomXZ->Draw("COLZ");
  c2.Print("histsPrinted/fMuMomXZ.pdf");

  fMuMomYZ->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fMuMomYZ->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fMuMomYZ->Draw("COLZ");
  c2.Print("histsPrinted/fMuMomYZ.pdf");

  fMuPosXY_cDecay->SetTitle("Muon xy-position on Entry (Decayed inside)");
  fMuPosXY_cDecay->GetXaxis()->SetTitle("Position (cm)");
  fMuPosXY_cDecay->GetYaxis()->SetTitle("Position (cm)");
  fMuPosXY_cDecay->Draw("COLZ");
  c2.Print("histsPrinted/fMuPosXY_cDecay.pdf");

  fMuPosXZ_cDecay->SetTitle("Muon xz-position on Entry (Decayed inside)");
  fMuPosXZ_cDecay->GetXaxis()->SetTitle("Position (cm)");
  fMuPosXZ_cDecay->GetYaxis()->SetTitle("Position (cm)");
  fMuPosXZ_cDecay->Draw("COLZ");
  c2.Print("histsPrinted/fMuPosXZ_cDecay.pdf");

  fMuPosYZ_cDecay->SetTitle("Muon yz-position on Entry (Decayed inside)");
  fMuPosYZ_cDecay->GetXaxis()->SetTitle("Position (cm)");
  fMuPosYZ_cDecay->GetYaxis()->SetTitle("Position (cm)");
  fMuPosYZ_cDecay->Draw("COLZ");
  c2.Print("histsPrinted/fMuPosYZ_cDecay.pdf");

  fMuMomXY_cDecay->SetTitle("Muon xy-momentum on Entry (Decayed inside)");
  fMuMomXY_cDecay->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fMuMomXY_cDecay->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fMuMomXY_cDecay->Draw("COLZ");
  c2.Print("histsPrinted/fMuMomXY_cDecay.pdf");

  fMuMomXZ_cDecay->SetTitle("Muon xz-momentum on Entry (Decayed inside)");
  fMuMomXZ_cDecay->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fMuMomXZ_cDecay->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fMuMomXZ_cDecay->Draw("COLZ");
  c2.Print("histsPrinted/fMuMomXZ_cDecay.pdf");

  fMuMomYZ_cDecay->SetTitle("Muon yz-momentum on Entry (Decayed inside)");
  fMuMomYZ_cDecay->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fMuMomYZ_cDecay->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fMuMomYZ_cDecay->Draw("COLZ");
  c2.Print("histsPrinted/fMuMomYZ_cDecay.pdf");

  fMuPosXY_cPass->SetTitle("Muon xy-position on Entry (Escaped)");
  fMuPosXY_cPass->GetXaxis()->SetTitle("Position (cm)");
  fMuPosXY_cPass->GetYaxis()->SetTitle("Position (cm)");
  fMuPosXY_cPass->Draw("COLZ");
  c2.Print("histsPrinted/fMuPosXY_cPass.pdf");

  fMuPosXZ_cPass->SetTitle("Muon xz-position on Entry (Escaped)");
  fMuPosXZ_cPass->GetXaxis()->SetTitle("Position (cm)");
  fMuPosXZ_cPass->GetYaxis()->SetTitle("Position (cm)");
  fMuPosXZ_cPass->Draw("COLZ");
  c2.Print("histsPrinted/fMuPosXZ_cPass.pdf");

  fMuPosYZ_cPass->SetTitle("Muon yz-position on Entry (Escaped)");
  fMuPosYZ_cPass->GetXaxis()->SetTitle("Position (cm)");
  fMuPosYZ_cPass->GetYaxis()->SetTitle("Position (cm)");
  fMuPosYZ_cPass->Draw("COLZ");
  c2.Print("histsPrinted/fMuPosYZ_cPass.pdf");

  fMuMomXY_cPass->SetTitle("Muon xy-momentum on Entry (Escaped)");
  fMuMomXY_cPass->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fMuMomXY_cPass->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fMuMomXY_cPass->Draw("COLZ");
  c2.Print("histsPrinted/fMuMomXY_cPass.pdf");

  fMuMomXZ_cPass->SetTitle("Muon xz-momentum on Entry (Escaped)");
  fMuMomXZ_cPass->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fMuMomXZ_cPass->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fMuMomXZ_cPass->Draw("COLZ");
  c2.Print("histsPrinted/fMuMomXZ_cPass.pdf");

  fMuMomYZ_cPass->SetTitle("Muon yz-momentum on Entry (Escaped)");
  fMuMomYZ_cPass->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fMuMomYZ_cPass->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fMuMomYZ_cPass->Draw("COLZ");
  c2.Print("histsPrinted/fMuMomYZ_cPass.pdf");

//----------------------------------------------------------------------------//

  // Electron

  fEPosXY->GetXaxis()->SetTitle("Position (cm)");
  fEPosXY->GetYaxis()->SetTitle("Position (cm)");
  fEPosXY->Draw("COLZ");
  c2.Print("histsPrinted/fEPosXY.pdf");

  fEPosXZ->GetXaxis()->SetTitle("Position (cm)");
  fEPosXZ->GetYaxis()->SetTitle("Position (cm)");
  fEPosXZ->Draw("COLZ");
  c2.Print("histsPrinted/fEPosXZ.pdf");

  fEPosYZ->GetXaxis()->SetTitle("Position (cm)");
  fEPosYZ->GetYaxis()->SetTitle("Position (cm)");
  fEPosYZ->Draw("COLZ");
  c2.Print("histsPrinted/fEPosYZ.pdf");

  fEMomXY->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fEMomXY->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fEMomXY->Draw("COLZ");
  c2.Print("histsPrinted/fEMomXY.pdf");

  fEMomXZ->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fEMomXZ->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fEMomXZ->Draw("COLZ");
  c2.Print("histsPrinted/fEMomXZ.pdf");

  fEMomYZ->GetXaxis()->SetTitle("Momentum (MeV/c)");
  fEMomYZ->GetYaxis()->SetTitle("Momentum (MeV/c)");
  fEMomYZ->Draw("COLZ");
  c2.Print("histsPrinted/fEMomYZ.pdf");



//============================================================================//
  //
  // Plots w/ fits
  //
//============================================================================//


//----------------------------------------------------------------------------//
  //
  // Function declaration
  //
//----------------------------------------------------------------------------//


  // Function domain limits
  Double_t xmin = 0.;
  Double_t xmax = 1e5;

  Double_t scale_par = 1e5; // area under histogram

  // Gaussian

  TF1* gaus = new TF1("gaus","[2]*TMath::Gaus(x,[0],[1],true)",xmin,xmax);

  // Gaussian distribution parameters:
  Double_t mu = 2.7e4;
  Double_t sigma = 1.5e4;
  Double_t gaus_par[3] = {mu, sigma, scale_par};
  gaus->SetParameters(gaus_par);

  // Exponential

  TF1* expo = new TF1("expo","(x >= 0)*exp([1]+[0]*x) + (x < 0)*0",xmin,xmax);

  // Exponential distribution parameters:
  Double_t lambda = -1/2.7e4;
  Double_t constant = std::log(-lambda*scale_par); // so that area = scale_par
  Double_t expo_par[2] = {lambda, constant};
  expo->SetParameters(expo_par);
  expo->SetParLimits(0,-100,0); // only decay

  // Landau

  TF1* land = new TF1("land","[2]*TMath::Landau(x,[0],[1],true)",xmin,xmax);

  // Landau distribution parameters:
  Double_t l_location = 2.7e4;
  Double_t l_scale = 1.5e4;
  Double_t land_par[3] = {l_location, l_scale, scale_par};
  land->SetParameters(land_par);

  // Convolutions

  TF1Convolution* expogaus_conv = new TF1Convolution(expo,gaus,true);
  expogaus_conv->SetNofPointsFFT(1000);
  TF1* expogaus = new TF1("expogaus",*expogaus_conv,xmin,xmax,expogaus_conv->GetNpar());
  expogaus->SetParameter(0, expo_par[0]); // lambda
  expogaus->SetParLimits(0,-100,0); // only decay
  expogaus->FixParameter(1, 0); // only gaussian scale used
  expogaus->SetParameter(2, gaus_par[0]); // mu
  expogaus->SetParameter(3, gaus_par[1]); // sigma
  expogaus->SetParameter(4, gaus_par[2]); // scale


  TF1Convolution* landgaus_conv = new TF1Convolution(land,gaus,true);
  landgaus_conv->SetNofPointsFFT(1000);
  TF1* landgaus = new TF1("landgaus",*landgaus_conv,xmin,xmax,landgaus_conv->GetNpar());
  landgaus->SetParameter(0, land_par[0]); // landau location
  landgaus->SetParameter(1, land_par[1]); // landau sigma
  landgaus->FixParameter(2, 1); // only gaussian scale
  landgaus->SetParameter(3, gaus_par[0]); // mu
  landgaus->SetParameter(4, gaus_par[1]); // sigma
  landgaus->SetParameter(5, gaus_par[2]); // gaussian scale


  TF1Convolution* expoland_conv = new TF1Convolution(expo,land,true);
  expoland_conv->SetNofPointsFFT(1000);
  TF1* expoland = new TF1("expoland",*expoland_conv,xmin,xmax,expoland_conv->GetNpar());
  expoland->SetParameter(0, expo_par[0]); // lambda
  expoland->SetParLimits(0,-100,0); // only decay
  expoland->FixParameter(1, 0); // only landau scale
  expoland->SetParameter(2, land_par[0]);
  expoland->SetParameter(3, land_par[1]);
  expoland->SetParameter(4, land_par[2]);


//----------------------------------------------------------------------------//
  //
  // Outer Detector
  //
//----------------------------------------------------------------------------//

  gStyle->SetOptStat(1110); // no stat title
  gStyle->SetOptFit(111); // display chi2

  // Muon Photons

  fODMuPh->SetTitle("OD Photons (Muon parent) per event");
  fODMuPh->Fit("land","ME","E",1.2e4,xmax);
  fODMuPh->Fit("land","ME+","E",xmin,xmax);
  fODMuPh->GetFunction("land")->SetLineColor(kOrange);

  c1.Print("histsPrinted/fODMuPh_land.pdf");

//----------------------------------------------------------------------------//

  fODMuPh->SetTitle("OD Photons (Muon parent) per event");
  fODMuPh->Fit("expogaus","ME","E",1.2e4,xmax);
  fODMuPh->Fit("expogaus","ME+","E",xmin,xmax);
  fODMuPh->GetFunction("expogaus")->SetLineColor(kOrange);

  c1.Print("histsPrinted/fODMuPh_expogaus.pdf");

//----------------------------------------------------------------------------//

  fODMuPh->SetTitle("OD Photons (Muon parent) per event");
  fODMuPh->Fit("expoland","ME","E",1.2e4,xmax);
  fODMuPh->Fit("expoland","ME+","E",xmin,xmax);
  fODMuPh->GetFunction("expoland")->SetLineColor(kOrange);

  c1.Print("histsPrinted/fODMuPh_expoland.pdf");

//----------------------------------------------------------------------------//

  fODMuPh->SetTitle("OD Photons (Muon parent) per event");
  fODMuPh->Fit("landgaus","ME","E",1.2e4,xmax);
  fODMuPh->Fit("landgaus","ME+","E",xmin,xmax);
  fODMuPh->GetFunction("landgaus")->SetLineColor(kOrange);

  c1.Print("histsPrinted/fODMuPh_landgaus.pdf");

//----------------------------------------------------------------------------//

  fODMuPh_cDecay->SetTitle("OD Photons (Muon parent) per event");
  fODMuPh_cDecay->Fit("land","ME","E",1.2e4,xmax);
  fODMuPh_cDecay->Fit("land","ME+","E",xmin,xmax);
  fODMuPh_cDecay->GetFunction("land")->SetLineColor(kOrange);

  c1.Print("histsPrinted/fODMuPh_cDecay_land.pdf");

//----------------------------------------------------------------------------//

  fODMuPh_cDecay->SetTitle("OD Photons (Muon parent) per event");
  fODMuPh_cDecay->Fit("expogaus","ME","E",1.2e4,xmax);
  fODMuPh_cDecay->Fit("expogaus","ME+","E",xmin,xmax);
  fODMuPh_cDecay->GetFunction("expogaus")->SetLineColor(kOrange);

  c1.Print("histsPrinted/fODMuPh_cDecay_expogaus.pdf");

//----------------------------------------------------------------------------//

  fODMuPh_cDecay->SetTitle("OD Photons (Muon parent) per event");
  fODMuPh_cDecay->Fit("expoland","ME","E",1.2e4,xmax);
  fODMuPh_cDecay->Fit("expoland","ME+","E",xmin,xmax);
  fODMuPh_cDecay->GetFunction("expoland")->SetLineColor(kOrange);

  c1.Print("histsPrinted/fODMuPh_cDecay_expoland.pdf");

//----------------------------------------------------------------------------//

  fODMuPh_cDecay->SetTitle("OD Photons (Muon parent) per event");
  fODMuPh_cDecay->Fit("landgaus","ME","E",1.2e4,xmax);
  fODMuPh_cDecay->Fit("landgaus","ME+","E",xmin,xmax);
  fODMuPh_cDecay->GetFunction("landgaus")->SetLineColor(kOrange);

  c1.Print("histsPrinted/fODMuPh_cDecay_landgaus.pdf");

//----------------------------------------------------------------------------//

  fODMuPh_cPass->SetTitle("OD Photons (Muon parent) per event");
  fODMuPh_cPass->Fit("land","ME","E",1.2e4,xmax);
  fODMuPh_cPass->Fit("land","ME+","E",xmin,xmax);
  fODMuPh_cPass->GetFunction("land")->SetLineColor(kOrange);

  hist->Fit("gaus","","",xmin,xmax);

  c1.Print("histsPrinted/fODMuPh_cPass_land.pdf");

//----------------------------------------------------------------------------//

  fODMuPh_cPass->SetTitle("OD Photons (Muon parent) per event");
  fODMuPh_cPass->Fit("expogaus","ME","E",1.2e4,xmax);
  fODMuPh_cPass->Fit("expogaus","ME+","E",xmin,xmax);
  fODMuPh_cPass->GetFunction("expogaus")->SetLineColor(kOrange);

  c1.Print("histsPrinted/fODMuPh_cPass_expogaus.pdf");

//----------------------------------------------------------------------------//

  fODMuPh_cPass->SetTitle("OD Photons (Muon parent) per event");
  fODMuPh_cPass->Fit("expoland","ME","E",1.2e4,xmax);
  fODMuPh_cPass->Fit("expoland","ME+","E",xmin,xmax);
  fODMuPh_cPass->GetFunction("expoland")->SetLineColor(kOrange);

  c1.Print("histsPrinted/fODMuPh_cPass_expoland.pdf");

//----------------------------------------------------------------------------//

  fODMuPh_cPass->SetTitle("OD Photons (Muon parent) per event");
  fODMuPh_cPass->Fit("landgaus","ME","E",1.2e4,xmax);
  fODMuPh_cPass->Fit("landgaus","ME+","E",xmin,xmax);
  fODMuPh_cPass->GetFunction("landgaus")->SetLineColor(kOrange);

  c1.Print("histsPrinted/fODMuPh_cPass_landgaus.pdf");

//----------------------------------------------------------------------------//

/////////////////////////////////////TODO///////////////////////////////////////

  // Electron Photons

  fODEPh->Fit("expo","ME","",xmin,xmax);

  c1.Print("histsPrinted/fODEPh.pdf");

//----------------------------------------------------------------------------//

  // Time

  xmin = 0;
  xmax = 50;

  gaus->SetRange(xmin,xmax);
  expo->SetRange(xmin,xmax);
  land->SetRange(xmin,xmax);
  expogaus->SetRange(xmin,xmax);
  expoland->SetRange(xmin,xmax);
  landgaus->SetRange(xmin,xmax);

  scale_par = 5e13;
  gaus_par[0] = 5;
  gaus_par[1] = 3;
  gaus_par[2] = scale_par;
  expo_par[0] = -1/5;
  expo_par[1] = std::log(0.2*scale_par);
  land_par[0] = 3;
  land_par[1] = 1e-2;
  land_par[2] = scale_par;

  gaus->SetParameters(gaus_par);
  expo->SetParameters(expo_par);
  land->SetParameters(land_par);

  expogaus->SetParameter(0, expo_par[0]); // lambda
  expogaus->SetParLimits(0,-100,0); // only decay
  expogaus->FixParameter(1, 0); // only gaussian scale used
  expogaus->SetParameter(2, gaus_par[0]); // mu
  expogaus->SetParameter(3, gaus_par[1]); // sigma
  expogaus->SetParameter(4, gaus_par[2]); // scale

  landgaus->SetParameter(0, land_par[0]); // landau location
  landgaus->SetParameter(1, land_par[1]); // landau sigma
  landgaus->FixParameter(2, 1); // only gaussian scale
  landgaus->SetParameter(3, gaus_par[0]); // mu
  landgaus->SetParameter(4, gaus_par[1]); // sigma
  landgaus->SetParameter(5, gaus_par[2]); // gaussian scale

  expoland->SetParameter(0, expo_par[0]); // lambda
  expoland->SetParLimits(0,-100,0); // only decay
  expoland->FixParameter(1, 0); // only landau scale
  expoland->SetParameter(2, land_par[0]);
  expoland->SetParameter(3, land_par[1]);
  expoland->SetParameter(4, land_par[2]);

  fODMuPhTime->Fit("expo","ME","E",4,25);

  c1.Print("histsPrinted/fODMuPhTime.pdf");

  fODMuPhTime_cDecay->Draw();

  c1.Print("histsPrinted/fODMuPhTime_cPass.pdf");


//////////////////////////////////////TODO//////////////////////////////////////

//============================================================================//
  //
  // Deleting objects
  //
//============================================================================//


  delete fRaw_hists;
  delete fEntry_hists;
  delete fDecay_hists;
  delete fPass_hists;

  delete gaus;
  delete expo;
  delete land;

  delete expogaus_conv;
  delete expoland_conv;
  delete landgaus_conv;
}

#ifndef __CINT__

int main()
{
  CosMuFits();

  return 0;
}

#endif

void printList(TList* l)
{
}

