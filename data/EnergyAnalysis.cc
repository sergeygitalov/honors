{
  /*
  TFile* fEntry = new TFile("CosMu/mainTest/CosMu_histlist_cEntry.root");
  TFile* fEscape = new TFile("CosMu/mainTest/CosMu_histlist_cEscape.root");
  TFile* fDecay = new TFile("CosMu/mainTest/CosMu_histlist_cDecay.root");


  TList* lEntry = (TList*)fEntry->Get("histlist");
  TList* lEscape = (TList*)fEscape->Get("histlist");
  TList* lDecay = (TList*)fDecay->Get("histlist");

  TH1* fMuEnergy_Entry = (TH1*)lEntry->FindObject("fMuEnergy");
  TH1* fMuEnergy_Escape = (TH1*)lEscape->FindObject("fMuEnergy");
  TH1* fMuEnergy_Decay = (TH1*)lDecay->FindObject("fMuEnergy");

  fMuEnergy_Escape->SetLineColor(kRed);
  fMuEnergy_Decay->SetLineColor(kOrange);

  TCanvas c;
  fMuEnergy_Entry->Draw();
  fMuEnergy_Escape->Draw("same");
  fMuEnergy_Decay->Draw("same");

  TLegend legend(0.1,0.7,0.3,0.9);
  legend.AddEntry(fMuEnergy_Entry,"entry");
  legend.AddEntry(fMuEnergy_Escape,"escaped");
  legend.AddEntry(fMuEnergy_Decay,"decay");
  legend.Draw("same");
  */
  /*
  TFile* fEscape = new TFile("CosMu/mainTest/CosMu_histlist_cEscape.root");
  TFile* fOe = new TFile("CosMu/mainTest/CosMu_histlist_cOe.root");
  TFile* fOIOe = new TFile("CosMu/mainTest/CosMu_histlist_cOIOe.root");

  TList* lEscape = (TList*)fEscape->Get("histlist");
  TList* lOe = (TList*)fOe->Get("histlist");
  TList* lOIOe = (TList*)fOIOe->Get("histlist");

  TH1* fMuEnergy_Escape = (TH1*)lEscape->FindObject("fMuEnergy");
  TH1* fMuEnergy_Oe = (TH1*)lOe->FindObject("fMuEnergy");
  TH1* fMuEnergy_OIOe = (TH1*)lOIOe->FindObject("fMuEnergy");

  fMuEnergy_Oe->SetLineColor(kRed);
  fMuEnergy_OIOe->SetLineColor(kOrange);

  TCanvas c;
  fMuEnergy_Escape->Draw();
  fMuEnergy_Oe->Draw("same");
  fMuEnergy_OIOe->Draw("same");

  TLegend legend(0.1,0.7,0.3,0.9);
  legend.AddEntry(fMuEnergy_Escape,"escaped");
  legend.AddEntry(fMuEnergy_Oe,"O#epsilon");
  legend.AddEntry(fMuEnergy_OIOe,"OIO#epsilon");
  legend.Draw("same");
  */
  TFile* fDecay = new TFile("CosMu/mainTest/CosMu_histlist_cDecay.root");
  TFile* fOd = new TFile("CosMu/mainTest/CosMu_histlist_cOd.root");
  TFile* fOId = new TFile("CosMu/mainTest/CosMu_histlist_cOId.root");
  TFile* fOIOd = new TFile("CosMu/mainTest/CosMu_histlist_cOIOd.root");

  TList* lDecay = (TList*)fDecay->Get("histlist");
  TList* lOd = (TList*)fOd->Get("histlist");
  TList* lOId = (TList*)fOId->Get("histlist");
  TList* lOIOd = (TList*)fOIOd->Get("histlist");

  TH1* fMuEnergy_Decay = (TH1*)lDecay->FindObject("fMuEnergy");
  TH1* fMuEnergy_Od = (TH1*)lOd->FindObject("fMuEnergy");
  TH1* fMuEnergy_OId = (TH1*)lOId->FindObject("fMuEnergy");
  TH1* fMuEnergy_OIOd = (TH1*)lOIOd->FindObject("fMuEnergy");

  fMuEnergy_Od->SetLineColor(kRed);
  fMuEnergy_OId->SetLineColor(kOrange);
  fMuEnergy_OIOd->SetLineColor(kMagenta);

  TCanvas c;
  fMuEnergy_Decay->Draw();
  fMuEnergy_Od->Draw("same");
  fMuEnergy_OId->Draw("same");
  fMuEnergy_OIOd->Draw("same");

  TLegend legend(0.1,0.7,0.3,0.9);
  legend.AddEntry(fMuEnergy_Decay,"decayed");
  legend.AddEntry(fMuEnergy_Od,"O#delta");
  legend.AddEntry(fMuEnergy_OId,"OI#delta");
  legend.AddEntry(fMuEnergy_OIOd,"OIO#delta");
  legend.Draw("same");
}
