// \file weigh_wave.cc
// \brief Program to weight photons by quantum efficiency of the detector

#include <iostream>
#include <sstream>
#include <string>

#include "include/AnalysisManager.hh"

#include "TCanvas.h"
#include "TStopwatch.h"

//----------------------------------------------------------------------------//
// This program is almost identical in functionality to analysis_bulk.cc. The
// only new (and commented) functions are GetWeight() and FillHistos()
//----------------------------------------------------------------------------//

using namespace std;

namespace
{
    TH1D fSignalIDMu("fSignalIDMu","Muon Signal in ID;signal;# of events",100,1.,1000.);
    TH1D fSignalIDE("fSignalIDE","Electron Signal in ID;signal;# of events",100,1.,1000.);
    TH1D fSignalODMu("fSignalODMu","Muon Signal in OD;signal;# of events",100,1.,20000.);
    TH1D fSignalODE("fSignalODE","Electron Signal in OD;signal;# of events",100,1.,20000.);

    TH2D fMuIDEnergySignal("fMuIDEnergy:fSignalIDMu","Signal in ID vs Muon Energy on Entry",100,100.,260.,100,1.,1000.);
    TH2D fMuODEnergySignal("fMuODEnergy:fSignalODMu","Signal in OD vs Muon Energy on Entry",100,100.,260.,100,1.,20000.);
    TH2D fEIDEnergySignal("fEEnergy:fSignalIDE","Signal in ID vs Electron Initial Energy",100,0.,60.,100,1.,1000.);
    TH2D fEODEnergySignal("fEEnergy:fSignalODE","Signal in OD vs Electron Initial Energy",100,0.,60.,100,1.,20000.);

    TH2D fMuIDEnergyWavelength("fMuIDEnergy:fPhIDMuWavelength","",100,100.,260.,100,200.,700.);
    TH2D fMuODEnergyWavelength("fMuODEnergy:fPhODMuWavelength","",100,100.,260.,100,200.,700.);
    TH2D fEIDEnergyWavelength("fEEnergy:fPhIDEWavelength","",100,0.,60.,100,200.,700.);
    TH2D fEODEnergyWavelength("fEEnergy:fPhODEWavelength","",100,0.,60.,100,200.,700.);
}

void AddBranches(AnalysisManager& anl);
double GetWeight(double wavelength);
void FillHistos(AnalysisManager& anl, Cut_abs* C);
void FillAndSave(AnalysisManager& anl, Cut_abs* C, const char* filename, 
                 const char* suffix);

void weigh_wave(const char* filename)
{
    TStopwatch stopwatch;
    stopwatch.Start();

    cout << "BEGIN" << endl;
    cout << "Filename: " << filename << endl;

    string s1 = string(filename);

    AnalysisManager anl(s1.c_str());

    AddBranches(anl);

/*
    Cut_abs C; // always returns true

    // Define cuts Escape (ce) and Decay (cd) (either ID or OD, doesn't matter)
    // The entries themselves are then enough to tell
    // Create cO,cOI,cOIO for mu, e
    // Create cdO, cdOI, cdOIO, cdI, cdIO for electron
    // Combine the two to get the desired 10 cuts


    int val[3] = {0, 1, 2};
    int min_one = -1;
    int min_two = -2;

    Cut<Int_t> cMuIDEntry0(anl.GetBranch_int("fMuIDEntry"), "==", &val[0]);
    Cut<Int_t> cMuIDEntry1(anl.GetBranch_int("fMuIDEntry"), "==", &val[1]);
    Cut<Int_t> cMuODEntry1(anl.GetBranch_int("fMuODEntry"), "==", &val[1]);
    Cut<Int_t> cMuODEntry2(anl.GetBranch_int("fMuODEntry"), "==", &val[2]);

    Cut_abs cMu_O(&cMuODEntry1, "&&", &cMuIDEntry0);
    Cut_abs cMu_OI(&cMuODEntry1, "&&", &cMuIDEntry1);
    Cut_abs cMu_OIO(&cMuODEntry2, "&&", &cMuIDEntry1);

    Cut<Int_t> cEIDEntry0(anl.GetBranch_int("fEIDEntry"), "==", &val[0]);
    Cut<Int_t> cEIDEntry1(anl.GetBranch_int("fEIDEntry"), "==", &val[1]);

    Cut<Int_t> cEODEntry0(anl.GetBranch_int("fEODEntry"), "==", &val[0]);
    Cut<Int_t> cEODEntry1(anl.GetBranch_int("fEODEntry"), "==", &val[1]);

    Cut_abs cE_O(&cEODEntry0, "&&", &cEIDEntry0);
    Cut_abs cE_OI(&cEODEntry0, "&&", &cEIDEntry1);
    Cut_abs cE_OIO(&cEODEntry1, "&&", &cEIDEntry1);
    Cut_abs cE_I(&cEODEntry0, "&&", &cEIDEntry0);
    Cut_abs cE_IO(&cEODEntry1, "&&", &cEIDEntry0);

    Cut<Int_t> ce(anl.GetBranch_int("fMuDecay"),"==",&min_two);
    Cut<Int_t> cdInner(anl.GetBranch_int("fMuDecay"),"==",&val[1]);
    Cut<Int_t> cdOuter(anl.GetBranch_int("fMuDecay"),"==",&val[2]);

    Cut_abs cdO(&cE_O, "&&", &cdOuter);
    Cut_abs cdOI(&cE_OI, "&&", &cdOuter);
    Cut_abs cdOIO(&cE_OIO, "&&", &cdOuter);
    Cut_abs cdI(&cE_I, "&&", &cdInner);
    Cut_abs cdIO(&cE_IO, "&&", &cdInner);

    Cut_abs cOe(&cMu_O, "&&", &ce);
    Cut_abs cOIOe(&cMu_OIO, "&&", &ce);
    Cut_abs cOdO(&cMu_O, "&&", &cdO);
    Cut_abs cOdOI(&cMu_O, "&&", &cdOI);
    Cut_abs cOdOIO(&cMu_O, "&&", &cdOIO);
    Cut_abs cOIdI(&cMu_OI, "&&", &cdI);
    Cut_abs cOIdIO(&cMu_OI, "&&", &cdIO);
    Cut_abs cOIOdO(&cMu_OIO, "&&", &cdO);
    Cut_abs cOIOdOI(&cMu_OIO, "&&", &cdOI);
    Cut_abs cOIOdOIO(&cMu_OIO, "&&", &cdOIO);

    // Muon Capture

    Cut<Int_t> ck(anl.GetBranch_int("fMuDecay"),"==",&min_one);

    Cut_abs cOk(&cMu_O, "&&", &ck);
    Cut_abs cOIk(&cMu_OI, "&&", &ck);
    Cut_abs cOIOk(&cMu_OIO, "&&", &ck);
*/
/*    
    // Decay cuts
    //
    Cut<Int_t> cDecay_min2(anl.GetBranch_int("fMuDecay"), "==", &min_two);
    Cut<Int_t> cDecay_min1(anl.GetBranch_int("fMuDecay"), "==", &min_one);
    Cut<Int_t> cDecay0(anl.GetBranch_int("fMuDecay"), "==", &val[0]);
    Cut<Int_t> cDecay1(anl.GetBranch_int("fMuDecay"), "==", &val[1]);
    Cut<Int_t> cDecay2(anl.GetBranch_int("fMuDecay"), "==", &val[2]);
    Cut_abs cDecay_min2_0(&cDecay_min2, "||", &cDecay0); // escape

    // Muon entry cuts
    //
    Cut<Int_t> cMuOD1(anl.GetBranch_int("fMuODEntry"), "==", &val[1]);
    Cut<Int_t> cMuOD2(anl.GetBranch_int("fMuODEntry"), "==", &val[2]);
    Cut<Int_t> cMuID0(anl.GetBranch_int("fMuIDEntry"), "==", &val[0]);
    Cut<Int_t> cMuID1(anl.GetBranch_int("fMuIDEntry"), "==", &val[1]);
    Cut_abs cMuOD1_ID0(&cMuOD1, "&&", &cMuID0); // O
    Cut_abs cMuOD1_ID1(&cMuOD1, "&&", &cMuID1); // OI
    Cut_abs cMuOD2_ID1(&cMuOD2, "&&", &cMuID1); // OIO

    // Muon entry and decay
    //
    Cut_abs cOe(&cMuOD1_ID0, "&&", &cDecay_min2_0);
    Cut_abs cOIOe(&cMuOD2_ID1, "&&", &cDecay_min2_0);
    Cut_abs cOk(&cMuOD1_ID0, "&&", &cDecay_min1);
    Cut_abs cOIk(&cMuOD1_ID1, "&&", &cDecay_min1);
    Cut_abs cOIOk(&cMuOD2_ID1, "&&", &cDecay_min1);
    Cut_abs cOd(&cMuOD1_ID0, "&&", &cDecay2);
    Cut_abs cOId(&cMuOD1_ID1, "&&", &cDecay1);
    Cut_abs cOIOd(&cMuOD2_ID1, "&&", &cDecay2);

    // Electron entry cuts
    //
    Cut<Int_t> cEOD0(anl.GetBranch_int("fEODEntry"), "==", &val[0]);
    Cut<Int_t> cEOD1(anl.GetBranch_int("fEODEntry"), "==", &val[1]);
    Cut<Int_t> cEOD2(anl.GetBranch_int("fEODEntry"), "==", &val[2]);
    Cut<Int_t> cEID0(anl.GetBranch_int("fEIDEntry"), "==", &val[0]);
    Cut<Int_t> cEID1(anl.GetBranch_int("fEIDEntry"), "==", &val[1]);
    Cut<Int_t> cEID2(anl.GetBranch_int("fEIDEntry"), "==", &val[2]);
    Cut_abs cEOD0_ID0(&cEOD0, "&&", &cEID0); // dO or dI 
    Cut_abs cEOD0_ID1(&cEOD0, "&&", &cEID1); // dOI 
    Cut_abs cEOD1_ID0(&cEOD1, "&&", &cEID0); // dIO
    Cut_abs cEOD1_ID1(&cEOD1, "&&", &cEID1); // dOIO

    // Muon decay + electron cuts
    //
    Cut_abs cOdO(&cOd, "&&", &cEOD0_ID0);
    Cut_abs cOdOI(&cOd, "&&", &cEOD0_ID1);
    Cut_abs cOdOIO(&cOd, "&&", &cEOD1_ID1);
    Cut_abs cOIdI(&cOId, "&&", &cEOD0_ID0);
    Cut_abs cOIdIO(&cOId, "&&", &cEOD1_ID0);
    Cut_abs cOIOdO(&cOIOd, "&&", &cEOD0_ID0);
    Cut_abs cOIOdOI(&cOIOd, "&&", &cEOD0_ID1);
    Cut_abs cOIOdOIO(&cOIOd, "&&", &cEOD1_ID1);
*/

	Cut_abs C; // this is a test dummy cut, it always returns true

	// Define cuts Escape (ce) and Decay (cd) (either ID or OD, doesn't matter)
	// The entries themselves are then enough to tell
	// Create cO,cOI,cOIO for mu, e
	// Create cdO, cdOI, cdOIO, cdI, cdIO for electron
	// Combine the two to get the desired 10 cuts

    // We need to pass pointers in the cut, so this array is an alternative to
    // defining 3 separate pointers. It is used to check integer variables such
    // as decay state
    //
	int val[3] = {0, 1, 2};
	int min_one = -1;
	int min_two = -2;

//----------------------------------------------------------------------------//
// These are primary cuts that track entry of muons and electrons in the outer
// and inner detectors
//----------------------------------------------------------------------------//

	Cut<Int_t> cMuIDEntry0(anl.GetBranch_int("fMuIDEntry"), "==", &val[0]);
	Cut<Int_t> cMuIDEntry1(anl.GetBranch_int("fMuIDEntry"), "==", &val[1]);
	Cut<Int_t> cMuODEntry1(anl.GetBranch_int("fMuODEntry"), "==", &val[1]);
	Cut<Int_t> cMuODEntry2(anl.GetBranch_int("fMuODEntry"), "==", &val[2]);

	Cut<Int_t> cEIDEntry0(anl.GetBranch_int("fEIDEntry"), "==", &val[0]);
	Cut<Int_t> cEIDEntry1(anl.GetBranch_int("fEIDEntry"), "==", &val[1]);
	Cut<Int_t> cEODEntry0(anl.GetBranch_int("fEODEntry"), "==", &val[0]);
	Cut<Int_t> cEODEntry1(anl.GetBranch_int("fEODEntry"), "==", &val[1]);

	Cut<Int_t> ce(anl.GetBranch_int("fMuDecay"),"==", &min_two);
	Cut<Int_t> cdInner(anl.GetBranch_int("fMuDecay"),"==", &val[1]);
	Cut<Int_t> cdOuter(anl.GetBranch_int("fMuDecay"),"==", &val[2]);
	Cut<Int_t> ck(anl.GetBranch_int("fMuDecay"),"==", &min_one);

//----------------------------------------------------------------------------//
// We now combine the cuts above using the Cut_abs class, which works like a
// binary tree. The final result are 13 cuts that partition the dataset based
// on where muons and electrons went.
//----------------------------------------------------------------------------//

	Cut_abs cMu_O(&cMuODEntry1, "&&", &cMuIDEntry0);
	Cut_abs cMu_OI(&cMuODEntry1, "&&", &cMuIDEntry1);
	Cut_abs cMu_OIO(&cMuODEntry2, "&&", &cMuIDEntry1);

	Cut_abs cE_O(&cEODEntry0, "&&", &cEIDEntry0);
	Cut_abs cE_OI(&cEODEntry0, "&&", &cEIDEntry1);
	Cut_abs cE_OIO(&cEODEntry1, "&&", &cEIDEntry1);
	Cut_abs cE_I(&cEODEntry0, "&&", &cEIDEntry0);
	Cut_abs cE_IO(&cEODEntry1, "&&", &cEIDEntry0);

	Cut_abs cdO(&cE_O, "&&", &cdOuter);
	Cut_abs cdOI(&cE_OI, "&&", &cdOuter);
	Cut_abs cdOIO(&cE_OIO, "&&", &cdOuter);
	Cut_abs cdI(&cE_I, "&&", &cdInner);
	Cut_abs cdIO(&cE_IO, "&&", &cdInner);
    
    // These are the final cuts used to partition the data
    //
	Cut_abs cOe(&cMu_O, "&&", &ce);
	Cut_abs cOIOe(&cMu_OIO, "&&", &ce);
	Cut_abs cOdO(&cMu_O, "&&", &cdO);
	Cut_abs cOdOI(&cMu_O, "&&", &cdOI);
	Cut_abs cOdOIO(&cMu_O, "&&", &cdOIO);
	Cut_abs cOIdI(&cMu_OI, "&&", &cdI);
	Cut_abs cOIdIO(&cMu_OI, "&&", &cdIO);
	Cut_abs cOIOdO(&cMu_OIO, "&&", &cdO);
	Cut_abs cOIOdOI(&cMu_OIO, "&&", &cdOI);
	Cut_abs cOIOdOIO(&cMu_OIO, "&&", &cdOIO);
	Cut_abs cOk(&cMu_O, "&&", &ck);
	Cut_abs cOIk(&cMu_OI, "&&", &ck);
	Cut_abs cOIOk(&cMu_OIO, "&&", &ck);

    cout << "Filling:" << endl;

    cout << endl << "NO CUT:" << endl;
    FillAndSave(anl, &C, filename, "raw");

    cout << endl << "CUT 1 of 13: cOe" << endl;
    FillAndSave(anl, &cOe, filename, "cOe");

    cout << endl << "CUT 2 of 13: cOIOe" << endl;
    FillAndSave(anl, &cOIOe, filename, "cOIOe");

    cout << endl << "CUT 3 of 13: cOdO" << endl;
    FillAndSave(anl, &cOdO, filename, "cOdO");

    cout << endl << "CUT 4 of 13: cOdOI" << endl;
    FillAndSave(anl, &cOdOI, filename, "cOdOI");

    cout << endl << "CUT 5 of 13: cOdOIO" << endl;
    FillAndSave(anl, &cOdOIO, filename, "cOdOIO");
   
    cout << endl << "CUT 6 of 13: cOIdI" << endl;
    FillAndSave(anl, &cOIdI, filename, "cOIdI");
  
    cout << endl << "CUT 7 of 13: cOIdIO" << endl;
    FillAndSave(anl, &cOIdIO, filename, "cOIdIO");

    cout << endl << "CUT 8 of 13: cOIOdO" << endl;
    FillAndSave(anl, &cOIOdO, filename, "cOIOdO");

    cout << endl << "CUT 9 of 13: cOIOdOI" << endl;
    FillAndSave(anl, &cOIOdOI, filename, "cOIOdOI");

    cout << endl << "CUT 10 of 13: cOIOdOIO" << endl;
    FillAndSave(anl, &cOIOdOIO, filename, "cOIOdOIO");

    cout << endl << "CUT 11 of 13: cOk" << endl;
    FillAndSave(anl, &cOk, filename, "cOk");

    cout << endl << "CUT 12 of 13: cOIk" << endl;
    FillAndSave(anl, &cOIk, filename, "cOIk");

    cout << endl << "CUT 13 of 13: cOIOk" << endl;
    FillAndSave(anl, &cOIOk, filename, "cOIOk");

    cout << "END" << endl;

    stopwatch.Stop();

    cout << "Runtime: " << stopwatch.RealTime() << " s" << endl;
}

#ifndef __CINT__

int main(int argc, char* argv[])
{
    weigh_wave(argv[1]);

    return 0;
}

#endif

void AddBranches(AnalysisManager& anl)
{
    anl.AddBranch_int("fPhIDMu");
    anl.AddBranch_vec_double("fPhIDMuWavelength");
    anl.AddBranch_vec_int("fPhIDMuProcess");

    anl.AddBranch_int("fPhIDE");
    anl.AddBranch_vec_double("fPhIDEWavelength");
    anl.AddBranch_vec_int("fPhIDEProcess");

    anl.AddBranch_int("fPhODMu");
    anl.AddBranch_vec_double("fPhODMuWavelength");
    anl.AddBranch_vec_int("fPhODMuProcess");

    anl.AddBranch_int("fPhODE");
    anl.AddBranch_vec_double("fPhODEWavelength");
    anl.AddBranch_vec_int("fPhODEProcess");

    anl.AddBranch_double("fMuIDEnergy");
    anl.AddBranch_double("fMuODEnergy");
    anl.AddBranch_double("fEEnergy");

    anl.AddBranch_int("fMuIDEntry");
    anl.AddBranch_int("fMuODEntry");
    anl.AddBranch_int("fEIDEntry");
    anl.AddBranch_int("fEODEntry");
    anl.AddBranch_int("fMuDecay");
}

// This function outputs a weight given photon's wavelength. This is based on
// the photocathode's quantum effiency plot. Data in the plot is modeled as
// 10 evenly-spaced straight lines
//
double GetWeight(double wavelength) {
    double ref_wav[11]; // wavelengths where one line ends and another begins
    for (int i = 0; i < 11; i++)
        ref_wav[i] = 295. + 42.5*i; // even spacing

    // The following were obtained from a plot
    //
    double slope[12] = {0., 0.400, 0.235, 0.0235, -0.188, -0.188, -0.0941,
                        -0.134, -0.0565, -0.0191, -0.00188, 0.};
    double inter[12] = {0., -117.00, -61.41, 19.06, 108.53, 108.53, 60.76, 
                        82.76, 36.76, 13.00, 1.37, 0.};

    // check where in the range of ref_wav wavelength is, then get weight from
    // the line fit
    //
    for (int i = 0; i < 12; i++)
        if (wavelength < ref_wav[i])
            return 0.01 * (slope[i] * wavelength + inter[i]);
}

// Fill histograms using the GetWeight function and Cut class
//
void FillHistos(AnalysisManager& anl, Cut_abs* cut)
{
    Int_t nentries = (Int_t)anl.chain->GetEntries();
    for (Int_t i = 0; i < nentries; i++)
    {
        anl.chain->GetEntry(i);

        if (cut->ApplyCut())
        {
            std::vector<Double_t>* fPhIDMuWavelength 
                = anl.GetBranch_vec_double("fPhIDMuWavelength");
            std::vector<Double_t>* fPhODMuWavelength 
                = anl.GetBranch_vec_double("fPhODMuWavelength");
            std::vector<Double_t>* fPhIDEWavelength 
                = anl.GetBranch_vec_double("fPhIDEWavelength");
            std::vector<Double_t>* fPhODEWavelength 
                = anl.GetBranch_vec_double("fPhODEWavelength");
            Double_t fMuIDEnergy = *anl.GetBranch_double("fMuIDEnergy");
            Double_t fMuODEnergy = *anl.GetBranch_double("fMuODEnergy");
            Double_t fEEnergy = *anl.GetBranch_double("fEEnergy");

            Double_t IDMuSignal = 0.;
            Double_t ODMuSignal = 0.;
            Double_t IDESignal = 0.;
            Double_t ODESignal = 0.;

            for (int i = 0; i < fPhIDMuWavelength->size(); i++)
            {
                IDMuSignal += GetWeight(fPhIDMuWavelength->at(i));
                fMuIDEnergyWavelength.Fill(fMuIDEnergy, fPhIDMuWavelength->at(i));
            }
            for (int i = 0; i < fPhODMuWavelength->size(); i++) {
                ODMuSignal += GetWeight(fPhODMuWavelength->at(i));
                fMuODEnergyWavelength.Fill(fMuODEnergy, fPhODMuWavelength->at(i));
            }
            for (int i = 0; i < fPhIDEWavelength->size(); i++) {
                IDESignal += GetWeight(fPhIDEWavelength->at(i));
                fEIDEnergyWavelength.Fill(fEEnergy, fPhIDEWavelength->at(i));
            }
            for (int i = 0; i < fPhODEWavelength->size(); i++) {
                ODESignal += GetWeight(fPhODEWavelength->at(i));
                fEODEnergyWavelength.Fill(fEEnergy, fPhODEWavelength->at(i));
            }   

            //if (IDMuSignal >= 1. && fMuIDEnergy > 0.)
            //{
                fSignalIDMu.Fill(IDMuSignal);
                fMuIDEnergySignal.Fill(fMuIDEnergy, IDMuSignal);
            //}
            //if (ODMuSignal >= 1. && fMuODEnergy > 0.)
            //{
                fSignalODMu.Fill(ODMuSignal);
                fMuODEnergySignal.Fill(fMuODEnergy, ODMuSignal);
            //}
            //if (IDESignal >= 1. && fEEnergy > 0.)
            //{
                fSignalIDE.Fill(IDESignal);
                fEIDEnergySignal.Fill(fEEnergy, IDESignal);
            //}
            //if (ODESignal >= 1. && fEEnergy > 0.)
            //{
                fSignalODE.Fill(ODESignal);
                fEODEnergySignal.Fill(fEEnergy, ODESignal);
            //}
        }
    }
}

void FillAndSave(AnalysisManager& anl, Cut_abs* C, const char* filename, 
                 const char* prefix)
{
    FillHistos(anl, C);

    TList l;
    l.Add(&fSignalIDMu);
    l.Add(&fSignalODMu);
    l.Add(&fSignalIDE);
    l.Add(&fSignalODE);
    l.Add(&fMuIDEnergySignal);
    l.Add(&fMuODEnergySignal);
    l.Add(&fEIDEnergySignal);
    l.Add(&fEODEnergySignal);
    l.Add(&fMuIDEnergyWavelength);
    l.Add(&fMuODEnergyWavelength);
    l.Add(&fEIDEnergyWavelength);
    l.Add(&fEODEnergyWavelength);

    string fname = string(filename) + "_signal_" + string(prefix) + ".root";
    cout << "Writing to " << fname << endl;
    TFile f(fname.c_str(),"RECREATE");
    l.Write("histlist", TObject::kSingleKey);
}
