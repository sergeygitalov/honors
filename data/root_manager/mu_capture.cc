// \file analysis_bulk.cc
// \brief Program to convert CosmicMu root ntuples into histograms

#include <iostream>
#include <sstream>
#include <string>

#include "include/AnalysisManager.hh"

#include "TCanvas.h"
#include "TStopwatch.h"

using namespace std;

// Histograms are defined globally in the namespace to use them throughout 
// mulltiple functions
//
namespace
{
	TH1D fETime("fETime","Muon Decay Time;time (ns);count",100,0.,20000.);
}

void FillAndSave(AnalysisManager& anl, Cut_abs* C, const char* filename, 
                 const char* suffix);
void FillMuCosAndMom(AnalysisManager& anl, Cut_abs* C);
void FillPhSum(AnalysisManager& anl, Cut_abs* C);
void AddBranches(AnalysisManager& anl);
void AddHistograms(AnalysisManager& anl);

// main()
// extracts TTrees from .root files and uses AnalysisManager class to fill 
// histograms using data from TTree. Cuts are made using a custom Cut class.
// After filling, histograms are saved in a file as a TList.
void analysis_bulk(const char* filename)
{
	TStopwatch stopwatch;
	stopwatch.Start();

	cout << "BEGIN" << endl;
	cout << "Filename: " << filename << endl;

    // Initialize manager with name of the .root file
    //
	string s1 = string(filename);
	AnalysisManager anl(s1.c_str());

	AddBranches(anl); // Add branches from the file to the manager
	AddHistograms(anl); // Add histgrams defined in namespace to the manager

	Cut_abs C; // this is a test dummy cut, it always returns true

	// Define cuts Escape (ce) and Decay (cd) (either ID or OD, doesn't matter)
	// The entries themselves are then enough to tell
	// Create cO,cOI,cOIO for mu, e
	// Create cdO, cdOI, cdOIO, cdI, cdIO for electron
	// Combine the two to get the desired 10 cuts

    // We need to pass pointers in the cut, so this array is an alternative to
    // defining 3 separate pointers. It is used to check integer variables such
    // as decay state
    //
	int val[3] = {0, 1, 2};
	int min_one = -1;
	int min_two = -2;

//----------------------------------------------------------------------------//
// These are primary cuts that track entry of muons and electrons in the outer
// and inner detectors
//----------------------------------------------------------------------------//

	Cut<Int_t> cMuIDEntry0(anl.GetBranch_int("fMuIDEntry"), "==", &val[0]);
	Cut<Int_t> cMuIDEntry1(anl.GetBranch_int("fMuIDEntry"), "==", &val[1]);
	Cut<Int_t> cMuODEntry1(anl.GetBranch_int("fMuODEntry"), "==", &val[1]);
	Cut<Int_t> cMuODEntry2(anl.GetBranch_int("fMuODEntry"), "==", &val[2]);

	Cut<Int_t> cEIDEntry0(anl.GetBranch_int("fEIDEntry"), "==", &val[0]);
	Cut<Int_t> cEIDEntry1(anl.GetBranch_int("fEIDEntry"), "==", &val[1]);
	Cut<Int_t> cEODEntry0(anl.GetBranch_int("fEODEntry"), "==", &val[0]);
	Cut<Int_t> cEODEntry1(anl.GetBranch_int("fEODEntry"), "==", &val[1]);

	Cut<Int_t> ce(anl.GetBranch_int("fMuDecay"),"==", &min_two);
	Cut<Int_t> cdInner(anl.GetBranch_int("fMuDecay"),"==", &val[1]);
	Cut<Int_t> cdOuter(anl.GetBranch_int("fMuDecay"),"==", &val[2]);
	Cut<Int_t> ck(anl.GetBranch_int("fMuDecay"),"==", &min_one);

//----------------------------------------------------------------------------//
// We now combine the cuts above using the Cut_abs class, which works like a
// binary tree. The final result are 13 cuts that partition the dataset based
// on where muons and electrons went.
//----------------------------------------------------------------------------//

	Cut_abs cMu_O(&cMuODEntry1, "&&", &cMuIDEntry0);
	Cut_abs cMu_OI(&cMuODEntry1, "&&", &cMuIDEntry1);
	Cut_abs cMu_OIO(&cMuODEntry2, "&&", &cMuIDEntry1);

	Cut_abs cE_O(&cEODEntry0, "&&", &cEIDEntry0);
	Cut_abs cE_OI(&cEODEntry0, "&&", &cEIDEntry1);
	Cut_abs cE_OIO(&cEODEntry1, "&&", &cEIDEntry1);
	Cut_abs cE_I(&cEODEntry0, "&&", &cEIDEntry0);
	Cut_abs cE_IO(&cEODEntry1, "&&", &cEIDEntry0);

	Cut_abs cdO(&cE_O, "&&", &cdOuter);
	Cut_abs cdOI(&cE_OI, "&&", &cdOuter);
	Cut_abs cdOIO(&cE_OIO, "&&", &cdOuter);
	Cut_abs cdI(&cE_I, "&&", &cdInner);
	Cut_abs cdIO(&cE_IO, "&&", &cdInner);
    
    // These are the final cuts used to partition the data
    //
	Cut_abs cOe(&cMu_O, "&&", &ce);
	Cut_abs cOIOe(&cMu_OIO, "&&", &ce);
	Cut_abs cOdO(&cMu_O, "&&", &cdO);
	Cut_abs cOdOI(&cMu_O, "&&", &cdOI);
	Cut_abs cOdOIO(&cMu_O, "&&", &cdOIO);
	Cut_abs cOIdI(&cMu_OI, "&&", &cdI);
	Cut_abs cOIdIO(&cMu_OI, "&&", &cdIO);
	Cut_abs cOIOdO(&cMu_OIO, "&&", &cdO);
	Cut_abs cOIOdOI(&cMu_OIO, "&&", &cdOI);
	Cut_abs cOIOdOIO(&cMu_OIO, "&&", &cdOIO);
	Cut_abs cOk(&cMu_O, "&&", &ck);
	Cut_abs cOIk(&cMu_OI, "&&", &ck);
	Cut_abs cOIOk(&cMu_OIO, "&&", &ck);

    Cut<Int_t> cPosMu(anl.GetBranch_int("fMuCharge"),"==",&val[1]); 
    Cut<Int_t> cNegMu(anl.GetBranch_int("fMuCharge"),"==",&min_one); 

    Cut_abs cOdO_PosMu(&cOdO, "&&", &cPosMu);
    Cut_abs cOdOI_PosMu(&cOdOI, "&&", &cPosMu);
    Cut_abs cOdOIO_PosMu(&cOdOIO, "&&", &cPosMu);
    Cut_abs cOIdI_PosMu(&cOIdI, "&&", &cPosMu);
    Cut_abs cOIdIO_PosMu(&cOIdIO, "&&", &cPosMu);
    Cut_abs cOIOdO_PosMu(&cOIOdO, "&&", &cPosMu);
    Cut_abs cOIOdOI_PosMu(&cOIOdOI, "&&", &cPosMu);
    Cut_abs cOIOdOIO_PosMu(&cOIOdOIO, "&&", &cPosMu);

    Cut_abs cOdO_NegMu(&cOdO, "&&", &cNegMu);
    Cut_abs cOdOI_NegMu(&cOdOI, "&&", &cNegMu);
    Cut_abs cOdOIO_NegMu(&cOdOIO, "&&", &cNegMu);
    Cut_abs cOIdI_NegMu(&cOIdI, "&&", &cNegMu);
    Cut_abs cOIdIO_NegMu(&cOIdIO, "&&", &cNegMu);
    Cut_abs cOIOdO_NegMu(&cOIOdO, "&&", &cNegMu);
    Cut_abs cOIOdOI_NegMu(&cOIOdOI, "&&", &cNegMu);
    Cut_abs cOIOdOIO_NegMu(&cOIOdOIO, "&&", &cNegMu);

//----------------------------------------------------------------------------//

    // We now fill all the histograms and save them to separate files depending
    // on the cut.
    //
	cout << "Filling:" << endl;

	FillAndSave(anl, &cOdO_PosMu, filename, "cOdO_PosMu");
	FillAndSave(anl, &cOdOI_PosMu, filename, "cOdOI_PosMu");
	FillAndSave(anl, &cOdOIO_PosMu, filename, "cOdOIO_PosMu");
	FillAndSave(anl, &cOIdI_PosMu, filename, "cOIdI_PosMu");
	FillAndSave(anl, &cOIdIO_PosMu, filename, "cOIdIO_PosMu");
	FillAndSave(anl, &cOIOdO_PosMu, filename, "cOIOdO_PosMu");
	FillAndSave(anl, &cOIOdOI_PosMu, filename, "cOIOdOI_PosMu");
	FillAndSave(anl, &cOIOdOIO_PosMu, filename, "cOIOdOIO_PosMu");

	FillAndSave(anl, &cOdO_NegMu, filename, "cOdO_NegMu");
	FillAndSave(anl, &cOdOI_NegMu, filename, "cOdOI_NegMu");
	FillAndSave(anl, &cOdOIO_NegMu, filename, "cOdOIO_NegMu");
	FillAndSave(anl, &cOIdI_NegMu, filename, "cOIdI_NegMu");
	FillAndSave(anl, &cOIdIO_NegMu, filename, "cOIdIO_NegMu");
	FillAndSave(anl, &cOIOdO_NegMu, filename, "cOIOdO_NegMu");
	FillAndSave(anl, &cOIOdOI_NegMu, filename, "cOIOdOI_NegMu");
	FillAndSave(anl, &cOIOdOIO_NegMu, filename, "cOIOdOIO_NegMu");

	cout << "END" << endl;

	stopwatch.Stop();

	cout << "Runtime: " << stopwatch.RealTime() << " s" << endl;
}

#ifndef __CINT__

// The main() is defined this way because root v5 interctve sessions didn't
// allow main(), and the actual main() is defined in analysis_bulk()
//
int main(int argc, char* argv[])
{
	analysis_bulk(argv[1]);

	return 0;
}

#endif

// This method takes all the branches in TTree and fills histograms named
// exactly like the branches. It also fills custom histos like cosine of zenith
// angle, but the algorithm for that is a separate function. AnalysisManager
// only fills histograms that directly correspond to branches
//
void FillAndSave(AnalysisManager& anl, Cut_abs* C, const char* filename, 
                 const char* cutname)
{
    // reset histograms if they were filled previouly
    //
	anl.Reset();

	// 1D histos
	cout << "4/12: fE" << endl;
	anl.Fill(C, "fETime");

	// Save all the histos to a file
	cout << "saving" << endl;
	TList l;
	for (int i = 0; i < anl.GetNumHistograms_1D(); i++)
		l.Add(&anl.hists_1D[i]);
    // .root files are named "<filename>_histlist_<cutname>.root"
    //
	string fname = string(filename) + "_capture_" + string(cutname) + ".root";
	cout << "Writing to " << fname << endl;
	TFile f(fname.c_str(),"RECREATE");
    l.Write("histlist", TObject::kSingleKey);
}

// Extracts branches from TTree to allow AnalysisManager to work on them
//
void AddBranches(AnalysisManager& anl)
{
	anl.AddBranch_double("fMuPosX");
	anl.AddBranch_double("fMuPosY");
	anl.AddBranch_double("fMuPosZ");
	anl.AddBranch_double("fMuEnergy");
	anl.AddBranch_double("fMuMomX");
	anl.AddBranch_double("fMuMomY");
	anl.AddBranch_double("fMuMomZ");
	anl.AddBranch_int("fMuCharge");
	anl.AddBranch_int("fMuDecay");

	anl.AddBranch_double("fMuIDTime");
	anl.AddBranch_double("fMuIDPosX");
	anl.AddBranch_double("fMuIDPosY");
	anl.AddBranch_double("fMuIDPosZ");
	anl.AddBranch_double("fMuIDEnergy");
	anl.AddBranch_double("fMuIDMomX");
	anl.AddBranch_double("fMuIDMomY");
	anl.AddBranch_double("fMuIDMomZ");
	anl.AddBranch_double("fMuIDDistance");
	anl.AddBranch_int("fMuIDEntry");

	anl.AddBranch_double("fMuODTime");
	anl.AddBranch_double("fMuODPosX");
	anl.AddBranch_double("fMuODPosY");
	anl.AddBranch_double("fMuODPosZ");
	anl.AddBranch_double("fMuODEnergy");
	anl.AddBranch_double("fMuODMomX");
	anl.AddBranch_double("fMuODMomY");
	anl.AddBranch_double("fMuODMomZ");
	anl.AddBranch_double("fMuODDistance");
	anl.AddBranch_int("fMuODEntry");

	anl.AddBranch_double("fETime");
	anl.AddBranch_double("fEPosX");
	anl.AddBranch_double("fEPosY");
	anl.AddBranch_double("fEPosZ");
	anl.AddBranch_double("fEEnergy");
	anl.AddBranch_double("fEMomX");
	anl.AddBranch_double("fEMomY");
	anl.AddBranch_double("fEMomZ");

	anl.AddBranch_double("fEIDTime");
	anl.AddBranch_double("fEIDPosX");
	anl.AddBranch_double("fEIDPosY");
	anl.AddBranch_double("fEIDPosZ");
	anl.AddBranch_double("fEIDEnergy");
	anl.AddBranch_double("fEIDMomX");
	anl.AddBranch_double("fEIDMomY");
	anl.AddBranch_double("fEIDMomZ");
	anl.AddBranch_double("fEIDDistance");
	anl.AddBranch_int("fEIDEntry");

	anl.AddBranch_double("fEODTime");
	anl.AddBranch_double("fEODPosX");
	anl.AddBranch_double("fEODPosY");
	anl.AddBranch_double("fEODPosZ");
	anl.AddBranch_double("fEODEnergy");
	anl.AddBranch_double("fEODMomX");
	anl.AddBranch_double("fEODMomY");
	anl.AddBranch_double("fEODMomZ");
	anl.AddBranch_double("fEODDistance");
	anl.AddBranch_int("fEODEntry");

	anl.AddBranch_int("fPhIDMu");
	anl.AddBranch_vec_double("fPhIDMuTime");
	anl.AddBranch_vec_double("fPhIDMuWavelength");
	anl.AddBranch_vec_int("fPhIDMuProcess");

	anl.AddBranch_int("fPhIDE");
	anl.AddBranch_vec_double("fPhIDETime");
 	anl.AddBranch_vec_double("fPhIDEWavelength");
    anl.AddBranch_vec_int("fPhIDEProcess");

 	anl.AddBranch_int("fPhODMu");
	anl.AddBranch_vec_double("fPhODMuTime");
	anl.AddBranch_vec_double("fPhODMuWavelength");
	anl.AddBranch_vec_int("fPhODMuProcess");

	anl.AddBranch_int("fPhODE");
	anl.AddBranch_vec_double("fPhODETime");
 	anl.AddBranch_vec_double("fPhODEWavelength");
    anl.AddBranch_vec_int("fPhODEProcess");
}

// Add all the histograms defined in the namespace to AnalysisManager
//
void AddHistograms(AnalysisManager& anl)
{
	anl.AddHist(fETime);
}
