#include "HistogramManager.h"

#include <iostream>
#include <sstream>

#include "TFile.h"

void Histogram::ParseHist(TH1* h1)
{
    hist = h1;

    std::string name = (std::string)h1->GetName();
    
    // according to the naming scheme, ':' indicates a 2D histo, so if there is
    // this char in the name of the histogram, we parse it as a 2D histogram
    //
    if (name.find(":") == -1) // 1D histo
    {
        Parse(name, 0);
        Parse("", 1);
        is2D = false;
    }
    else // 2D histo
    {
        // split the name of the histogram into x-axis and y-axis parts
        //
        std::istringstream iss(name);

        std::string first;
        std::string second;

        std::getline(iss, first, ':');
        std::getline(iss, second);

        Parse(first, 0);
        Parse(second, 1);
        is2D = true;
    }
}

// Compares input name with a predefined set of strings and fills out variables
// in the struct Histogram based on these comparisons
//
void Histogram::Parse(std::string name, int index)
{
    int detector_pos = 0;
    int mother_pos = 0;
    int var_pos = 0;

    if (name.find("Mu") == 1)
    {
        particle[index] = "Mu";
        detector_pos = 3;
    }
    else if (name.find("E") == 1)
    {
        particle[index] = "E";
        detector_pos = 2;
    }
    else if (name.find("Ph") == 1)
    {
        particle[index] = "Ph";
        detector_pos = 3;
    }
    else
    {
        particle[index] = detector[index] = mother[index] = var[index] = "";
        return;
    }

    if (name.find("ID") == detector_pos)
    {
        detector[index] = "ID";
        mother_pos = detector_pos + 2;
    }
    else if (name.find("OD") == detector_pos)
    {
        detector[index] = "OD";
        mother_pos = detector_pos + 2;
    }
    else
    {
        detector[index] = "N/A";
        mother_pos = detector_pos;
    }

    if (name.find("Mu") == mother_pos)
    {
        mother[index] = "Mu";
        var_pos = mother_pos + 2;
    }
    else if (name.find("E") == mother_pos)
    {
        mother[index] = "E";
        var_pos = mother_pos + 1;
    }
    else
    {
        mother[index] = "N/A";
        var_pos = mother_pos;
    }

    var[index] = name.substr(var_pos);
}

// Iterates through TList in the root file and fills out the hists[] array
//
void HistogramManager::ExtractFromFile(const char* filename, 
                                       std::string cutname)
{
    TFile f(filename);
    TList* l = 0;
    f.GetObject("histlist",l);

    TH1* h1;
    TIter itr(l);

    while(h1 = (TH1D*)itr())
    {
        hists[sz].ParseHist(h1);
        hists[sz].cutname = cutname;
        sz++;
    }
}


// Format:
// dim is dimension of histogram (either 1 or 2)
// option takes "pdmvc" (particle, detector, mother, var, cutname)
// or substrings of it
// criteria takes semicolon-separated criteria names in the same order as
// in options
// e.g. option = "pdvc", criteria = "Mu;ID;PosX;OdOI"
std::vector<TH1*> HistogramManager::GetHists(int dim, std::string option, 
                                             std::string criteria)
{
    std::vector<TH1*> vec;

    std::istringstream iss(criteria);

    std::string arg;
    std::string args[5];

    int i = 0;
    while (std::getline(iss, arg, ';'))
    {
        args[i] = arg;
        i++;
    }

    std::string particle, detector, mother, var, cutname;
    particle = detector = mother = var = cutname = "";

    int p_index = option.find('p');
    int d_index = option.find('d');
    int m_index = option.find('m');
    int v_index = option.find('v');
    int c_index = option.find('c');

    if (p_index != -1)
        particle = args[p_index];
    if (d_index != -1)
        detector = args[d_index];
    if (m_index != -1)
        mother = args[m_index];
    if (v_index != -1)
        var = args[v_index];
    if (c_index != -1)
        cutname = args[c_index];

    for (int i = 0; i < sz; i++)
    {
        // If a criterion is present and not equal to current hist's criterion,
        // skip. Only if all criteria are satisified we add the vector
        if (dim == 1)
        {
        if (hists[i].is2D)
            continue;
        if (p_index != -1 && hists[i].particle[0].compare(particle))
            continue;
        if (d_index != -1 && hists[i].detector[0].compare(detector))
            continue;
        if (m_index != -1 && hists[i].mother[0].compare(mother))
            continue;
        if (v_index != -1 && hists[i].var[0].compare(var))
            continue;
        }
        else if (dim == 2)
        {
        if (!hists[i].is2D)
            continue;
        if (p_index != -1
            && (hists[i].particle[0].compare(particle) 
            && hists[i].particle[1].compare(particle)))
            continue;
        if (d_index != -1
            && (hists[i].detector[0].compare(detector) 
            && hists[i].detector[1].compare(detector)))
            continue;
        if (m_index != -1
            && (hists[i].mother[0].compare(mother) 
            && hists[i].mother[1].compare(mother)))
            continue;
        if (v_index != -1
            && (hists[i].var[0].compare(var) && hists[i].var[1].compare(var)))
            continue;
        }

        if (cutname.size() && hists[i].cutname.compare(cutname))
            continue;

        vec.push_back(hists[i].hist);
    }

    return vec;
}
