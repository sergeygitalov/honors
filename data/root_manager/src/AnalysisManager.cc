// \file AnalysisManager.cc
// \brief Implementation of AnalysisManager class

#include <iostream>
#include <sstream>

#include "../include/AnalysisManager.hh"

AnalysisManager::AnalysisManager(std::string filename)
{
    // Initialize TChain for one file
    // Enter file name without '.root'
    //
	chain = new TChain("data");
    std::string name = filename + ".root";
	chain->Add(name.c_str());
}

AnalysisManager::AnalysisManager(std::string filename, int numFiles)
{
    // Initialize TChain for multiple files
    // Files should be named filename0.root, filename1.root, ...
    //
	chain = new TChain("data");
	for (int i = 0; i < numFiles; i++)
	{
        // convert int to string
        //
		std::stringstream ss;
		ss << i;
		std::string str = ss.str();
        // concatenate and add
        //
		std::string name = filename + str + ".root";
		chain->Add(name.c_str());
	}
}

// For the following 4 methods, use the exact name of the root branch in file
//
void AnalysisManager::AddBranch_double(const char* n)
{
	data[numBranches].name = n;
	data[numBranches].dEntry = new Double_t;
	chain->SetBranchAddress(n,data[numBranches].dEntry);
	numBranches++;
}

void AnalysisManager::AddBranch_int(const char* n)
{
	data[numBranches].name = n;
	data[numBranches].iEntry = new Int_t;
	chain->SetBranchAddress(n,data[numBranches].iEntry);
	numBranches++;
}

void AnalysisManager::AddBranch_vec_double(const char* n)
{
	data[numBranches].name = n;
	chain->SetBranchAddress(n,&data[numBranches].vec_dEntry);
	numBranches++;
}

void AnalysisManager::AddBranch_vec_int(const char* n)
{
	data[numBranches].name = n;
	chain->SetBranchAddress(n,&data[numBranches].vec_iEntry);
	numBranches++;
}

// 1D histograms should have a root name exactly the same as the branch name in
// the root file
//
void AnalysisManager::AddHist(TH1D h)
{
	hists_1D[numHistograms_1D] = h;
	numHistograms_1D++;
}

// 2D histograms should named by their x-variable then followed by ':', and then
// y-variable name. So if we had a 2D histo plotting fEnergy on the x-axis and
// fAngle on the y-axis, the histogram should be named "fEnergy:fAngle"
//
void AnalysisManager::AddHist(TH2D h)
{
	hists_2D[numHistograms_2D] = h;
	numHistograms_2D++;
}

// All the Fill() methods use the same FillHelper() method to save up on space
// and avoid redundancy. 
//
void AnalysisManager::Fill()
{
	Cut_abs c;

	FillHelper(&c,0,numHistograms_1D,0,numHistograms_2D);
}

void AnalysisManager::Fill(const char* name)
{
	Cut_abs c;

	int histIndex = GetHist_1DIndex(name);

	if (histIndex != -1) // if 1D histogram
		FillHelper(&c,histIndex,histIndex+1,-1,0);
	else // if 2D histogram
	{
		histIndex = GetHist_2DIndex(name);
		FillHelper(&c,-1,0,histIndex,histIndex+1);
	}
}

void AnalysisManager::Fill(const char* begin, const char* end)
{
	Cut_abs c;

	int histIndex_begin = GetHist_1DIndex(begin);
	int histIndex_end = GetHist_1DIndex(end) + 1;

	if (histIndex_begin != -1)
		FillHelper(&c,histIndex_begin,histIndex_end,-1,0);
	else
	{
		histIndex_begin = GetHist_2DIndex(begin);
		histIndex_end = GetHist_2DIndex(end) + 1;
		FillHelper(&c,-1,0,histIndex_begin,histIndex_end);
	}
}

void AnalysisManager::Fill(Cut_abs* c)
{
	FillHelper(c,0,numHistograms_1D,0,numHistograms_2D);
}

void AnalysisManager::Fill(Cut_abs* c, const char* name)
{
	int histIndex = GetHist_1DIndex(name);

	if (histIndex != -1)
		FillHelper(c,histIndex,histIndex+1,-1,0);
	else
	{
		histIndex = GetHist_2DIndex(name);
		FillHelper(c,-1,0,histIndex,histIndex+1);
	}
}

void AnalysisManager::Fill(Cut_abs* c, const char* begin, const char* end)
{
	int histIndex_begin = GetHist_1DIndex(begin);
	int histIndex_end = GetHist_1DIndex(end) + 1;

	if (histIndex_begin != -1)
		FillHelper(c,histIndex_begin,histIndex_end,-1,0);
	else
	{
		histIndex_begin = GetHist_2DIndex(begin);
		histIndex_end = GetHist_2DIndex(end) + 1;
		FillHelper(c,-1,0,histIndex_begin,histIndex_end);
	}
}

Int_t* AnalysisManager::GetBranch_int(const char* name)
{
	int branchIndex = GetBranchIndex(name);

	if (branchIndex == -1)
		return NULL;

	return data[branchIndex].iEntry;
}

Double_t* AnalysisManager::GetBranch_double(const char* name)
{
	int branchIndex = GetBranchIndex(name);

	if (branchIndex == -1)
		return NULL;

	return data[branchIndex].dEntry;
}

std::vector<Double_t>* AnalysisManager::GetBranch_vec_double(const char* name)
{
	int branchIndex = GetBranchIndex(name);

	if (branchIndex == -1)
		return NULL;

	return data[branchIndex].vec_dEntry;
}

std::vector<Int_t>* AnalysisManager::GetBranch_vec_int(const char* name)
{
	int branchIndex = GetBranchIndex(name);

	if (branchIndex == -1)
		return NULL;

	return data[branchIndex].vec_iEntry;
}

// This method resets all the histograms using the Reset() method in TH1D class
//
void AnalysisManager::Reset()
{
	for (std::size_t i = 0; i < numHistograms_1D; i++)
		hists_1D[i].Reset();
	for (std::size_t i = 0; i < numHistograms_2D; i++)
		hists_2D[i].Reset();
}

int AnalysisManager::GetBranchIndex(const char* name)
{
	for (int i = 0; i < numBranches; i++)
		if (strcmp(name,data[i].name) == 0)
			return i;

	std::cout << "Error: Branch " << name << " not found" << std::endl;

	return -1;
}

int AnalysisManager::GetHist_1DIndex(const char* name)
{
	for (int i = 0; i < numHistograms_1D; i++)
		if (strcmp(name,hists_1D[i].GetName()) == 0)
			return i;

	return -1;
}

int AnalysisManager::GetHist_2DIndex(const char* name)
{
	for (int i = 0; i < numHistograms_2D; i++)
		if (strcmp(name,hists_2D[i].GetName()) == 0)
			return i;

	return -1;
}

// This is the most important method of this class. It fills either 1D hists or 
// 2D hists in the order they were added from 'begin' to the one before 'end' 
// and applies a cut specified by 'cut'.
//
void AnalysisManager::FillHelper(Cut_abs* cut, int begin_1D, int end_1D, 
                                 int begin_2D, int end_2D)
{
	Int_t nentries = (Int_t)chain->GetEntries();
	for (Int_t i = 0; i < nentries; i++)
	{
		chain->GetEntry(i);
        
        // check for errors
        //
		if (cut->ApplyCut() == -1)
			return;
        
        // apply cut
        //
		if (cut->ApplyCut())
		{
			// 1D histograms

			if (begin_1D != -1)
				for (int j = begin_1D; j < end_1D; j++)
				{
					int branchIndex = GetBranchIndex(hists_1D[j].GetName());
                    
                    // look through all the BranchData variables to find a
                    // non-empty one
                    //
					if (data[branchIndex].dEntry != NULL)
					{
						if (*data[branchIndex].dEntry != -INT_MAX)
							hists_1D[j].Fill(*data[branchIndex].dEntry);
					}
					else if (data[branchIndex].iEntry != NULL)
						hists_1D[j].Fill(*data[branchIndex].iEntry);
                    // for vectors, loop through the entire vector
                    //
					else if (data[branchIndex].vec_dEntry != NULL)
						for (std::size_t k = 0; k < data[branchIndex].vec_dEntry->size(); k++)
							hists_1D[j].Fill(data[branchIndex].vec_dEntry->at(k));
					else if (data[branchIndex].vec_iEntry != NULL)
						for (std::size_t k = 0; k < data[branchIndex].vec_iEntry->size(); k++)
							hists_1D[j].Fill(data[branchIndex].vec_iEntry->at(k));
					else // occurs when BranchData is empty or uninitialized
					{
						std::cout << "ERROR: Branch address == NULL" 
                                  << std::endl;
						std::cout << data[branchIndex].name << std::endl;
						return;
					}
				}

			// 2D Histograms
            // same idea as above, but for 2 variables this time
            // this does not support vectors, but you can add them if need be
            //
			if (begin_2D != -1)
				for (int j = begin_2D; j < end_2D; j++)
				{
                    // parse the name of the histogram to separate x and y
                    // variables
                    //
					std::string name = hists_2D[j].GetName();
					std::stringstream ss(name);
					std::string x;
					std::string y;
					std::getline(ss,x,':');
					std::getline(ss,y);

					int branchIndex_x = GetBranchIndex(x.c_str());
					int branchIndex_y = GetBranchIndex(y.c_str());

					if (data[branchIndex_x].dEntry != NULL)
					{
						if (data[branchIndex_y].dEntry != NULL)
							hists_2D[j].Fill(*data[branchIndex_x].dEntry,
                                             *data[branchIndex_y].dEntry);
						else if (data[branchIndex_y].iEntry != NULL)
							hists_2D[j].Fill(*data[branchIndex_x].dEntry,
                                             *data[branchIndex_y].iEntry);
					}
					else if (data[branchIndex_x].iEntry != NULL)
					{
						if (data[branchIndex_y].dEntry != NULL)
							hists_2D[j].Fill(*data[branchIndex_x].iEntry,
                                             *data[branchIndex_y].dEntry);
						else if (data[branchIndex_y].iEntry != NULL)
							hists_2D[j].Fill(*data[branchIndex_x].iEntry,
                                             *data[branchIndex_y].iEntry);
					}
				}
		}
	}
}
