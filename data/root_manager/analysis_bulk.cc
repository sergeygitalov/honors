// \file analysis_bulk.cc
// \brief Program to convert CosmicMu root ntuples into histograms

#include <iostream>
#include <sstream>
#include <string>

#include "include/AnalysisManager.hh"

#include "TCanvas.h"
#include "TStopwatch.h"

using namespace std;

// Histograms are defined globally in the namespace to use them throughout 
// mulltiple functions
//
namespace
{
	TH1D fMuPosX("fMuPosX","Muon Initial x-Position;position (cm);count",100,-80.,80.);
	TH1D fMuPosY("fMuPosY","Muon Initial y-Position;position (cm);count",100,-80.,80.);
	TH1D fMuPosZ("fMuPosZ","Muon Initial z-Position;position (cm);count",100,0.,50.);
	TH1D fMuEnergy("fMuEnergy","Muon Initial Total Energy; energy (MeV); count",100,100.,260.);
	TH1D fMuMomX ("fMuMomX","Muon Initial x-Momentum;momentum (MeV/c);count",100,-240.,240.);
	TH1D fMuMomY("fMuMomY","Muon Initial y-Momentum;momentum (MeV/c);count",100,-240.,240.);
	TH1D fMuMomZ("fMuMomZ","Muon Initial z-Momentum;momentum (MeV/c);count",100,-240.,0.);

	TH1D fMuIDTime("fMuIDTime","Muon Time on Entry;time (ns);count",100,0.,5.);
	TH1D fMuIDPosX("fMuIDPosX","Muon x-Position on ID Entry;position (cm);count",100,-16.,16.);
	TH1D fMuIDPosY("fMuIDPosY","Muon y-Position on ID Entry;position (cm);count",100,-16.,16.);
	TH1D fMuIDPosZ("fMuIDPosZ","Muon z-Position on ID Entry;position (cm);count",100,-13.,15.);
	TH1D fMuIDEnergy("fMuIDEnergy","Muon Total Energy on ID Entry;energy (MeV);count",100,100.,260.);
	TH1D fMuIDMomX("fMuIDMomX","Muon x-Momentum on ID Entry;momentum (MeV/c);count",100,-260.,260.);
	TH1D fMuIDMomY("fMuIDMomY","Muon y-Momentum on ID Entry;momentum (MeV/c);count",100,-260.,260.);
	TH1D fMuIDMomZ("fMuIDMomZ","Muon z-Momentum on ID Entry;momentum (MeV/c);count",100,-260.,0.);
	TH1D fMuIDDistance("fMuIDDistance","Distance Traveled by Muon in ID;distance (cm);count",100,0.,40.);

	TH1D fMuODTime("fMuODTime","Muon Time on OD Entry;time (ns);count",100,0.,5.);
	TH1D fMuODPosX("fMuODPosX","Muon x-Position on OD Entry;position (cm);count",100,-16.,16.);
	TH1D fMuODPosY("fMuODPosY","Muon y-Position on OD Entry;position (cm);count",100,-16.,16.);
	TH1D fMuODPosZ("fMuODPosZ","Muon z-Position on OD Entry;position (cm);count",100,-13.,15.);
	TH1D fMuODEnergy("fMuODEnergy","Muon Total Energy on OD Entry;energy (MeV);count",100,100.,260.);
	TH1D fMuODMomX("fMuODMomX","Muon x-Momentum on OD Entry;momentum (MeV/c);count",100,-260.,260.);
	TH1D fMuODMomY("fMuODMomY","Muon y-Momentum on OD Entry;momentum (MeV/c);count",100,-260.,260.);
	TH1D fMuODMomZ("fMuODMomZ","Muon z-Momentum on OD Entry;momentum (MeV/c);count",100,-260.,0.);
	TH1D fMuODDistance("fMuODDistance","Distance Traveled by Muon in OD;distance (cm);count",100,0.,40.);

	TH1D fETime("fETime","Muon Decay Time;time (ns);count",100,0.,20000.);
	TH1D fEPosX("fEPosX","Muon Decay x-Position;position (cm);count",100,-20.,20.);
	TH1D fEPosY("fEPosY","Muon Decay y-Position;position (cm);count",100,-20.,20.);
	TH1D fEPosZ("fEPosZ","Muon Decay z-Position;position (cm);count",100,-20.,20.);
	TH1D fEEnergy("fEEnergy","Electron Initial Energy;energy (MeV);count",100,0.,60.);
	TH1D fEMomX("fEMomX","Electron Initial x-Momentum; momentum (MeV/c);count",100,-60.,60.);
	TH1D fEMomY("fEMomY","Electron Initial y-Momentum; momentum (MeV/c);count",100,-60.,60.);
	TH1D fEMomZ("fEMomZ","Electron Initial z-Momentum; momentum (MeV/c);count",100,-60.,60.);

	TH1D fEIDTime("fEIDTime","Electron Time of ID Entry;time (ns);count",100,0.,20000.);
	TH1D fEIDPosX("fEIDPosX","Electron x-Position on ID Entry;position (cm);count",100,-20.,20.);
	TH1D fEIDPosY("fEIDPosY","Electron y-Position on ID Entry;position (cm);count",100,-20.,20.);
	TH1D fEIDPosZ("fEIDPosZ","Electron z-Position on ID Entry;position (cm);count",100,-20.,20.);
	TH1D fEIDEnergy("fEIDEnergy","Electron Energy on ID Entry;energy (MeV);count",100,0.,60.);
	TH1D fEIDMomX("fEIDMomX","Electron x-Momentum on ID Entry; momentum (MeV/c);count",100,-60.,60.);
	TH1D fEIDMomY("fEIDMomY","Electron y-Momentum on ID Entry; momentum (MeV/c);count",100,-60.,60.);
	TH1D fEIDMomZ("fEIDMomZ","Electron z-Momentum on ID Entry; momentum (MeV/c);count",100,-60.,60.);
	TH1D fEIDDistance("fEIDDistance","Distance Traveled by Electron in ID",100,0.,40.);

	TH1D fEODTime("fEODTime","Electron Time of OD Entry;time (ns);count",100,0.,20000.);
	TH1D fEODPosX("fEODPosX","Electron x-Position on OD Entry;position (cm);count",100,-20.,20.);
	TH1D fEODPosY("fEODPosY","Electron y-Position on OD Entry;position (cm);count",100,-20.,20.);
	TH1D fEODPosZ("fEODPosZ","Electron z-Position on OD Entry;position (cm);count",100,-20.,20.);
	TH1D fEODEnergy("fEODEnergy","Electron Energy on OD Entry;energy (MeV);count",100,0.,60.);
	TH1D fEODMomX("fEODMomX","Electron x-Momentum on OD Entry; momentum (MeV/c);count",100,-60.,60.);
	TH1D fEODMomY("fEODMomY","Electron y-Momentum on OD Entry; momentum (MeV/c);count",100,-60.,60.);
	TH1D fEODMomZ("fEODMomZ","Electron z-Momentum on OD Entry; momentum (MeV/c);count",100,-60.,60.);
	TH1D fEODDistance("fEODDistance","Distance Traveled by Electron in OD",100,0.,40.);

	TH1D fPhIDMu("fPhIDMu","Muon Photons in ID;# of photons;# of events",100,0,5000);
	TH1D fPhIDMuTime("fPhIDMuTime","Time of Photon Detection in ID (mu);time (ns);# of photons",100,0.,50.);
	TH1D fPhIDMuWavelength("fPhIDMuWavelength","Photon Wavelength in ID (mu);wavelength (nm);# of photons",100,200.,700.);
	TH1D fPhIDE("fPhIDE","Electron Photons in ID;# of photons;# of events",100,0,5000);
	TH1D fPhIDETime("fPhIDETime","Time of Photon Detection in ID (e);time (ns);# of photons",100,0.,10000.);
	TH1D fPhIDEWavelength("fPhIDEWavelength","Photon Wavelength in ID (e);wavelength (nm);# of photons",100,200.,700.);

	TH1D fPhODMu("fPhODMu","Muon Photons in OD;# of photons;# of events",100,0,100000);
	TH1D fPhODMuTime("fPhODMuTime","Time of Photon Detection in OD (mu);time (ns);# of photons",100,0.,50.);
	TH1D fPhODMuWavelength("fPhODMuWavelength","Photon Wavelength in OD (mu);wavelength (nm);# of photons",100,200.,700.);
	TH1D fPhODE("fPhODE","Electron Photons in OD;# of photons;# of events",100,0,100000);
	TH1D fPhODETime("fPhODETime","Time of Photon Detection in OD (e);time (ns);# of photons",100,0.,10000.);
	TH1D fPhODEWavelength("fPhODEWavelength","Photon Wavelength in OD (e);wavelength (nm);# of photons",100,200.,700.);

	// 2D branch histos

	TH2D fMuPosXY("fMuPosX:fMuPosY","Muon Initial xy-Position",100,-80.,80.,100,-80.,80.);
	TH2D fMuMomXY("fMuMomX:fMuMomY","Muon Initial xy-Momentum",100,-240.,240.,100,-240.,240.);
	TH2D fMuMomXZ("fMuMomX:fMuMomZ","Muon Initial xz-Momentum",100,-240.,240.,100,-240.,0.);
	TH2D fMuMomYZ("fMuMomY:fMuMomZ","Muon Initial yz-Momentum",100,-240.,240.,100,-240.,0.);

	TH2D fMuIDPosXY("fMuIDPosX:fMuIDPosY","Muon xy-Position on ID Entry",100,-16.,16.,100,-16.,16.);
	TH2D fMuIDPosXZ("fMuIDPosX:fMuIDPosZ","Muon xz-Position on ID Entry",100,-16.,16.,100,-16.,16.);
	TH2D fMuIDPosYZ("fMuIDPosY:fMuIDPosZ","Muon yz-Position on ID Entry",100,-16.,16.,100,-16.,16.);
	TH2D fMuIDMomXY("fMuIDMomX:fMuIDMomY","Muon xy-Momentum on ID Entry",100,-260.,260.,100,-260.,260.);
	TH2D fMuIDMomXZ("fMuIDMomX:fMuIDMomZ","Muon xz-Momentum on ID Entry",100,-260.,260.,100,-260.,0.);
	TH2D fMuIDMomYZ("fMuIDMomY:fMuIDMomZ","Muon yz-Momentum on ID Entry",100,-260.,260.,100,-260.,0.);

	TH2D fMuODPosXY("fMuODPosX:fMuODPosY","Muon xy-Position on OD Entry",100,-16.,16.,100,-16.,16.);
	TH2D fMuODPosXZ("fMuODPosX:fMuODPosZ","Muon xz-Position on OD Entry",100,-16.,16.,100,-16.,16.);
	TH2D fMuODPosYZ("fMuODPosY:fMuODPosZ","Muon yz-Position on OD Entry",100,-16.,16.,100,-16.,16.);
	TH2D fMuODMomXY("fMuODMomX:fMuODMomY","Muon xy-Momentum on OD Entry",100,-260.,260.,100,-260.,260.);
	TH2D fMuODMomXZ("fMuODMomX:fMuODMomZ","Muon xz-Momentum on OD Entry",100,-260.,260.,100,-260.,0.);
	TH2D fMuODMomYZ("fMuODMomY:fMuODMomZ","Muon yz-Momentum on OD Entry",100,-260.,260.,100,-260.,0.);

	TH2D fEPosXY("fEPosX:fEPosY","Electron Initial xy-Position",100,-30.,30.,100,-30.,30.);
	TH2D fEPosXZ("fEPosX:fEPosZ","Electron Initial xz-Position",100,-30.,30.,100,-30.,30.);
	TH2D fEPosYZ("fEPosY:fEPosZ","Electron Initial yz-Position",100,-30.,30.,100,-30.,30.);
	TH2D fEMomXY("fEMomX:fEMomY","Electron Initial xy-Momentum",100,-60.,60.,100,-60.,60.);
	TH2D fEMomXZ("fEMomX:fEMomZ","Electron Initial xz-Momentum",100,-60.,60.,100,-60.,60.);
	TH2D fEMomYZ("fEMomY:fEMomZ","Electron Initial yz-Momentum",100,-60.,60.,100,-60.,60.);

	TH2D fEIDPosXY("fEIDPosX:fEIDPosY","Electron xy-Position on ID Entry",100,-30.,30.,100,-30.,30.);
	TH2D fEIDPosXZ("fEIDPosX:fEIDPosZ","Electron xz-Position on ID Entry",100,-30.,30.,100,-30.,30.);
	TH2D fEIDPosYZ("fEIDPosY:fEIDPosZ","Electron yz-Position on ID Entry",100,-30.,30.,100,-30.,30.);
	TH2D fEIDMomXY("fEIDMomX:fEIDMomY","Electron xy-Momentum on ID Entry",100,-60.,60.,100,-60.,60.);
	TH2D fEIDMomXZ("fEIDMomX:fEIDMomZ","Electron xz-Momentum on ID Entry",100,-60.,60.,100,-60.,60.);
	TH2D fEIDMomYZ("fEIDMomY:fEIDMomZ","Electron yz-Momentum on ID Entry",100,-60.,60.,100,-60.,60.);

	TH2D fEODPosXY("fEODPosX:fEODPosY","Electron xy-Position on OD Entry",100,-30.,30.,100,-30.,30.);
	TH2D fEODPosXZ("fEODPosX:fEODPosZ","Electron xz-Position on OD Entry",100,-30.,30.,100,-30.,30.);
	TH2D fEODPosYZ("fEODPosY:fEODPosZ","Electron yz-Position on OD Entry",100,-30.,30.,100,-30.,30.);
	TH2D fEODMomXY("fEODMomX:fEODMomY","Electron xy-Momentum on OD Entry",100,-60.,60.,100,-60.,60.);
	TH2D fEODMomXZ("fEODMomX:fEODMomZ","Electron xz-Momentum on OD Entry",100,-60.,60.,100,-60.,60.);
	TH2D fEODMomYZ("fEODMomY:fEODMomZ","Electron yz-Momentum on OD Entry",100,-60.,60.,100,-60.,60.);

	TH2D fPhIDvsODMu("fPhIDMu:fPhODMu","Photons in ID vs OD (mu)",100,0,5000,100,0,100000);
	TH2D fPhIDvsODE("fPhIDE:fPhODE","Photons in ID vs OD (e)",100,0,5000,100,0,100000);
	TH2D fMuIDDistancePh("fMuIDDistance:fPhIDMu","Distance Traveled by Muon vs Photons in ID;Distance (cm);# of photons",100,0.,40.,100,0,5000);
	TH2D fMuODDistancePh("fMuODDistance:fPhODMu","Distance Traveled by Muon vs Photons in OD",100,0.,40.,100,0,100000);
	TH2D fEIDDistancePh("fEIDDistance:fPhIDE","Distance Traveled by Electron vs Photons in ID",100,0.,40.,100,0,5000);
	TH2D fEODDistancePh("fEODDistance:fPhODE","Distance Traveled by Electron vs Photons in OD",100,0.,40.,100,0,100000);
	TH2D fMuIDEnergyPh("fMuIDEnergy:fPhIDMu","Muon Energy on Entry vs Photons in ID",100,100.,260.,100,0,5000);
	TH2D fMuODEnergyPh("fMuODEnergy:fPhODMu","Muon Energy on Entry vs Photons in OD",100,100.,260.,100,0,100000);
	TH2D fEEnergyPhID("fEEnergy:fPhIDE","Electron Initial Energy vs Photons in ID",100,0.,60.,100,0,5000);
	TH2D fEEnergyPhOD("fEEnergy:fPhODE","Electron Initial Energy vs Photons in OD",100,0.,60.,100,0,100000);
	TH2D fMuIDEnergyDistance("fMuIDEnergy:fMuIDDistance","Muon Energy on Entry vs Distance in ID",100,100.,260.,100,0.,40.);
	TH2D fMuODEnergyDistance("fMuODEnergy:fMuODDistance","Muon Energy on Entry vs Distance in OD",100,100.,260.,100,0.,40.);
	TH2D fEEnergyIDDistance("fEEnergy:fEIDDistance","Muon Energy on Entry vs Distance in ID",100,0.,60.,100,0.,40.);
	TH2D fEEnergyODDistance("fEEnergy:fEODDistance","Muon Energy on Entry vs Distance in OD",100,0.,60.,100,0.,40.);

	// Custom 1D hists

	TH1D fMuMom("fMuMom","Muon Initial Momentum;momentum (MeV/c);count",100,0.,150.);
	TH1D fCosZenithAngle("fCosZenithAngle","Cosine of Zenith Angle",100,-1.,0.);
}

void FillAndSave(AnalysisManager& anl, Cut_abs* C, const char* filename, 
                 const char* suffix);
void FillMuCosAndMom(AnalysisManager& anl, Cut_abs* C);
void FillPhSum(AnalysisManager& anl, Cut_abs* C);
void AddBranches(AnalysisManager& anl);
void AddHistograms(AnalysisManager& anl);

// main()
// extracts TTrees from .root files and uses AnalysisManager class to fill 
// histograms using data from TTree. Cuts are made using a custom Cut class.
// After filling, histograms are saved in a file as a TList.
void analysis_bulk(const char* filename)
{
	TStopwatch stopwatch;
	stopwatch.Start();

	cout << "BEGIN" << endl;
	cout << "Filename: " << filename << endl;

    // Initialize manager with name of the .root file
    //
	string s1 = string(filename) + ".root";
	AnalysisManager anl(s1.c_str());

	AddBranches(anl); // Add branches from the file to the manager
	AddHistograms(anl); // Add histgrams defined in namespace to the manager

	Cut_abs C; // this is a test dummy cut, it always returns true

	// Define cuts Escape (ce) and Decay (cd) (either ID or OD, doesn't matter)
	// The entries themselves are then enough to tell
	// Create cO,cOI,cOIO for mu, e
	// Create cdO, cdOI, cdOIO, cdI, cdIO for electron
	// Combine the two to get the desired 10 cuts

    // We need to pass pointers in the cut, so this array is an alternative to
    // defining 3 separate pointers. It is used to check integer variables such
    // as decay state
    //
	int val[3] = {0, 1, 2};
	int min_one = -1;
	int min_two = -2;

//----------------------------------------------------------------------------//
// These are primary cuts that track entry of muons and electrons in the outer
// and inner detectors
//----------------------------------------------------------------------------//

	Cut<Int_t> cMuIDEntry0(anl.GetBranch_int("fMuIDEntry"), "==", &val[0]);
	Cut<Int_t> cMuIDEntry1(anl.GetBranch_int("fMuIDEntry"), "==", &val[1]);
	Cut<Int_t> cMuODEntry1(anl.GetBranch_int("fMuODEntry"), "==", &val[1]);
	Cut<Int_t> cMuODEntry2(anl.GetBranch_int("fMuODEntry"), "==", &val[2]);

	Cut<Int_t> cEIDEntry0(anl.GetBranch_int("fEIDEntry"), "==", &val[0]);
	Cut<Int_t> cEIDEntry1(anl.GetBranch_int("fEIDEntry"), "==", &val[1]);
	Cut<Int_t> cEODEntry0(anl.GetBranch_int("fEODEntry"), "==", &val[0]);
	Cut<Int_t> cEODEntry1(anl.GetBranch_int("fEODEntry"), "==", &val[1]);

	Cut<Int_t> ce(anl.GetBranch_int("fMuDecay"),"==", &min_two);
	Cut<Int_t> cdInner(anl.GetBranch_int("fMuDecay"),"==", &val[1]);
	Cut<Int_t> cdOuter(anl.GetBranch_int("fMuDecay"),"==", &val[2]);
	Cut<Int_t> ck(anl.GetBranch_int("fMuDecay"),"==", &min_one);

//----------------------------------------------------------------------------//
// We now combine the cuts above using the Cut_abs class, which works like a
// binary tree. The final result are 13 cuts that partition the dataset based
// on where muons and electrons went.
//----------------------------------------------------------------------------//

	Cut_abs cMu_O(&cMuODEntry1, "&&", &cMuIDEntry0);
	Cut_abs cMu_OI(&cMuODEntry1, "&&", &cMuIDEntry1);
	Cut_abs cMu_OIO(&cMuODEntry2, "&&", &cMuIDEntry1);

	Cut_abs cE_O(&cEODEntry0, "&&", &cEIDEntry0);
	Cut_abs cE_OI(&cEODEntry0, "&&", &cEIDEntry1);
	Cut_abs cE_OIO(&cEODEntry1, "&&", &cEIDEntry1);
	Cut_abs cE_I(&cEODEntry0, "&&", &cEIDEntry0);
	Cut_abs cE_IO(&cEODEntry1, "&&", &cEIDEntry0);

	Cut_abs cdO(&cE_O, "&&", &cdOuter);
	Cut_abs cdOI(&cE_OI, "&&", &cdOuter);
	Cut_abs cdOIO(&cE_OIO, "&&", &cdOuter);
	Cut_abs cdI(&cE_I, "&&", &cdInner);
	Cut_abs cdIO(&cE_IO, "&&", &cdInner);
    
    // These are the final cuts used to partition the data
    //
	Cut_abs cOe(&cMu_O, "&&", &ce);
	Cut_abs cOIOe(&cMu_OIO, "&&", &ce);
	Cut_abs cOdO(&cMu_O, "&&", &cdO);
	Cut_abs cOdOI(&cMu_O, "&&", &cdOI);
	Cut_abs cOdOIO(&cMu_O, "&&", &cdOIO);
	Cut_abs cOIdI(&cMu_OI, "&&", &cdI);
	Cut_abs cOIdIO(&cMu_OI, "&&", &cdIO);
	Cut_abs cOIOdO(&cMu_OIO, "&&", &cdO);
	Cut_abs cOIOdOI(&cMu_OIO, "&&", &cdOI);
	Cut_abs cOIOdOIO(&cMu_OIO, "&&", &cdOIO);
	Cut_abs cOk(&cMu_O, "&&", &ck);
	Cut_abs cOIk(&cMu_OI, "&&", &ck);
	Cut_abs cOIOk(&cMu_OIO, "&&", &ck);

//----------------------------------------------------------------------------//

    // We now fill all the histograms and save them to separate files depending
    // on the cut.
    //
	cout << "Filling:" << endl;
	cout << endl << "NO CUT:" << endl;
	FillAndSave(anl, &C, filename, "raw");

	cout << endl << "CUT 1 of 13: cOe" << endl;
	FillAndSave(anl, &cOe, filename, "cOe");

	cout << endl << "CUT 2 of 13: cOIOe" << endl;
	FillAndSave(anl, &cOIOe, filename, "cOIOe");

	cout << endl << "CUT 3 of 13: cOdO" << endl;
	FillAndSave(anl, &cOdO, filename, "cOdO");

	cout << endl << "CUT 4 of 13: cOdOI" << endl;
	FillAndSave(anl, &cOdOI, filename, "cOdOI");

	cout << endl << "CUT 5 of 13: cOdOIO" << endl;
	FillAndSave(anl, &cOdOIO, filename, "cOdOIO");

	cout << endl << "CUT 6 of 13: cOIdI" << endl;
	FillAndSave(anl, &cOIdI, filename, "cOIdI");

	cout << endl << "CUT 7 of 13: cOIdIO" << endl;
	FillAndSave(anl, &cOIdIO, filename, "cOIdIO");

	cout << endl << "CUT 8 of 13: cOIOdO" << endl;
	FillAndSave(anl, &cOIOdO, filename, "cOIOdO");

	cout << endl << "CUT 9 of 13: cOIOdOI" << endl;
	FillAndSave(anl, &cOIOdOI, filename, "cOIOdOI");

	cout << endl << "CUT 10 of 13: cOIOdOIO" << endl;
	FillAndSave(anl, &cOIOdOIO, filename, "cOIOdOIO");

	cout << endl << "CUT 11 of 13: cOk" << endl;
	FillAndSave(anl, &cOk, filename, "cOk");

	cout << endl << "CUT 12 of 13: cOIk" << endl;
	FillAndSave(anl, &cOIk, filename, "cOIk");

	cout << endl << "CUT 13 of 13: cOIOk" << endl;
	FillAndSave(anl, &cOIOk, filename, "cOIOk");

	cout << "END" << endl;

	stopwatch.Stop();

	cout << "Runtime: " << stopwatch.RealTime() << " s" << endl;
}

#ifndef __CINT__

// The main() is defined this way because root v5 interctve sessions didn't
// allow main(), and the actual main() is defined in analysis_bulk()
//
int main(int argc, char* argv[])
{
	analysis_bulk(argv[1]);

	return 0;
}

#endif

// This method takes all the branches in TTree and fills histograms named
// exactly like the branches. It also fills custom histos like cosine of zenith
// angle, but the algorithm for that is a separate function. AnalysisManager
// only fills histograms that directly correspond to branches
//
void FillAndSave(AnalysisManager& anl, Cut_abs* C, const char* filename, 
                 const char* cutname)
{
    // reset histograms if they were filled previouly
    //
	anl.Reset();

	// 1D histos
	cout << "1/12: fMu" << endl;
	anl.Fill(C, "fMuPosX","fMuMomZ");
	cout << "2/12: fMuID" << endl;
	anl.Fill(C, "fMuIDTime","fMuIDDistance");
	cout << "3/12: fMuOD" << endl;
	anl.Fill(C, "fMuODTime","fMuODDistance");
	cout << "4/12: fE" << endl;
	anl.Fill(C, "fETime","fEMomZ");
	cout << "5/12: fEID" << endl;
	anl.Fill(C, "fEIDTime","fEIDDistance");
	cout << "6/12: fEOD" << endl;
	anl.Fill(C, "fEODTime","fEODDistance");
	cout << "7/12: fPhIDMu" << endl;
	anl.Fill(C, "fPhIDMu","fPhIDMuWavelength");
	cout << "8/12: fPhIDE" << endl;
	anl.Fill(C, "fPhIDE","fPhIDEWavelength");
	cout << "9/12: fPhODMu" << endl;
	anl.Fill(C, "fPhODMu","fPhODMuWavelength");
	cout << "10/12: fPhODE" << endl;
	anl.Fill(C, "fPhODE","fPhODEWavelength");
    // 2D histos
	cout << "11/12: 2D hists" << endl;
	anl.Fill(C, "fMuPosX:fMuPosY","fEEnergy:fEODDistance");

	// Custom histos
    // These histos don't have a matching branch and are filled in a separate
    // function. We pass AnalysisManager in the arguments just to have the
    // histograms stored there for easier time saving things to a file and to
    // apply same cuts as everything else
    //
	cout << "12/12: Cosine and momentum" << endl;
	FillMuCosAndMom(anl, C);

	// Save all the histos to a file
	cout << "saving" << endl;
	TList l;
	for (int i = 0; i < anl.GetNumHistograms_1D(); i++)
		l.Add(&anl.hists_1D[i]);
	for (int i = 0; i < anl.GetNumHistograms_2D(); i++)
		l.Add(&anl.hists_2D[i]);

    // .root files are named "<filename>_histlist_<cutname>.root"
    //
	string fname = string(filename) + "_histlist_" + string(cutname) + ".root";
	cout << "Writing to " << fname << endl;
	TFile f(fname.c_str(),"RECREATE");
    l.Write("histlist", TObject::kSingleKey);
}

// Function to fill histograms that don't have an associated TTree branch
//
void FillMuCosAndMom(AnalysisManager& anl, Cut_abs* C)
{
	Int_t nentries = (Int_t)anl.chain->GetEntries();
	for (Int_t i = 0; i < nentries; i++)
	{
		anl.chain->GetEntry(i);

		if (C->ApplyCut())
		{
			Double_t MomX = *anl.GetBranch_double("fMuMomX");
			Double_t MomY = *anl.GetBranch_double("fMuMomY");
			Double_t MomZ = *anl.GetBranch_double("fMuMomZ");

			Double_t Momentum = std::pow(MomX*MomX+MomY*MomY+MomZ*MomZ,0.5);
			Double_t cos_theta = MomZ/Momentum;

			anl.hists_1D[anl.GetHist_1DIndex("fMuMom")].Fill(Momentum);
			anl.hists_1D[anl.GetHist_1DIndex("fCosZenithAngle")].Fill(cos_theta);
		}
	}
}

// Extracts branches from TTree to allow AnalysisManager to work on them
//
void AddBranches(AnalysisManager& anl)
{
	anl.AddBranch_double("fMuPosX");
	anl.AddBranch_double("fMuPosY");
	anl.AddBranch_double("fMuPosZ");
	anl.AddBranch_double("fMuEnergy");
	anl.AddBranch_double("fMuMomX");
	anl.AddBranch_double("fMuMomY");
	anl.AddBranch_double("fMuMomZ");
	anl.AddBranch_int("fMuCharge");
	anl.AddBranch_int("fMuDecay");

	anl.AddBranch_double("fMuIDTime");
	anl.AddBranch_double("fMuIDPosX");
	anl.AddBranch_double("fMuIDPosY");
	anl.AddBranch_double("fMuIDPosZ");
	anl.AddBranch_double("fMuIDEnergy");
	anl.AddBranch_double("fMuIDMomX");
	anl.AddBranch_double("fMuIDMomY");
	anl.AddBranch_double("fMuIDMomZ");
	anl.AddBranch_double("fMuIDDistance");
	anl.AddBranch_int("fMuIDEntry");

	anl.AddBranch_double("fMuODTime");
	anl.AddBranch_double("fMuODPosX");
	anl.AddBranch_double("fMuODPosY");
	anl.AddBranch_double("fMuODPosZ");
	anl.AddBranch_double("fMuODEnergy");
	anl.AddBranch_double("fMuODMomX");
	anl.AddBranch_double("fMuODMomY");
	anl.AddBranch_double("fMuODMomZ");
	anl.AddBranch_double("fMuODDistance");
	anl.AddBranch_int("fMuODEntry");

	anl.AddBranch_double("fETime");
	anl.AddBranch_double("fEPosX");
	anl.AddBranch_double("fEPosY");
	anl.AddBranch_double("fEPosZ");
	anl.AddBranch_double("fEEnergy");
	anl.AddBranch_double("fEMomX");
	anl.AddBranch_double("fEMomY");
	anl.AddBranch_double("fEMomZ");

	anl.AddBranch_double("fEIDTime");
	anl.AddBranch_double("fEIDPosX");
	anl.AddBranch_double("fEIDPosY");
	anl.AddBranch_double("fEIDPosZ");
	anl.AddBranch_double("fEIDEnergy");
	anl.AddBranch_double("fEIDMomX");
	anl.AddBranch_double("fEIDMomY");
	anl.AddBranch_double("fEIDMomZ");
	anl.AddBranch_double("fEIDDistance");
	anl.AddBranch_int("fEIDEntry");

	anl.AddBranch_double("fEODTime");
	anl.AddBranch_double("fEODPosX");
	anl.AddBranch_double("fEODPosY");
	anl.AddBranch_double("fEODPosZ");
	anl.AddBranch_double("fEODEnergy");
	anl.AddBranch_double("fEODMomX");
	anl.AddBranch_double("fEODMomY");
	anl.AddBranch_double("fEODMomZ");
	anl.AddBranch_double("fEODDistance");
	anl.AddBranch_int("fEODEntry");

	anl.AddBranch_int("fPhIDMu");
	anl.AddBranch_vec_double("fPhIDMuTime");
	anl.AddBranch_vec_double("fPhIDMuWavelength");
	anl.AddBranch_vec_int("fPhIDMuProcess");

	anl.AddBranch_int("fPhIDE");
	anl.AddBranch_vec_double("fPhIDETime");
 	anl.AddBranch_vec_double("fPhIDEWavelength");
    anl.AddBranch_vec_int("fPhIDEProcess");

 	anl.AddBranch_int("fPhODMu");
	anl.AddBranch_vec_double("fPhODMuTime");
	anl.AddBranch_vec_double("fPhODMuWavelength");
	anl.AddBranch_vec_int("fPhODMuProcess");

	anl.AddBranch_int("fPhODE");
	anl.AddBranch_vec_double("fPhODETime");
 	anl.AddBranch_vec_double("fPhODEWavelength");
    anl.AddBranch_vec_int("fPhODEProcess");
}

// Add all the histograms defined in the namespace to AnalysisManager
//
void AddHistograms(AnalysisManager& anl)
{
	anl.AddHist(fMuPosX);
	anl.AddHist(fMuPosY);
	anl.AddHist(fMuPosZ);
	anl.AddHist(fMuEnergy);
	anl.AddHist(fMuMomX);
	anl.AddHist(fMuMomY);
	anl.AddHist(fMuMomZ);
	anl.AddHist(fMuIDTime);
	anl.AddHist(fMuIDPosX);
	anl.AddHist(fMuIDPosY);
	anl.AddHist(fMuIDPosZ);
	anl.AddHist(fMuIDEnergy);
	anl.AddHist(fMuIDMomX);
	anl.AddHist(fMuIDMomY);
	anl.AddHist(fMuIDMomZ);
	anl.AddHist(fMuIDDistance);
	anl.AddHist(fMuODTime);
	anl.AddHist(fMuODPosX);
	anl.AddHist(fMuODPosY);
	anl.AddHist(fMuODPosZ);
	anl.AddHist(fMuODEnergy);
	anl.AddHist(fMuODMomX);
	anl.AddHist(fMuODMomY);
	anl.AddHist(fMuODMomZ);
	anl.AddHist(fMuODDistance);
	anl.AddHist(fETime);
	anl.AddHist(fEPosX);
	anl.AddHist(fEPosY);
	anl.AddHist(fEPosZ);
	anl.AddHist(fEEnergy);
	anl.AddHist(fEMomX);
	anl.AddHist(fEMomY);
	anl.AddHist(fEMomZ);
	anl.AddHist(fEIDTime);
	anl.AddHist(fEIDPosX);
	anl.AddHist(fEIDPosY);
	anl.AddHist(fEIDPosZ);
	anl.AddHist(fEIDEnergy);
	anl.AddHist(fEIDMomX);
	anl.AddHist(fEIDMomY);
	anl.AddHist(fEIDMomZ);
	anl.AddHist(fEIDDistance);
	anl.AddHist(fEODTime);
	anl.AddHist(fEODPosX);
	anl.AddHist(fEODPosY);
	anl.AddHist(fEODPosZ);
	anl.AddHist(fEODEnergy);
	anl.AddHist(fEODMomX);
	anl.AddHist(fEODMomY);
	anl.AddHist(fEODMomZ);
	anl.AddHist(fEODDistance);
	anl.AddHist(fPhIDMu);
	anl.AddHist(fPhIDMuTime);
	anl.AddHist(fPhIDMuWavelength);
	anl.AddHist(fPhIDE);
	anl.AddHist(fPhIDETime);
	anl.AddHist(fPhIDEWavelength);
	anl.AddHist(fPhODMu);
	anl.AddHist(fPhODMuTime);
	anl.AddHist(fPhODMuWavelength);
	anl.AddHist(fPhODE);
	anl.AddHist(fPhODETime);
	anl.AddHist(fPhODEWavelength);
	anl.AddHist(fMuPosXY);
	anl.AddHist(fMuMomXY);
	anl.AddHist(fMuMomXZ);
	anl.AddHist(fMuMomYZ);
	anl.AddHist(fMuIDPosXY);
	anl.AddHist(fMuIDPosXZ);
	anl.AddHist(fMuIDPosYZ);
	anl.AddHist(fMuIDMomXY);
	anl.AddHist(fMuIDMomXZ);
	anl.AddHist(fMuIDMomYZ);
	anl.AddHist(fMuODPosXY);
	anl.AddHist(fMuODPosXZ);
	anl.AddHist(fMuODPosYZ);
	anl.AddHist(fMuODMomXY);
	anl.AddHist(fMuODMomXZ);
	anl.AddHist(fMuODMomYZ);
	anl.AddHist(fEPosXY);
	anl.AddHist(fEPosXZ);
	anl.AddHist(fEPosYZ);
	anl.AddHist(fEMomXY);
	anl.AddHist(fEMomXZ);
	anl.AddHist(fEMomYZ);
	anl.AddHist(fEIDPosXY);
	anl.AddHist(fEIDPosXZ);
	anl.AddHist(fEIDPosYZ);
	anl.AddHist(fEIDMomXY);
	anl.AddHist(fEIDMomXZ);
	anl.AddHist(fEIDMomYZ);
	anl.AddHist(fEODPosXY);
	anl.AddHist(fEODPosXZ);
	anl.AddHist(fEODPosYZ);
	anl.AddHist(fEODMomXY);
	anl.AddHist(fEODMomXZ);
	anl.AddHist(fEODMomYZ);
	anl.AddHist(fPhIDvsODMu);
	anl.AddHist(fPhIDvsODE);
	anl.AddHist(fMuIDDistancePh);
	anl.AddHist(fMuODDistancePh);
	anl.AddHist(fEIDDistancePh);
	anl.AddHist(fEODDistancePh);
	anl.AddHist(fMuIDEnergyPh);
	anl.AddHist(fMuODEnergyPh);
	anl.AddHist(fEEnergyPhID);
	anl.AddHist(fEEnergyPhOD);
	anl.AddHist(fMuIDEnergyDistance);
	anl.AddHist(fMuODEnergyDistance);
	anl.AddHist(fEEnergyIDDistance);
	anl.AddHist(fEEnergyODDistance);
	anl.AddHist(fMuMom);
	anl.AddHist(fCosZenithAngle);
}
