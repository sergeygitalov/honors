// \file analysis_bulk.cc
// \brief Program to convert CosmicMu root ntuples into histograms

#include <iostream>
#include <sstream>
#include <string>

#include "TCanvas.h"
#include "TStopwatch.h"
#include "TH2.h"
#include "TChain.h"
#include "TFile.h"

#include "include/Cut.hh"

using namespace std;

// Histograms are defined globally in the namespace to use them throughout 
// mulltiple functions
//
namespace
{
	// 2D branch histos

	TH2D fMuPosXY("fMuPosX:fMuPosY","Muon xy-Position on OD Entry",100,-16.,16.,100,-16.,16.);
	TH2D fMuPosXZ("fMuPosX:fMuPosZ","Muon xz-Position on OD Entry",100,-16.,16.,100,-16.,16.);
	TH2D fMuPosYZ("fMuPosY:fMuPosZ","Muon yz-Position on OD Entry",100,-16.,16.,100,-16.,16.);
	TH2D fMuMomXY("fMuMomX:fMuMomY","Muon xy-Momentum on OD Entry",100,-260.,260.,100,-260.,260.);
	TH2D fMuMomXZ("fMuMomX:fMuMomZ","Muon xz-Momentum on OD Entry",100,-260.,260.,100,-260.,0.);
	TH2D fMuMomYZ("fMuMomY:fMuMomZ","Muon yz-Momentum on OD Entry",100,-260.,260.,100,-260.,0.);

	TH2D fMuIDEnergyPh("fMuIDEnergy:fIDMuPh","Muon Energy on Entry vs Photons in ID",100,100.,260.,100,0,5000);
	TH2D fMuODEnergyPh("fMuEnergy:fODMuPh","Muon Energy on Entry vs Photons in OD",100,100.,260.,100,0,100000);
	TH2D fEEnergyPhID("fEEnergy:fIDEPh","Electron Initial Energy vs Photons in ID",100,0.,60.,100,0,5000);
	TH2D fEEnergyPhOD("fEEnergy:fODEPh","Electron Initial Energy vs Photons in OD",100,0.,60.,100,0,100000);

	// Custom 1D hists

	TH1D fCosZenithAngle("fCosZenithAngle","Cosine of Zenith Angle",100,-1.,0.);
}

// main()
// extracts TTrees from .root files and uses AnalysisManager class to fill 
// histograms using data from TTree. Cuts are made using a custom Cut class.
// After filling, histograms are saved in a file as a TList.
void analysis_bulk(const char* filename)
{
	TStopwatch stopwatch;
	stopwatch.Start();

	cout << "BEGIN" << endl;
	cout << "Filename: " << filename << endl;

    // Initialize manager with name of the .root file
    //
	cout << "1" << filename << endl;
	string s1 = string(filename) + ".root";
	cout << "2" << filename << endl;
    TChain* chain = new TChain("data");
	cout << "3" << filename << endl;
    chain->Add(s1.c_str());
	cout << "4" << filename << endl;

    Double_t* fMuPosX = new Double_t; 
    chain->SetBranchAddress("fMuPosX", fMuPosX);
    Double_t* fMuPosY = new Double_t; 
    chain->SetBranchAddress("fMuPosY", fMuPosY);
    Double_t* fMuPosZ = new Double_t; 
    chain->SetBranchAddress("fMuPosZ", fMuPosZ);
    Double_t* fMuMomX = new Double_t; 
    chain->SetBranchAddress("fMuMomX", fMuMomX);
    Double_t* fMuMomY = new Double_t; 
    chain->SetBranchAddress("fMuMomY", fMuMomY);
    Double_t* fMuMomZ = new Double_t; 
    chain->SetBranchAddress("fMuMomZ", fMuMomZ);
    Double_t* fMuEnergy = new Double_t; 
    chain->SetBranchAddress("fMuEnergy", fMuEnergy);
    Double_t* fEEnergy = new Double_t; 
    chain->SetBranchAddress("fEEnergy", fEEnergy);
    Int_t* fIDMuPh = new Int_t; 
    chain->SetBranchAddress("fIDMuPh", fIDMuPh);
    Int_t* fODMuPh = new Int_t; 
    chain->SetBranchAddress("fODMuPh", fODMuPh);
    Int_t* fIDEPh = new Int_t; 
    chain->SetBranchAddress("fIDEPh", fIDEPh);
    Int_t* fODEPh = new Int_t; 
    chain->SetBranchAddress("fODEPh", fODEPh);

    Int_t* fMuEntry = new Int_t;
    chain->SetBranchAddress("fMuEntry",fMuEntry);
    Int_t* fDecayVolume = new Int_t;
    chain->SetBranchAddress("fDecayVolume",fDecayVolume);

    int val[3] = {0,1,2};

	Cut_abs C; // this is a test dummy cut, it always returns true
    Cut<Int_t> cMuEntry(fMuEntry,"==", &val[1]);
    Cut<Int_t> cDecayVolume(fDecayVolume,"==",&val[1]);
    
	cout << "Filename: " << filename << endl;

    Int_t nentries = (Int_t)chain->GetEntries();
	cout << "Filename: " << filename << endl;
	for (Int_t i = 0; i < nentries; i++)
	{
		chain->GetEntry(i);
        
	        //if (C.ApplyCut())
		//if (cMuEntry.ApplyCut())
		if (cDecayVolume.ApplyCut())
		{
            Double_t MomX = *fMuMomX;
            Double_t MomY = *fMuMomY;
            Double_t MomZ = *fMuMomZ;
			Double_t Momentum = std::pow(MomX*MomX+MomY*MomY+MomZ*MomZ,0.5);
			Double_t cos_theta = MomZ/Momentum;

            fMuPosXY.Fill(*fMuPosX, *fMuPosY);
            fMuPosXZ.Fill(*fMuPosX, *fMuPosZ);
            fMuPosYZ.Fill(*fMuPosY, *fMuPosZ);
            fMuMomXY.Fill(*fMuMomX, *fMuMomY);
            fMuMomXZ.Fill(*fMuMomX, *fMuMomZ);
            fMuMomYZ.Fill(*fMuMomY, *fMuMomZ);
            fMuIDEnergyPh.Fill(*fMuEnergy, *fIDMuPh);
            fMuODEnergyPh.Fill(*fMuEnergy, *fODMuPh);
            fEEnergyPhID.Fill(*fEEnergy, *fIDEPh);
            fEEnergyPhOD.Fill(*fEEnergy, *fODEPh);
            fCosZenithAngle.Fill(cos_theta);
		}
	}

    cout << "saving" << endl;
	TList l;
    l.Add(&fMuPosXY);
    l.Add(&fMuPosXZ);
    l.Add(&fMuPosYZ);
    l.Add(&fMuMomXY);
    l.Add(&fMuMomXZ);
    l.Add(&fMuMomYZ);
    l.Add(&fMuIDEnergyPh);
    l.Add(&fMuODEnergyPh);
    l.Add(&fEEnergyPhID);
    l.Add(&fEEnergyPhOD);
    l.Add(&fCosZenithAngle);

    // .root files are named "<filename>_histlist_<cutname>.root"
    //
	string fname = string(filename) + "_angle" + ".root";
	cout << "Writing to " << fname << endl;
	TFile f(fname.c_str(),"RECREATE");
    l.Write("histlist", TObject::kSingleKey);

	cout << "END" << endl;

	stopwatch.Stop();

	cout << "Runtime: " << stopwatch.RealTime() << " s" << endl;
}

#ifndef __CINT__

// The main() is defined this way because root v5 interctve sessions didn't
// allow main(), and the actual main() is defined in analysis_bulk()
//
int main(int argc, char* argv[])
{
	analysis_bulk(argv[1]);

	return 0;
}

#endif

