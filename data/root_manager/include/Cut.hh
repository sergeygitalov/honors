// \file Cut.hh
// \brief Definition and implementation of Cut_abs and Cut classes

#define Cut_h
#ifdef Cut_h

#include <iostream>
#include <string>

#include "TMath.h"

// This class stands for 'cut abstract'. It is abstract in the sense that it
// does not itself compare values of variables between each other (i.e x < y and
// such). Instead this class is used for 2 purposes:
// 1. As a dummy variable in AnalysisManager's Fill() method. This variable's
// ApplyCut() method always returns true. These dummy variables are only used
// internally.
// 2. As a binary tree to link multiple cuts. Cut_abs initializes using 2 Cut
// classes and a logical operator to link them. So if you want to say have
// energy < 100 AND position > 0 both be cuts in your histogram, you can do this
// by creating Cut<Double_t>* energy_cut, Cut<Double_t>* position_cut, and 
// Cut_abs* combined_cut(energy_cut, "&&", position_cut). Then use combine_cut
// in your Fill() method for AnalysisManager. The binary tree structure allows
// to create almost arbitrarily complex cuts with multiple conditions.
//
class Cut_abs
{
public:
    // initialize as dummy variable
	Cut_abs() : lcut(NULL), rcut(NULL) {}
    // copy initializer
	Cut_abs(const Cut_abs& C) : lcut(C.lcut),
                                log_operator(C.log_operator),
                                rcut(C.rcut) {}
    // initialize as binary tree. log_operator is a string that is the same as
    // logical operator names in C++ (i.e. "&&" and "||")
	Cut_abs(Cut_abs* l, std::string log_o, Cut_abs* r) : lcut(l), 
                                                         log_operator(log_o),
                                                         rcut(r) {}
    // initialize by negating a given cut. Only use "!" as log_operator
	Cut_abs(Cut_abs* l, std::string log_o) : lcut(l),
                                             log_operator(log_o),
                                             rcut(NULL) {}
	~Cut_abs() {}

    // This method returns a boolean 0 or 1 recuresively by applying cuts to
    // lcut and rcut. If there is an error, the method returns -1
    //
	virtual int ApplyCut()
	{
        // dummy variable
		if (lcut == NULL && rcut == NULL)
			return true;
        
        // negating a single cut
		if (rcut == NULL)
		{
			if (log_operator == "!")
				return !lcut->ApplyCut();
			else
			{
				std::cout << "ERROR: Incorrect logic syntax (invalid log_operator)" 
                          << std::endl;
				return -1;
			}
		}

        // combining cuts
		if (log_operator == "&&")
			return lcut->ApplyCut() && rcut->ApplyCut();
		else if (log_operator == "||")
			return lcut->ApplyCut() || rcut->ApplyCut();
		else
			std::cout << "ERROR: Incorrect logic syntax (invalid log_operator)"
                      << std::endl;
			return -1;
	}

protected:
	Cut_abs(std::string log_o) : lcut(NULL),log_operator(log_o),rcut(NULL) {}
	std::string log_operator;

private:
	Cut_abs* lcut;
	Cut_abs* rcut;
};

// This class is a template class inherited from Cut_abs. It is inherited to
// allow the Fill() method to intake both the Cut_abs and Cut classes. The
// template specifies the variable type used for the cut (i.e. Int_t, Double_t,
// and etc.)
//
template <typename T>
class Cut : public Cut_abs
{
public:
    // initialize by giving pointers to two variable that will be compared and
    // providing the boolean operator as a string (i.e. "<" or "==")
	Cut(T* l, std::string log_o, T* r) : Cut_abs(log_o),
                                         loperand(l),
                                         roperand(r) {}
	~Cut() {}

    // This method return a boolean 0 or 1 in case of a successful comparison.
    // In case of an error, returns -1
    //
	int ApplyCut();

private:
	T* loperand;
	T* roperand;
};

// Implementations are written in the header due to how C++ handles templates
//
template <typename T>
int Cut<T>::ApplyCut()
{
	if (loperand == NULL)
	{
		std::cout << "ERROR: Incorrect logic syntax (loperand == NULL)"
                  << std::endl;
		return -1;
	}

	if (log_operator == "==")
	{
		if (*loperand == *roperand)
			return true;
		else
			return false;
	}
	else if (log_operator == "!=")
	{
		if (*loperand != *roperand)
			return true;
		else
			return false;
	}
	else if (log_operator == "<")
	{
		if (*loperand < *roperand)
			return true;
		else
			return false;
	}
	else if (log_operator == "<=")
	{
		if (*loperand <= *roperand)
			return true;
		else
			return false;
	}
	else if (log_operator == ">")
	{
		if (*loperand > *roperand)
			return true;
		else
			return false;
	}
	else if (log_operator == ">=")
	{
		if (*loperand >= *roperand)
			return true;
		else
			return false;
	}
	else
		std::cout << "ERROR: Incorrect logic syntax (invalid log_operator)" 
                  << std::endl;
		return -1;
}

#endif
