// \file HistogramManager.h
// \brief Definition of HistogramManager class

#ifndef HISTOGRAM_MANAGER_H
#define HISTOGRAM_MANAGER_H

#include <string>
#include <vector>

#include "TH1.h"
#include "TList.h"

// This struct stores the histogram and its name
//
struct Histogram
{
    Histogram() : hist(NULL) {}

    // format: fParticleVolumeParentVar
    // e.g: fPhIDMuWavelength
    // if any parameter is absent, fill string with ""

    // parses the input histogram name to fill out the variables below
    void ParseHist(TH1*);

    TH1* hist;
    std::string particle[2]; // tracked particle name
    std::string detector[2]; // inner or outer detectors
    std::string mother[2]; // mother particle. Used for photons.
    std::string var[2]; // tracked variable e.g. position or momentum
    std::string cutname; // cut applied to that histo
    bool is2D; // specifies whether the histo is 1D or 2D

private:
    void Parse(std::string name, int index); // helper method
};

// This class is used specifically for the histograms made in the analysis of
// CosmicMu geant simulation. There are a lot of histograms, so working with
// them is very cumbersome, especially in root. Fortunately, the histograms can
// be grouped by particle, detector volume, etc. HistogramManager takes a root
// TList of histograms, parses histogram names to group them. The user can
// then get a vector of hists grouped by specified parameters. For instance,
// if you wanted to just work with histograms in the outer detector, you would
// create a vector<TH1> with GetHists() method and "OD" as a criterion.
//
class HistogramManager
{
public:
    HistogramManager() : sz(0) {}
    ~HistogramManager() {}

    void ExtractFromFile(const char* filename, std::string cutname);
    // Format:
    // option takes "pdmvc" (particle, detector, mother, var, cutname)
    // or substrings of it
    // criteria takes semicolon-separated criteria names in the same order as
    // in options
    // e.g. option = "pdvc", criteria = "Mu;ID;PosX;OdOI"
    std::vector<TH1*> GetHists(int dim, std::string option, 
                               std::string criteria);

private:
    Histogram hists[2000];
    int sz;
};

#endif // HISTOGRAM_MANAGER_H
