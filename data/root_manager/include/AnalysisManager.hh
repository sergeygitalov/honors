// \file AnalysisManager.hh
// \brief Definition of AnalysisManager class and BranchData struct

#define AnalysisManager_h
#ifdef AnalysisManager_h

#include "Cut.hh"

#include <string>
#include <vector>
#include <limits.h>

#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TH1D.h"
#include "TH1I.h"
#include "TH2D.h"
#include "TH2I.h"

// This is an auxillary struct. It is used since ROOT can get weird about type
// conversions sometimes, and it also stores the name of the branch. It is
// intended to store only one type at a time (i.e. only int or only vec<int>).
//
struct BranchData
{
	BranchData() : name(NULL), dEntry(NULL), iEntry(NULL), vec_dEntry(NULL), 
                   vec_iEntry(NULL) {}
	~BranchData()
	{
		delete dEntry;
		delete iEntry;
		delete vec_dEntry;
		delete vec_iEntry;
	}
	const char* name; // name of the branch
	Double_t* dEntry; // double branch
	Int_t* iEntry; // int branch
	std::vector<Double_t>* vec_dEntry; // vec<double> branch
	std::vector<Int_t>* vec_iEntry; // vec<int> branch
};

// This class is used to open a .root file, extract branch data, and fill
// histograms with it. All branches and histograms are stored in a class, named,
// and can be accessed quickly by that name. The Fill() method fills either
// a single histogram or multiple histograms in the order they were initialized.
// The Fill() method makes use of the Cut class, which allows to quickly set up
// cuts for multiple histograms.
//
class AnalysisManager
{
public:
	AnalysisManager(std::string filename); // extracts tree from file
    // extracts trees from multiple files
	AnalysisManager(std::string filename, int numFiles);
	~AnalysisManager() {delete chain;}
    
    // Methods for extracting branches from a .root file
    //
	void AddBranch_double(const char* name);
	void AddBranch_int(const char* name);
	void AddBranch_vec_double(const char* name);
	void AddBranch_vec_int(const char* name);

    // 1D and 2D histograms are stored and filled separately 
    //
	void AddHist(TH1D);
	void AddHist(TH2D);
    
    // Methods for filling histograms
    //
	void Fill(); // fill every histogram currently stored
	void Fill(const char* name); // fill a specified histo by name
    // fill every histogram from begin to end in order they were added
	void Fill(const char* begin, const char* end);
    // The following fill methods are identical, but with a cut specified
	void Fill(Cut_abs* c);
	void Fill(Cut_abs* c, const char* name); 
	void Fill(Cut_abs* c, const char* begin, const char* end);

    // get methods
    //
	int GetNumBranches() {return numBranches;} // # of stored branches
	int GetNumHistograms_1D() {return numHistograms_1D;} // # of stored histos
	int GetNumHistograms_2D() {return numHistograms_2D;} // # of stored histos
 	int GetHist_1DIndex(const char* name); // array index in hists_1D[]
	int GetHist_2DIndex(const char* name); // array index in hists_2D[]
    // the following methods return pointers to a specified branch. Make sure
    // to use the right method for the right branch, otherwise they will return
    // NULL
	Int_t* GetBranch_int(const char* name);
	Double_t* GetBranch_double(const char* name);
	std::vector<Double_t>* GetBranch_vec_double(const char* name);
	std::vector<Int_t>* GetBranch_vec_int(const char* name);

    // Reset all the 1D and 2D histograms. Uses the TH1D/TH2D Reset() method.
    //
	void Reset();

	TChain* chain;

    // Branches and histograms are stored in fixed-size arrays.
    //
	BranchData data[100];
	TH1D hists_1D[100];
	TH2D hists_2D[100];

private:
    // Counter for the # of added histograms and branches. Used internally for
    // for loops and such
    //
	int numBranches = 0;
	int numHistograms_1D = 0;
	int numHistograms_2D = 0;
    
	int GetBranchIndex(const char* name); // array index of data[]

    // Helper function used in all public Fill() methods
    //
	void FillHelper(Cut_abs* cut, int begin_1D, int end_1D, int begin_2D, 
                    int end_2D);
};

#endif
